
## Jyaista Gangadin  Final Project Video "Backoff"
<div class="youtube-wrapper">
<iframe width="560" height="315" src="https://www.youtube.com/embed/sUHy0N9cfXI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
## Jyaista Gangadin Presentation Slides "Backoff"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR2xOysKcPlRyh1EGbmkxgmI57i0cLoI08WyIE1qRb93wruEw8H7k27WALr-G4LmhZGctwM98Etl06P/embed?start=true&loop=true&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Adjako Manischa Final Project Video "Handzoff"
<iframe width="560" height="315" src="https://www.youtube.com/embed/EEamVrl9fEE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Adjako Manischa Presentation Slides "Handzoff"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTy-j3zkhqyjH0DGm4oqKfIx9pKcjONze-dFPmfJGpuMaqBV14oSFcqaOL1QimEMZOnMEfWP_AVRoYm/embed?start=true&loop=true&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


## Shyfka Landveld & Ghilles Dipotroeno  Final Project Video
### Ghilles Dipotroeno "Advanced Temperature"
<iframe width="560" height="315" src="https://www.youtube.com/embed/nDflaA2a3wQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
### Shyfka Landveld "Advanced Temperature"
<iframe width="560" height="315" src="https://www.youtube.com/embed/kXI-sUaRFiE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
## Shyfka Landveld & Ghilles Dipotroeno Presentation Slide
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT9pxlFg74lMFdCvbXLBxAZmM1mE0RZqkTLyn2gqGDE23J-B5SekkUtMtaCKfMTT_ndkLeH3AHFozb9/embed?start=false&loop=false&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Shanice Ramdien & Darlene Gefferie  Final Project Video
### Shanice Ramdien "Autospenser"
<iframe width="560" height="315" src="https://www.youtube.com/embed/yi7GA2vRpAs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
### Darlene Gefferi "Autospenser"
<iframe width="560" height="315" src="https://www.youtube.com/embed/ZPsndXJFOtE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
## Shanice Ramdien & Darlene Gefferie Presentation Slide
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSBQPPA8KQyrvaxs9TiPcKdXYkkT7VnxDQ1utkmryI0Ii0l0U_dmhcWr3MHP4xn4TVEgkBB7JzMFF3-/embed?start=true&loop=true&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Shayenne Overeem Final Project Video "MiniIotGreenhouse"
<iframe width="560" height="315" src="https://www.youtube.com/embed/nkKe0oVLx4s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Shayenne Overeem  Presentation Slide "MiniIotGreenhouse"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRkTACGvj8AxRs5T63RhhuJFs1KCWWLql-doaicP3E_3A4Z74yxDSJSj8uAd98aDPs6qOdBahJFQmzx/embed?start=true&loop=true&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


### Shulaika La Cruise Final Project Video "AutomaticFishFeeder"
<iframe width="560" height="315" src="https://www.youtube.com/embed/B6BPzgQAQV8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
## Shulaika La Cruise Presentation Slide "AutomaticFishFeeder"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRSn5k-3QjEMLjj6wTUA4eb_282G9-6tV9X6udc3XTbrKfNkmAwZ-eDfxiaYj217wcN-Bxv_RxzLhS8/embed?start=true&loop=true&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Trandny Dunand  Final Project Video "AutoBin"
<iframe width="560" height="315" src="https://www.youtube.com/embed/jViG74sTdWI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Trandny Dunand Presentation Slide "AutoBin"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQobSnid4wDjRBUTp5YoeuEqK1Ywaniu2xTH6DrP252H1Wvr2zWTMmAHJC_94OGNwmZv8N6GJ2E0cZU/embed?start=true&loop=true&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Lydia Arjune Final Project Video "Mister Cooler"
<iframe width="560" height="315" src="https://www.youtube.com/embed/wnsYaPasf1w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
## Lydia Arjune Presentation Slides "Mister Cooler"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTieVICshP3RYJRLCe9P5WhoEtKrwm21UHTzCkTDBCuEzhlijNdtj-qpcaTz7LhnkfCpWi39Ghip3t7/embed?start=true&loop=true&delayms=3000" frameborder="0" width="560" height="315" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

