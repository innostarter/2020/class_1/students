
About me        
================

My name is Adjako Manischa. I was born in Paramaribo, Flora on April 21st 2001. I'm currently attending the E.P. Meyer Lyceum and aspire to become a pharmacist because I would love it if I could help others with what I've achieved. Each day I strive to expand my knowledge in every little thing. And that's why I wanted to participate in the Codettes Inno Starter. To expand my horizons and make good use of this opportunity given to young girls.

Day 1(Introduction): Basic blink LED
====================================

We were introduced to Arduino Uno and build a basic blink LED sample

We were to change the LED connected to 12 to 13 and see if it would blink after shifting it.

We were to change the speed of the blinking of the LEDs by adjusting the delay in the code. (e.g. from 1000 milliseconds to 2000 milliseconds)

Build a basic electronic circuit using the arduino uno to blink a led connected to digital pin 13. 

 Handson: (Circuits) Pictures
-----------------------------

![](https://lh4.googleusercontent.com/yocHLUwtqkPohsdJybqytALKuexZkpX6JvGUpkhMFceKdayvjTPng5d_Fb405c3ZqX4sYBF4DqKST-Lj9qphJrZCuDBODca-0YIVATpAetf4VMNB02DY9__UAKKQ0vDpfjoKYucRtK2liRwicDAuCEQ)

<https://photos.app.goo.gl/jUAcWJ8QbgjBhqNYA>

    Component list
------------------

-   Arduino Uno 

-   Breadboard(solderless)

-   LEDs

-   USB Cable

-   Jumpers

-   Resistor( 220Ω)

Code Block (Code Explained)
---------------------------

int switchState = 0;

             void setup(){

  pinMode(3,OUTPUT);

  pinMode(4,OUTPUT);

  pinMode(5,OUTPUT);

  pinMode(6,OUTPUT);

  pinMode(2,INPUT);

}

void loop(){

  switchState = digitalRead(2);

// uhhhh

  if (switchState == LOW) {

// the button is not pressed

    digitalWrite(3,HIGH); // green LED

    digitalWrite(4,LOW); // red LED

    digitalWrite(5,LOW); // red LED

    digitalWrite(6,LOW); //white LED   

  }

  else {  // the button is pressed

    digitalWrite(3,LOW);

    digitalWrite(4,LOW);

    digitalWrite(5,HIGH);

    digitalWrite(6,LOW);

    delay(250);  //wait for a quarter second

    // toggle the LEDs

    digitalWrite(4,HIGH);

    digitalWrite(5,LOW);

    digitalWrite(6,HIGH);

    delay(250);  // wait for a quarter second

  }

} // go back to the beginning of the loop

Mistakes made? What have you learnt?
------------------------------------

I mistook ground rail(-) for power rail(+) which resulted in my breadboard not getting any electricity and orders.

I learned how to use the Arduino Ide app and how to input codes.

Day 2: Series and Parallel Circuit and Spaceship interface
==========================================================

Series Circuit
--------------

Objective
---------

-   We were introduced to Series and Parallel Circuits

-   We were to build a few circuits to understand how the current flowed between Series and Parallel

-   We were to taught how to calculate the current with The Ohm's Law

-   We were then taught how to use a voltmeter and a multimeter in Tinkercad

Theory
------

Build a series and a parallel circuit and then observe with a multimeter what is actually happening to current.

Handson (Circuits): Pictures
----------------------------

![](https://lh5.googleusercontent.com/v-DW7nxeTVuNVfHYQ1ym1To_XR8DvRzE1MoA2vlPU1JO2GpNVhCj4rYrtCqGiIxpjqGuJ5KicJCLPI4FGOVCnb7tHr8dxqjNYV_1oQSQHLEuhDvkGuUFU3rJ5cyFSgO0Uj2wOwZkToP4updxY-B38CY)

First we started with one switch to show the basic LED circuit. Then we had to add a switch to the one already one the breadboard and wire them together in series as shown in the picture shown above.

We had to build the circuit in 4 steps:

1.  Connect the long leg of the LED to the second switch

2.  Connect the short leg of the LED to the ground (GRND)

3.  Power up the Arduino: turn the LED on and press both switches

4.  Voila! Your LED is now lit up

Component list
--------------

-   Arduino Uno

-   Breadboard(solderless)

-   Jumpers

-   Resistor (220 Ohm)

-   LED

-   USB Cable

-   Pushbuttons (2)

Code Block(Code Explained)
--------------------------

No codes were used in this circuit.

Parallel Circuit
----------------

Handson (Circuits): Pictures
----------------------------

![](https://lh5.googleusercontent.com/JcK-976mI0w6LRvY32c0nN2Um8VnHjvTuutLhAxtiA3tI9Cg8iOxbYQwcolmTh9r5OO9X13v1YfK70v6rv2_XGbEIdp863yb8wY7iTZYKk65wlfMxs4TtpKdgWtDdl8PGZeKIDTCuK_-7tuPV58VGAk)

After we learned how to build series circuits, it was time to start with parallel circuits.

We had to build in 4 steps (continuing with the parts of the series circuit):

1.  Keep the switches and LED where they are

2.  Remove the connection between the two switches

3.  Wire both switches to the resistor

4.  Attach the other end of both switches to the LED

5.  Now press either of the buttons

The circuit is completed and the light turns on.

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   Resistors ( 220 and 10K Ohm)

-   USB Cable

-   LED

-   Jumpers (10)

-   Voltmeters and Multimeter

-   Pushbuttons (2)

Code Block (Code Explained)
---------------------------

No codes were used in this circuit.

Spaceship Interface
-------------------

Objective
---------

-   We were introduced to Spaceship Interface

-   We were taught what the codes of spaceship interface indicated

-   Teaching one how to build a Spaceship Interface circuit

Theory
------

    Build a Spaceship Interface circuit using Arduino Uno and a Breadboard

Handson(Circuits): Pictures
---------------------------

![](https://lh6.googleusercontent.com/KRYQc6u4sLBwX7ltJXcrmxLql-1n4aAIlrvgqvXWFiVSnJrp-BXOKJuQNmbbVsfqqxA36BkWiX5bL5_fP7urBgnVi9eAFdY8Mabt9mWSnVe-kWW_nPsZ4MMBQeGV_ABhbl5Br6XtZFotnnPMVpIddtQ)<https://www.tinkercad.com/things/9e5A68as31u-surprising-jaagub/editel?sharecode=-ITmasyfxLbtD8MrI0cEyAb8J-2jtXH00m8glIOP2jw=>

<https://photos.app.goo.gl/KXxstrUnxmGmM1tb8>

Building the spaceship interface in 5 steps:

1.  Wire up the breadboard to the Arduino's 5V and ground connections

2.  Place the LEDs on the breadboard(Short leg of each LED to ground through a 220-ohm resistor and the long leg of remaining LED to a pin on the Arduino)

3.  Place the switch on the breadboard(at the center)

4.  Attach one side to power, and the other to a pin on the Arduino

5.  Add a 10k-ohm resistor from ground to the switch pin that connects to the Arduino

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   LEDs (3)

-   USB Cable

-   Resistors (220 and 10K Ohm)

-   Jumpers (9)

-   Pushbutton

-   Voltmeter

Code Block (Code Explained)
---------------------------

int switchState = 0;

void setup(){

  pinMode(3,OUTPUT);

  pinMode(4,OUTPUT);

  pinMode(5,OUTPUT);

  pinMode(2,INPUT);

}

void loop(){

  switchState = digitalRead(2);

// uhhhh

  if (switchState == LOW) {

// the button is not pressed

    digitalWrite(3,HIGH); // green LED

    digitalWrite(4,LOW); // red LED

    digitalWrite(5,LOW); // red LED   

  }

  else {  // the button is pressed

    digitalWrite(3,LOW);

    digitalWrite(4,LOW);

    digitalWrite(5,HIGH);

    delay(250);  //wait for a quarter second

    // toggle the LEDs

    digitalWrite(4,HIGH);

    digitalWrite(5,LOW);

    delay(250);  // wait for a quarter second

  }

} // go back to the beginning of the loop

Day 3: Continuation of Spaceship interface
==========================================

Spaceship Interface (Continuation)
----------------------------------

Objective
---------

-   We were to add extra parts to our circuit

-   We were to change the code so other parts could work

Theory
------

We were to add an extra LED and change the code so that the LED could light up and toggle.

Handson (Circuit): Pictures 
----------------------------

![](https://lh3.googleusercontent.com/aWXo647qFH3woyng1gAa3xhHhBGtSnPnUkYlWpqVOamC3LeR6yHXeYzLxgnrPCUWWxde1dBHRHdCzSXTR2n06aALUXaIdJiu4Pf0iidTNZygXXegKXZTIdRxPvWzBOZIjtrQy2VwkPbFpJqVPps-LgI)

<https://www.tinkercad.com/things/kGvZvoMSaPH-spaceship-interface/editel?sharecode=gIKIxW5dvuyEZuD5iRjFphSXWjTRWQmrLW0fZWEgz_0=>

<https://photos.app.goo.gl/PZ5bk1j7yWccx3778>

Component list
--------------

-   Arduino Uno

-   USB Cable

-   Breadboard (solderless)

-   LEDs (4)

-   Jumpers (10)

-   Resistors (220 and 10K Ohm)

-   Pushbutton

Code Blocked (Code Explained)
-----------------------------

int switchState = 0;

void setup(){

  pinMode(3,OUTPUT);

  pinMode(4,OUTPUT);

  pinMode(5,OUTPUT);

  pinMode(6,OUTPUT);

  pinMode(2,INPUT);

}

void loop(){

  switchState = digitalRead(2);

// uhhhh

  if (switchState == LOW) {

// the button is not pressed

    digitalWrite(3,HIGH); // green LED

    digitalWrite(4,LOW); // red LED

    digitalWrite(5,LOW); // red LED

    digitalWrite(6,LOW); //white LED

  }

  else {  // the button is pressed

    digitalWrite(3,LOW);

    digitalWrite(4,LOW);

    digitalWrite(5,HIGH);

    digitalWrite(6,LOW);

    delay(250);  //wait for a quarter second

    // toggle the LEDs

    digitalWrite(4,HIGH);

    digitalWrite(5,LOW);

    digitalWrite(6,HIGH);

    delay(250);  // wait for a quarter second

  }

} // go back to the beginning of the loop

LDR (Light Dependent Resistor)
------------------------------

Objective
---------

-   We were introduced to LDR and what its function is

-   We were taught how to map the code

Theory
------

Build an LDR circuit and find out what color the LDR will show when the light intensity is increased or decreased.

Handson (Circuits): Pictures
----------------------------

![](https://lh3.googleusercontent.com/hVFofCRpjKHH-RsHzJdtCGUFkzShvWoCkD6OwoDmzZ3jlM8QBQQj7rDI0hwLNnayKTatFaSOeGPsZDxS-MdpmY_wOrerQbXdX726sql2WMOT1pC_kYCJmHEi_Dkc9LfUd-Afh46zNooxtDcbWjIwmL8)

<https://www.tinkercad.com/things/9e5A68as31u-surprising-jaagub/editel?sharecode=-ITmasyfxLbtD8MrI0cEyAb8J-2jtXH00m8glIOP2jw=>

<https://photos.app.goo.gl/So5b3FuALxB8i7Wi7>

We had to finish this in 5 steps:

1.  Wire up the breadboard so it can have power and ground on both sides (just like other projects)

2.  Place the photoresistors on the breadboard so they cross the center to divide from one side to the other.

3.  Attach one end of each photoresistor to power

4.  Attach on the other side, a 10 KΩ resistor to ground

5.  Attach an RGB LED to the breadboard (Be sure to connect the longest leg to ground correctly)

Once you have your Arduino programmed and wired up, the LED will probably be an off-white color.(Depending on the color of the light near you)

Code Blocked (Code Explained)
-----------------------------

const int greenLEDPin = 9;

const int redLEDPin = 11; 

const int blueLEDPin = 10; 

const int redSensorPin = A0; 

const int greenSensorPin = A1; 

const int blueSensorPin = A2; 

int redValue = 0; 

int greenValue = 0; 

int blueValue = 0; 

int redSensorValue = 0; 

int greenSensorValue = 0; 

int blueSensorValue = 0; 

void setup() { 

 Serial.begin(9600); 

 pinMode(greenLEDPin,OUTPUT);

 pinMode(redLEDPin,OUTPUT);

 pinMode(blueLEDPin,OUTPUT);

}

void loop() {

 redSensorValue = analogRead(redSensorPin);

 delay(1000);

 greenSensorValue = analogRead(greenSensorPin);

 delay(1000);

 blueSensorValue = analogRead(blueSensorPin);

 Serial.print("Raw Sensor Values \t Red: ");

 Serial.print(redSensorValue);

Serial.print("\t Green: ");

Serial.print(greenSensorValue);

Serial.print("\t Blue: ");

Serial.println(blueSensorValue);

redValue = redSensorValue/4;

 greenValue = greenSensorValue/4;

 blueValue = blueSensorValue/4;

 Serial.print("Mapped Sensor Values \t Red: ");

 Serial.print(redValue);

 Serial.print("\t Green: ");

 Serial.print(greenValue);

 Serial.print("\t Blue: ");

 Serial.println(blueValue); 

 analogWrite(redLEDPin, redValue);

 analogWrite(greenLEDPin, greenValue);

 analogWrite(blueLEDPin, blueValue);

}

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   LDR(RGB)

-   Photosensors (3)

-   Resistors (220 and 10K Ohm)

-   Jumpers (16)

-   USB Cable

What is PWM (Pulse Width Modulation)
------------------------------------

Pulse width modulation (PWM), or pulse-duration modulation (PDM), is a method of reducing the average power delivered by an electrical signal, by effectively chopping it up into discrete parts.

Day 4 Continuation of LDR
=========================

Love-O-Meter (Temperature sensor)
---------------------------------

We were introduced to Temperature Sensor
----------------------------------------

We were then tasked to build a circuit practically and theoretically
--------------------------------------------------------------------

Build a Temperature Sensor circuit and find out what happens

Handson (Circuits): Pictures
----------------------------

![](https://lh6.googleusercontent.com/1rJy-AFW-z1Uf_Etv6PWxtVa9b1Ua_9ZrqzyMljgS3oXB3ejKZdxvlr17aiqsaj2N7QNmzuJclz6dvb-7IHs0fNaYaGSQxBODIFSXJnsxG8YAHZdGj9iostMrGtYWQCNaErQtM04mLRMUVP8MzBXBfA)

<https://www.tinkercad.com/things/dLqrvQBkgTG-love-o-meter/editel?sharecode=Hv4B_ENRirajOCgikbA1EgSowPBtmtknQCK42DK0byY=>

<https://photos.app.goo.gl/DpnJBSTFANmkzhcy5>

Components list
---------------

-   Arduino Uno

-   Breadboard (solderless)

-   Temperature sensor (TMP)

-   LEDs (3)

-   Resistors (220 Ohm)

-   Jumpers (8)

-   USB Cable

Code Blocked (Code Explained)
-----------------------------

#include<Servo.h>

Servo myServo;

int const potPin = A0;

int potVal;

int angle;

void setup() {

myServo.attach(9);

Serial.begin(9600);

}

void loop() {

potVal = analogRead(potPin);

Serial.print("potVal: ");

Serial.print(potVal);

angle = map(potVal,0,1023,0,179);

Serial.print(",angle: ");

Serial.println(angle);

myServo.write(angle);

delay(15);

}

Potentiometer and Servo Motor
-----------------------------

We were introduced to the Potentiometer and the Servo Motor

We were to understand the coding and how the parts communicate with each other

Build a circuit with a Potentiometer and a Servo motor

Handson (Circuits): Pictures
----------------------------

![](https://lh6.googleusercontent.com/EkInnjU7v7kQasPUdJeH-jrG4M6IXvWzBHdaAK_pTMJnhfi-8mqwMi1fh_2vrhN9yX3k1DfMx3penpGEymbPbmQj4Lezdvs6tLrDvnzKfocaw18HRd8_rnwIoA9i4K306T3c4uu9WTVN0yjv4pCC9Nw)

<https://www.tinkercad.com/things/dGqORK7BdTg-potentiometerservo/editel?sharecode=4e_d9pS0JW0y9MzsTHxRKgHGsLvJITz5DIDRDAR-LmU=>

<https://photos.app.goo.gl/YGceSNuNaZPwb7dMA>

Code Blocked (Code Explained)
-----------------------------

#include<Servo.h>

Servo myServo;

int const potPin = A0;

int potVal;

int angle;

void setup() {

myServo.attach(9);

Serial.begin(9600);

}

void loop() {

potVal = analogRead(potPin);

Serial.print("potVal: ");

Serial.print(potVal);

angle = map(potVal,0,1023,0,179);

Serial.print(",angle: ");

Serial.println(angle);

myServo.write(angle);

delay(15);

}

                         Map

Analoginput = 2^10 = 0-1023 (10 bits)

Analogoutput = 2^8 = 0-255 (8 bits)

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   Potentiometer

-   Servo motor

-   Capacitors (2)

-   Jumpers (11)

-   USB Cable

Day 5 Finishing Documentation
=============================

(Finishing LCD circuit homework from day4)

Basic Arduino LCD Text
----------------------

We were introduced to a basic Arduino LCD combination

We were to understand what the LCD was meant for

We were to build a Basic Arduino LCD circuit (Homework)

Handson (Circuits): Pictures
----------------------------

 ![](https://lh6.googleusercontent.com/RVAZfgtYVUjvbmdzC29gNycpggX_ho2By1ungzkJgWASgk3Qhrr5aSWW5EhfkH4PnOK3ZcW4ESf75g2YPUZVreg3TKqtjd1O_kZkslDVN4M5MGyTaAZZqXONviz__LQXA4cyu6U0-WuguyKx-jlUnZI)<https://www.tinkercad.com/things/4gP4MSGwRUR-basic-arduino-lcd/editel?sharecode=4ld-zsgefa5N1Gkt4er9VTHMjP5-Fm5MpUV3pPCA918=>

Code Blocked (Code Explained)
-----------------------------

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5, d6, d7) 

void setup() { 

 lcd.begin(16,2); // Initializes the interface to the LCD screen, and specifies the dimensions (width and height) of the display } 

}

void loop() { 

 //lcd.print("Arduino"); // Prints "Arduino" on the LCD 

 lcd.print("Hello_world"); // Prints "Arduino" on the LCD 

 delay(3000); // 3 seconds delay 

 lcd.setCursor(2,1); // Sets the location at which subsequent text written to the LCD will be displayed 

 lcd.print("LCD Tutorial"); 

 delay(3000); 

 lcd.clear(); // Clears the display 

 lcd.blink(); //Displays the blinking LCD cursor 

 delay(4000); 

 lcd.setCursor(7,1); 

 delay(3000); 

 lcd.noBlink(); // Turns off the blinking LCD cursor 

 lcd.cursor(); // Displays an underscore (line) at the position to which the next character will be written 

 delay(4000); 

 lcd.noCursor(); // Hides the LCD cursor 

 lcd.clear(); // Clears the LCD screen 

}

Component list
--------------

-   Arduino Uno

-   LCD

-   Potentiometer

-   Jumpers(12)

-   USB Cable

Day 6 Arduino LCD circuit and Build a project
=============================================

Arduino LCD circuit
-------------------

We were introduced to an easier Arduino LCD circuit

We were to learn how to simplify a Basic Arduino LCD

We were to learn how to make make the commands shorter and solid

Build an Arduino LCD circuit physically

Handson (Circuits): Pictures
----------------------------

<https://photos.app.goo.gl/RRhxFhxZKmzdkeDs7>

Code Blocked (Code Explained)
-----------------------------

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

void setup() {

  lcd.init();

  lcd.backlight();

  lcd.print("Hello World!");

}

void loop() {

  lcd.scrollDisplayLeft();

  delay(500);

}

Component list
--------------

-   Arduino Uno

-   Potentiometer

-   USB Cable

-   Jumpers (9)

-   Breadboard (solderless)

Build your own project: LCD and Servo motor
-------------------------------------------

Objective
---------

Build a circuit on Tinkercad with: 1. An LCD I2C

                                                     2. A Servo motor

                                                     3. An Arduino Uno

Handson (Circuits): Pictures
----------------------------

![](https://lh5.googleusercontent.com/j9r2VeFcAUBY_x684hwCmCI63I1cWKMGgtlx4hKhKdApG1nzq28ZHHq9PbD9WyL0rgfKcvNNApVqXIN8SadFhnKAa349QG15pDKgOjgYSPXTYgJjpP8qwz9ZFuw-6spsb-s5oki2hd1z3jNW8bfu3tM)

Code Blocked (Code Explained)
-----------------------------

I2C Board of LCD Arduino

GND <---> GND

VCC <---> 5V

SDA <---> A4

SCL <---> A5

#include <LiquidCrystal_I2C.h>

#include<Servo.h>

Servo myServo;

int potVal;

int angle;

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

void setup() {

  lcd.init();

  lcd.backlight();

  lcd.print("Motor ON!");

  myServo.attach(9);

Serial.begin(9600);

}

void loop() {

lcd.scrollDisplayLeft();

delay(500);

Serial.print("potVal: ");

Serial.print(potVal);

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   Potentiometer (2)

-   Servo motor

-   Jumpers (18)

-   USB Cable

Day 7:  H-bridge motor control
==============================

Objective
---------

-   We were introduced to H-bridge

-   Learn how to build an H-bridge practically and theoretically

Handson (Circuits): Pictures
----------------------------

![](https://lh6.googleusercontent.com/pGn9of_75j_AWDGMnu5launPeTUM8SmyPkott7TyiyA0E5SnWWSx3IWpQLRlmpueKGGg9aCf2Xtc5DhN82XaLjOyQDgFDdEHD6NPkVmwb3Mv-FTaLBIslbdQGTjRlM8_73MXOcDMfUbttYEM-3Htw1Y)

<https://www.tinkercad.com/things/1HNlnsL1OvT-grand-gaaris/editel?tenant=circuits?sharecode=X7oSkc20bUV8Hs52EIdy2dLRgXMI8V20i7cNNkVED0c=>

Code Blocked (Code Explained)
-----------------------------

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

}

void loop() {

   //For Clock wise motion , in_1 = High , in_2 = Low

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

   analogWrite(pwm,250) ;

   /* setting pwm of the motor to 255 we can change the speed of rotation

   by changing pwm input but we are only using arduino so we are using highest

   value to driver the motor */

   //Clockwise for 3 secs

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

}

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   Jumpers

-   L 293D (Motor Driver)

-   Potentiometer

-   Voltmeter

-   Multimeter

-   DC Motor

3D Designing
============

Objective
---------

-   We learn about designing enclosures

-   We learn how to use a 3D printer

Theory
------

First you create a file for your design. Then choose the available shapes and forms for your enclosure. Be sure to use boxes and holes. Lastly keep an eye on the measurements.

Handson (Circuits): Pictures
----------------------------

![](https://lh5.googleusercontent.com/b2wu_BAnuS1gCKUgd2UWrhQgj8zLPpHCyFCHagCzs1O8pTZLEJgXoabaj5CCT8an5y2aKbdYyR7e9eSBFS1WEiYbhfXxFDisgLO0FeFGXmlUUmSSL2ZKyuskmv3HkA7DeuAOyYjCv-1UfaGh_NQnjsw)

<https://www.tinkercad.com/things/6CUlVxHYYoC-arduino-case/edit?sharecode=Tz86EwAUwVAH21JWxxR-bIouBkYaytKvKyM2FTs6zSU>

Final project: RFID Door Lock 
==============================

Handzoff
--------

![](https://lh3.googleusercontent.com/1xXq99p8R_yo_bHB3EtzYChB0AGUgvkyffzD2l0HspP6P-zYsNbPRURI4ZkRLxegXHe1_E5xIZstFv8NDuqY9-h7pun7en5LuJv-_kjOOgz-BhH0butOqtF-rP-V0V6m_A9uaZZ_5B2z5Ed8yTMFJnU)

-   Is an RFID lock, also called the electronic hotel lock or smart card lock

-   Is convenient, hands-free  as well as risk-free.

-   Is designed to transmit a signal on a specific frequency to the card reader, via a small device known as a smart chip embedded inside the plastic.

-   Once the lock on the door has received this signal from the card reader, it can then be opened using the door's lockset or push-bar, as it would normally be.

Handson (Circuits): Pictures
----------------------------

![](https://lh5.googleusercontent.com/SDVDzoIf845IP1hgN6YINDVv8D2j2MiH2YshsMj_ifIAWZLFg5I_glorBGltxyO5h-_bHEPbke7Dbvl-pGPY2PBsrWMyzwv5BdSKTZg4Q61vYLi-DlUeiiv9BKIATJUrYsAauPn6MGDj6xhR0atw_m8)                                                                    Tinkercad        3d          design

   ![](https://lh3.googleusercontent.com/cQQm-c94nnDbJbaP4m4o7tvFxIn1wyP0DaJY-h-CMTdSSbvUnhoCiXZI2waoIoQPNYZEVJtXEAg1NYobjGfRzN-gwIUl9XWzUQZ22Ho3gc9FNwpObEBdV_r9Rop4R0FLzoJO5-G-RfuvAnDt3V2VuCs) ![](https://lh3.googleusercontent.com/f0echVzCo1KIaINuX5u0WqWNZd1SVHYh9LxW6XippwG1qESxU83hmjPAn1jrDHi4ey57LBIXLkERuSxRTxwZMa3sGE6uDtih-XD_fFSWUjWbREfDH7lKinjXi2fhWZLHHnUTFqqsBMxvHmdcCWd5lIM)

     ![](https://lh4.googleusercontent.com/Er6swq5L_ThhMOae2KIIr51w9rlMolnwWl0-Vm_8dhIczFFJI6e0y0QFlkJZc3swQu9fcndlA7dONHROLGfkbB6IQG1r_6IaTtneW1dUZzSlnm6xEYhOM7R2jVMrhSb_Gfx0E-PGo3h9ng6SupTxfVU)  ![](https://lh6.googleusercontent.com/QH8RJXJ_bOVO46VrOSucYXd1FDiwy23M-9xqyto1jzr5BXph-xDBvGulUnT7wJKE1jQPvpoLxLUkmyqNwk5tIROZzUHMNi12d14aqmr1rwCdbyTFyO5KP2WwFrozeA-EVTLh-pFRXv20ekwWAJU2YQ0)

Code Blocked (Code Explained)
-----------------------------

#include <SPI.h>

#include <MFRC522.h>

#include <Wire.h>

#include <LiquidCrystal_I2C.h>

#define SS_PIN 10

#define RST_PIN 9

#define solenoidpin  3    //defines solenoid pin 3

#define ACCESS_DELAY 1000

#define DENIED_DELAY 1000

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup()

{

  pinMode(3, OUTPUT); //sets solenoid as Output

  lcd.init();

  // initialize the lcd

  lcd.init();

  // Print a message to the LCD.

  lcd.backlight();

  // Print a message to the LCD.

  lcd.setCursor(0, 0);

  lcd.print("Put your card");

  lcd.setCursor(2, 1);

  lcd.print("to reader");

  SPI.begin();          // Initiate  SPI bus

  mfrc522.PCD_Init();   // Initiate MFRC522

}

void loop()

{

  // Look for new cards

  if ( ! mfrc522.PICC_IsNewCardPresent())

  {

    return;

  }

  // Select one of the cards

  if ( ! mfrc522.PICC_ReadCardSerial())

  {

    return;

  }

  //Show UID on serial monitor

  //lcd.print("UID tag :");

  String content = "";

  byte letter;

  for (byte i = 0; i < mfrc522.uid.size; i++)

  {

    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");

    Serial.print(mfrc522.uid.uidByte[i], HEX);

    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));

    content.concat(String(mfrc522.uid.uidByte[i], HEX));

  }

  Serial.println();

  Serial.print("Message : ");

  content.toUpperCase();

  if (content.substring(1) == "D9 47 0D BA") //change here the UID of the card

  {

    digitalWrite(3, HIGH);  //sets the solenoid into HIGH state

    delay(2000);            //duration 2 seconds

    digitalWrite(3, LOW); //sets the solenoid into LOW state

    delay(4000);            //duration 4 seconds

    lcd.setCursor(0, 0);

    lcd.print("Access granted");

    lcd.setCursor(2, 1);

    lcd.print("to customer");

  }

  else   {

    lcd.setCursor(0, 0);

    lcd.print("Access denied");

    lcd.setCursor(2, 1);

    lcd.print("to customer");

    delay(DENIED_DELAY)

  }

}

![](https://lh3.googleusercontent.com/wObYwgCiKakGiso9u6lUyqalZ80WISUmrTs4NLdg4MD6dSrdHTFJvcLMvIz2CYAIbN_SDZAcaXAynIZIqnOXdDPoR6Scy_A_WRW5_F40lpzCiFpracg6GRE9h3lYQVpv0ZwDDTtHpC9VmFfEOfNahrI)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

3D printer parameters
---------------------

![](https://lh4.googleusercontent.com/nJBL4Ojrg6TgdpAqV-1z3Xg5N3gZjlzFg01W8JeGDwCW-8AP7luKRgsJIQLQ_xvoea8rFnpcnLHQYANtFk8YDdD9YRoXhkkx4aUhWBMIqWds3tld63ApruFnla2aCcqEoDB5JLe4mvvd8jQgIuKFuog)

![](https://lh5.googleusercontent.com/SQZT0jYmXVwoGPLW-lKihmKzeqeRHOrlMuKE2MP9YDSMLwa3T2Z_RhMNVizaaCLvu4TylGosyQ91p_V8m17JGY6w3gVWHyUwJOvOY5kBxBgrfDkbH7s_A8im-4glyHgQiVNSeLOPUWQlz6F4XGY2vtc)