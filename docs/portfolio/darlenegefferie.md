
About Me

![](https://lh6.googleusercontent.com/Oo3SsD_dCvo2td-DhDfDZrJHTnohVKrb9MfQeYI0Lp_OsteXEwRG-vuGcOL-uVuCmpduTaucoMrnxxJ-UpcG3kzKYK-ZRjwEwQ8wChZQ2mxbu3QPGVuorJegPNEFcUJ-s1lviatXezlH8whT0RsIbA)

My name is Darlene Gefferie. I am 20 years old.  I graduated from Ewald P Meyer Lyceum in 2019. I did Electric engineering at Anton de Kom university but realized I wanted to do more coding.

I always wanted to learn to code and learn things about the technology world.

I choose codettes to expand my knowledge about coding. I want to do software engineering in the future.

Day 1

Objective:

-   Introduction to Arduino Uno build basic blink led sample on a breadboard

-   Change the circuit to blink led connected to pin 12 to 13

-   Change the speed of the blinking of the LEDs by adjusting the delay in the code

Theory:

Build a basic electronic circuit on a breadboard using the Arduino Uno to blink a led connected to digital pin 11.

Component list:

-   Arduino Uno

-   LED

-   Resistor(in tinkercad 220Ω / with the kit 200Ω)

-   jumpers

-   breadboard

Code BlockCODE

pinMode(LED_BUILTIN, OUTPUT) ; // LED_BUILTIN is where you put the  pin you can change the pin number (the pin number i  used  was pin number 11)

} // the loop function runs over and over again forever

void loop() { // the loop begins

digitalWrite(LED_BUILTIN, HIGH); // turn the LED on (HIGH is the voltage level )HIGH means on 

delay(1000);  // wait for a second, the time can be changed

digitalWrite(LED_BUILTIN, LOW); // turn the LED off by making the voltage LOW , LOW means off

delay(1000);  // wait for a second

}

The Arduino app provides the blink code

Hands-on:

Open the Arduino app 

The code that was used for the blinking circuit was found in file-> examples-> 01.basic->blink

Select your port and your board before you upload the code to your Arduino Uno.![](https://lh5.googleusercontent.com/AjJqN8ojyKmKKOlykPx8gJY8TOrFArERJpg-uyQ0nmlT3I2AO4XTqDv5DCcjw90hK0O3Y9t5S8s9oKSf-sb1i8MXOJsRALX6S5Nok7PfEoROjs0woiLw9wc4CZ-We-hdnGrPBDdFEc0qBTr0o3cElQ)\
![](https://lh3.googleusercontent.com/7sGsxQ3KVo4AJBGWzjZP4snbQyV9r9VvjkuJtU4HiSqSAayuk3rqUY19kqoIjUzbCDOIRIYC6tBlcEyFYvbM45n6A5h1kHr_dieENdUqbfOhXReOldKXl8WSfcWHyHNb2OUhbLWR5g55rF9cHItGMA)

![](https://lh5.googleusercontent.com/BMvMP45fkT3yGiossNJFjkjR2UZsuJEAItWy_I6T626bysmdIxzIKbbQc2TtKV3DHMvIMUW1ARukcv_Kh2FiryCoBZmdS424cC3SXmUKIGqsU-2Id3ln2vHCe4V4h8pe6mOBDz8iai2veEXUsEW9PA)

![](https://lh3.googleusercontent.com/9srESeGIwrF8DdC2uwhUsiDC-2Ovne96Jhfj9p1G7j596dYjTFb9kjLS_VEImLXQMKkGahYADJa7Vq1elepIWpZcEE8a2yP8dGUwZ4Icnzj8-2M1rXSX3fNK1WIDL1bwmYQq2HhoMh7UP5q-VtItYg)

Circuit built on tinkercad

The resistor is connected to the anode pole (positive side, long side) of the LED and the cathode pole(negative side short side) is connected to your ground ( minus - on the breadboard)with a jumper. Connect the resistor with a jumper to pin number 11. Give the breadboard power, use 2 jumpers and connect one of them to 5v on the Arduino Uno and the other side to the plus + on the breadboard. Connect the other to the ground and to GND.![](https://lh5.googleusercontent.com/5ltMN8_xX1zg7rl83vw7rwK3J5Ss9D_z1nNXSBs4zLPv0ABVhCkOs_FCu87uDa_qE8S4CIYvOuziXojVe-kFBX4B4PPxqWHcpY7pIqYNjEILiaubHxGLJrVLWJWshsmlIIfVEeNf6LHvKivB1qcrsw)

Day 2

Objective:

-   Build a series circuit on tinkercad

-   If you push both buttons the led will turn on

-   Build a parallel circuit on tinker 

-   If you push one of the buttons the led will turn on

-   Build the spaceship interface on tinkercad

Component list:

-   Pushbutton

-   LED

-   Arduino Uno

-   Resistor

-   Jumpers

-   Breadboard

Mistakes Made?

I thought that if you put the push button next to each other the circuit it would be in serie circuit and if you put the push button under each other  you'll have a parallel circuit

Conclusion

The jumpers are used to put the components in series or parallel.\
![](https://lh6.googleusercontent.com/f7WUSq71CTmP3iGHnojgTZtJqAzh7AIpeCgIdfvZupKHxkAzFwQWIrK5rqmhNrCLnkEIsh84SpcnrjxxnQiiznRnPmXNhlEqa0v_a3bw43xcYbUc3V_x17EK5g7lpw0TCgKBtn0We6rhKkUMgej1Lg)

![](https://lh5.googleusercontent.com/m_QSwVZfur6k1fO_Iugy_eS3ZFd0YKgZpwGH910osP0yhYk0O_aZ_6LMlm7LakM0TMVO9b6DIgvSyUklNApOA-Ay8PW3S0dXtFeRXsUOp_3DexmvWPfYjPu3tylW76nWEnDxKW2JQgb59I7EMbMztQ)

Resistor is pinned to + plus on the breadboard

Power Jumpers + on the breadboard are connected to 5V on the Arduino Uno. - jumper is connected to GND on the Arduino Uno

![](https://lh5.googleusercontent.com/crgCAfRm4_FhLyLV32fr9eJTiiYoV4R3RrEU5Z-FYGmy9rKlnWaQD_tXAkVK5Iab_Xu77ITZaEgt_pd7W--E47xKiGzSxBZlDZhLw-3MJaER3QmsFvAelxBRhY0TZigKpe14nqcc8IHCj4d2u32B4Q)Transistor: is an electronic component that can be used as an amplified switching element in an electronic circuit (fast switch)

![](https://lh6.googleusercontent.com/ULJMs2n7ywj_9DnomvIx817buT75U27LNnQwzmWeTWdhp_N4G3-ZXvWB4Vh1e7jajqJJF6pqU9gX2nDxXH9xbPVO06TTXI6XJgE2edlD7KU2JnBCbgS_FB0Jth23mH2xBsDF-FqJ8M87n6NZeUyGOw)Capacitor: is an electrical component in which electrical charge can be stored. (delivers Fast power)

Weerstanden

1kΩ pull up

10kΩ pull down (maakt stroom kleiner) wordt gebruikt voor saftey zodat de dingen niet zomaar aan de code gaan

![](https://lh6.googleusercontent.com/t-GlmpF4gKMKgqf-XlfyAqC9067fg3vm_EL4fstKo7uUCno82dcyqM8VeGZOMMbx61USt0t77z6eD__meZ4ZsYkXRPmHSm7fVoR0Ead6EySZbrYCx-_t6BS8FNCTj-5AZ3OvB2LuPn8r4ElZRMGBDg)

![](https://lh4.googleusercontent.com/CcZad0Vw9ju7glAWN9uwv51Jaepg8mHu9yco-e-U0kiOKcwOvuG9deAOHWuKnEjwthQUyZ0DAs7pBtU1r41PazuqD5rTjQnLdwq4_1VAD8ti-nLMi0KWDMQhC5aav4eXYyXvp50EA8ZHOrYUiUytPg)

![](https://lh5.googleusercontent.com/WtMdotFXmqC7xacnxvxOU3bmip-Y8sgZIBxUUbTdO3LWDhyE34r8cjNAC5eaWYzybRaAq_jWqvBKR0dUzmeXp27AFiF4YbT-cC8PERb5tdvqEIySmUCBt2j304dMMsCi8L5wBFF2k4wcCc2YAQli2g)![](https://lh4.googleusercontent.com/RUEXcS6zoKSISrs3gTAnOTo2p2pvrg2-XmjlaZB9LMU41PXkLSjs3Rbv_S34xs0rInR56tiBri0dNFPrItS2Oh8Bk_nB84tQWzTWLEeRraYrfRbFwjkWsT5K4rTfUwFAHp8xNcDNHTgtdB8r9B1wYQ)\
![](https://lh6.googleusercontent.com/0L6awOSLFOIyYEwRdOxZ14wjY-JMNliKKCngfMJnOqZaWoMKnmzP1vJ-CoEiufIsm4Qap07hd0EnmW25J0DDxkBt3QJ4C7mdHvYlMamWCrxG12F8Zs2BUrjahXg_LtzeXPg3ygKPinYfXdaajpRsSw)![](https://lh6.googleusercontent.com/0RFsO14NFuHF5S9KmkhbEv5qa3gmoeothMAIH1xcZuM5NBFTXiFlsj5IytQbJtgiLLcJuEkLZ5cVGzdEB9M3Kvx9YDWTkmVMFra90dxe9ZwWoeofmVz8Gmc3KUgxzYu1BBl9RPfFdNb6dPUcmKEarA)

 ![](https://lh4.googleusercontent.com/0Y7PPzTbUMtoWd3j5XVHdSByWt6SkBSdiuaNm7_duX1oNRC0JdDPvSwWkqbragMhSd81ute6a5Bj3wvawXN2J2xx03Kl87Fj7BfpYBJT0Gk4p3SPYFVnSxowhu_Yc4bhSpzPJXBVYXWUiAreU9w_qw)![](https://lh5.googleusercontent.com/GaHkPjuhAynCYGZq4-1L1100Zm8uFEY015tvi9rSLZmWxhVCcLvvCiWDIx7PjOPraObTZJx8CpKjzF-Ff4agGIP1Qh3lH_hcFlTl18Cwr8SjPRNmVY5oC3MLsRXkP_1WjDSzhxPc75nAyzzoL4CaPQ)

Day 3

Objective: 

-   Build the spaceship interface using the kit

-   The green led should be on

-   If you push the button the 2 red led should be toggling

-   Build  LDR RGB using the kit

-   The photoresistor that gets light should transfer to the LDR and should give the corresponding color

Theory: Spaceship Interface 

Build a circuit connecting the LEDs to 3, 4 and 5. Put a resistor of 10kΩ connected to the pushbutton.

Components list:

-   LEDs

-   Pushbutton

-   Jumpers

-   Arduino

-   Breadboard

-   resistors\
![](https://lh5.googleusercontent.com/SL6XwyeRtoad-TPDFlpv6ky1r4cGlmRx-jAvwPcK6TXPUFQBYC-0d495cYDlUomr309veyz2oIftCZS8wFaxBIn7SG5jBAEiMq8bNZh7VROU4ULnlsmny9YyzrI6Fi8Gyx06SP2PDDBrlHZJ78g0pA)

![](https://lh6.googleusercontent.com/MQlkxu8RTlT8wvvCUvovMqBhqdZViOFcRT6PLOIyUwMY6B-M1zp3r0xvWGj8FrvHAqQLWY_Vb8BNxQYQUe965JzTteSwuCAGJ3OyPYTammHQi3tC4TvRoIdckYSgC3kJtuhPt6b2idA0sHtz1DdyFw)![](https://lh6.googleusercontent.com/avx0uZCPbu105SsnuXdA4Z3lc8vpS9n8fXSco_UCBYJpFY-A_ftAwSP4rUu9D2UcylLnBw6U-4GOjK8tDi9C0o6VJ8GSCJnlftpfhp-UFpL7XsEA9neVp1GigpdFEjd14oWyD1lpMcF83vYtr7VLLg)

CODE

int switchState = 0; // integer can store a whole number

void setup(){

pinMode(3,OUTPUT);

pinMode(4,OUTPUT);

pinMode(5,OUTPUT);

pinMode(2,INPUT);

}

void loop (){

switchState = digitalRead(2);

if (switchState == LOW) // the button is not pressed

digitalWrite(3, HIGH); // Green LED is on

digitalWrite(4, LOW); // Red LED is off

digitalWrite(5, LOW); // Red LEDis off

}

else { // the button is pressed

digitalWrite(3, LOW);

digitalWrite(4, LOW);

digitalWrite (5, HIGH);

delay(250); //wait for a quarter second

// toggle the LEDs

digitalWrite(4, HIGH);

digitalWrite(5, LOW );

delay (250); // wait for a quarter second

}

} // go back to the beginning of the loop

Weerstand bij de pushbutton gedraagt zich als een pull down

LDR RGB

Build the circuit connecting the  photoresistors (greenLED)to Pin 9, (redLED) Pin11, (blueLED) Pin10.

Component list

-   Photoresistor

-   LDR RGB

-   Jumpers

-   Breadboard

-   resistors

Mistakes Made?

We used the opposite resistors as a result, the light did not shine as sharply.\
![](https://lh4.googleusercontent.com/Khm1A0j9NYPbfVDnvfzB-xBI5wc_Lri0xmAEA9NbfqO3TGZHw5TeHyOmJFB02r1wYhnjE3s_HNhL5hvUDWEherKQNOhtrkTwirQp2b3C0YfwYEnIW4PlX-ko9C_aaapfDWhWX-v0Q30R5f6KJ5k7Lw)

![](https://lh4.googleusercontent.com/Y8veM2JN95dBma7GZ_F0iWwfwlR6PLUIrSpUDwIL4XIT31MQnlqw_zeJ8GsAPf8tTsjg_S24ZB5t9qucWzGzgQp8bEYD8hRM5O9g8mxkqk27PZkktPpDEXbQOZxDySHrFe4Q8KrYiZi19pZ4N-4tvw)

CODE

const int greenLEDPin = 9;

const int redLEDPin = 11;

const int blueLEDPin = 10;

const int redSensorPin = A0;

const int greenSensorPin = A1;

const int blueSensorPin = A2;

int redValue = 0;

int greenValue = 0;

int blueValue = 0;

int redSensorValue = 0;

int greenSensorValue = 0;

int blueSensorValue = 0;

void setup() {

Serial.begin(9600); //speed

pinMode(greenLEDPin,OUTPUT);

pinMode(redLEDPin,OUTPUT);

pinMode(blueLEDPin,OUTPUT);

}

void loop() {

redSensorValue = analogRead(redSensorPin); // read the input pin

delay(50);

greenSensorValue = analogRead(greenSensorPin);

delay(50);

blueSensorValue = analogRead(blueSensorPin);

Serial.print("Raw Sensor Values \t Red:"); 

Serial.print(redSensorValue);

Serial.print("\t Green:");

Serial.print(greenSensorValue);

Serial.print("\t Blue:"); // gives blue

Serial.println(blueSensorValue);

redValue = redSensorValue/4;

greenValue = greenSensorValue/4;

blueValue = blueSensorValue/4;

Serial.print("Mapped Sensor Values \t Red: ");

Serial.print(redValue);

Serial.print("\t Green: ");

Serial.print(greenValue);

Serial.print("\t Blue: ");

Serial.println(blueValue);

analogWrite(redLEDPin, redValue);

analogWrite(greenLEDPin, greenValue);

analogWrite(blueLEDPin, blueValue);

}

![](https://lh5.googleusercontent.com/gaxTryLyZ79L54WE3JNDUSPfT7BbuXhGxCRV1IdHYNdq4oY8VAiRjWQOTqoniIR5n8eJb19LMtHjZHkE957-g_bg11qhZaF81LvO4jdeB06GucAkM3ExsZYf41moOBPBmMxgXEhG0vgjq0JznLOV6A)Photoresistor: A light-sensitive resistor or LDR is an electrical component whose resistance is influenced by the amount of light falling on it. The resistance value of an LDR becomes smaller as the LDR is exposed more strongly. As a result, the value of the resistance can vary greatly.

AnalogWrite

int redPin=9;

int bright=127; // 0-255 "2^8 bit number / 2^8 different positions or increments that you can do

                // 0 = 0v  

// 255 = 5v

//127 = 2.5v

void setup() {

  // put your setup code here, to run once:

pinMode (redPin,OUTPUT);

}

void loop() {

  // put your main code here, to run repeatedly:

  analogWrite (redPin,bright);

}

Map

Analoginput = 2^10 = 0-1023 (10 bits)

Analog Output = 2^8 = 0-255 (8 bits)

Day 4

LOVE-O-METER

Objective: 

-   Build the love-o-meter (indicates the temperature) using the kit

-   If the temperature is lower than the base temperature the LEDs should be off

-   If the temperature is  higher/ equal to the base temperature+2 & if the temperature is lower than the base temperature +4 one led should be on

-   If the temperature is higher/ equal to the base temperature+4 & if the temperature is lower than the base temperature+6 two LEDs should be on

-   If the temperature is higher than base temperature + 6 all three LEDs should be on

Components list: 

-   LEDs

-   Resistors

-   Breadboard

-   Arduino Uno

-   jumpers

![](https://lh5.googleusercontent.com/R3g9IKFTz4UM4ikqq00EBC5qc4mLw-SQEfV-pvPkl5DJrvYInfpjZm67Rdn41fofP-oAvzRej-wIQFmtrU2m8HzSTQ8eWyAaK-n_s2NNvBu5kn7YlWkPiiyT3lDxW32My7KDp62ivltMs9zjd7Hgjg)![](https://lh4.googleusercontent.com/lTV0UnMO2Qj2uVkWMfuyoWhP5-O0DG9-xGYLBr-3oGtSl9l883MZlPHWSNEp9Kx0WjnmepQL3CkdpPmflUD06WEA-nAXsPhEV2EGXAcTse6I0R-BAj6umTNDIudztwJSBIYQcUWZY2ozPEuDB0jNEw)

CODE

const int sensorPin = A0;

const float baselineTemp = 25.0;

void setup(){

Serial.begin(9600); // open a serial port byte per seconde

   for(int pinNumber = 2; pinNumber<5; pinNumber++)

   {

   pinMode(pinNumber,OUTPUT);

   digitalWrite(pinNumber, LOW);

    } 

}

void loop(){

   int sensorVal = analogRead(sensorPin);

   Serial.print("Sensor Value: "); 

   Serial.print(sensorVal);//convert the adc reading to voltage

   float voltage = (sensorVal/1024.0) * 5.0;

   Serial.print(", Volts: ");

   Serial.print(voltage);

   Serial.print(", degrees C:"); 

 // convert the voltage to temperature in degrees

 float temperature = (voltage - .5) * 100;

  Serial.println(temperature);

if(temperature < baselineTemp)

{

   digitalWrite(2, LOW);

   digitalWrite(3, LOW);

   digitalWrite(4, LOW);

  }

else if(temperature >= baselineTemp+2 && 

  temperature < baselineTemp+4)

{

   digitalWrite(2, HIGH);

   digitalWrite(3, LOW);

   digitalWrite(4, LOW);

   } 

 else if(temperature >= baselineTemp+4 && 

   temperature < baselineTemp+6)

 {

    digitalWrite(2, HIGH);

    digitalWrite(3, HIGH);

    digitalWrite(4, LOW); 

  }

 else if(temperature >= baselineTemp+6)

 {

   digitalWrite(2, HIGH);

   digitalWrite(3, HIGH);

   digitalWrite(4, HIGH);

   }

  delay(1);

}  

Micro servo

Objective:

-   Build the micro servo circuit using the kit

-   If you move the potentiometer the servo is also going to the direction your turning the potentiometer

Components list:

-   Arduino Uno

-   Breadboard

-   Jumpers

-   Potentiometer

-   Micro servo

-   Condensator

![](https://lh3.googleusercontent.com/IBHf3lqnQZ36vq8WI9VjngSUlBEfG-INcKPrQNAWAPnvCu5ol-OfwJdskRIYM0caTMF6tAYxToHxeZ9CQqTduGpK73sVd1FU9SFBxSrWd47McMv_OiytElk59cavXWg9OwNvpsmyX_DMfDzge-n2ow)

CODE

#include<Servo.h>//directive tells the preprocessor to insert the contents of another file into the source code

Servo myServo;

int const potPin = A0;

int potVal;

int angle;

void setup() {

  myServo.attach(9);

  Serial.begin(9600);

}

void loop() {

  potVal= analogRead(potPin);

  Serial.print("potVal:");

  Serial.print(potVal);

  angle= map(potVal,0,1023,0,179);

  Serial.print(",angle:");

  Serial.println(angle);

  myServo.write(angle);

  delay(15);

}

Short code 

myServo.write(map(analogRead(A0),0,1023,0,179));

Day 6

Objective:

-   Build a LCD circuit

-   Build a LCD circuit (with shift)

Arduino Lcd Basic Text

Components list:

-   Jumpers

-   LCD

-   Potentiometer

-   Arduino Uno

CODE

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5, d6, d7)

void setup() { 

lcd.begin(16,2); // Initializes the interface to the LCD screen, and specifies the dimensions (width and height) of the display }

}

void loop() { 

 //lcd.print("Arduino"); // Prints "Arduino" on the LCD

 lcd.print("Hello_world"); // Prints "Arduino" on the LCD

 delay(3000); // 3 seconds delay

 lcd.setCursor(2,1); // Sets the location at which subsequent text written to the LCD will be displayed

 lcd.print("LCD Tutorial"); 

 delay(3000); 

 lcd.clear(); // Clears the display

lcd.blink(); //Displays the blinking LCD cursor 

 delay(4000); 

 lcd.setCursor(7,1); 

 delay(3000); 

lcd.noBlink(); // Turns off the blinking LCD cursor 

lcd.cursor(); // Displays an underscore (line) at the position to which the next character will be written 

 delay(4000); 

lcd.noCursor(); // Hides the LCD cursor 

 lcd.clear(); // Clears the LCD screen 

}

![](https://lh3.googleusercontent.com/OT-gxb_RsHwKnUqaoGrZVoVU6RkUCmHoVrzYjKgBP2LJ9uw5PvB1XjKeJzAVpVftgSa43UMzxONlpV7PV0RcOTRu894tDBD_VD3pEwGba3v2_KjWgzkqhAxYNnWWaDizg2xCzcFcahEeibCJlQn5Ag)

LCD I2C

 Components list:

-   Jumpers

-   LCD

-   Potentiometer

-   Arduino Uno

CODE

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

void setup() {

  lcd.init();

  lcd.backlight();

  lcd.print("Hello World!");

}

void loop() {

  lcd.scrollDisplayLeft();

  delay(500);

}

![](https://lh3.googleusercontent.com/9zr3DRy0v80XOT4N5ABeG1yhBw05vjyZ9kdxlvNmPOG1Ewb0YpFdS-CmQnrjM8ZyqVU_YpA1btQJa9wn1sDmrx0Wptdb9z-LZhBO261AtgVqUcy4vpJTnp8FAL0FMMcFOou-atC1jZ0AhW3oPhm9pQ)![](https://lh3.googleusercontent.com/wHSUrn7RGIm_aBeqlRa8imKAhj7F1bylZReTsFJF1lUN3cVJoG3CuwO8pXPh2amu0mduhFYhqyt8X5Nr7-O3Dk6Ec9Q9gfS2lZOKfqJ0bAP_istNxG-tBpRZaxzqfnIeBIiE7Y7YA2TaXtj8d4BMgA)![](https://lh5.googleusercontent.com/SqJCTvJfJMhJok5GwMubruNfetsCeVlET_XZnt2jm0LhgWKTrLIptKUWVlPzyn62jfk3TyU3wiwwAdy6ozP5lQsmZa3YEG-qgk-GuVHV9GltEdpwC_LPfRhgdEZvHODOMBGVAIOWPmTDjVo3KgXxsQ)![](https://lh5.googleusercontent.com/gxoOgiZGu-TBnrEKO91JV4rOUZKpTd6XWSwWjyUIc9SJCrgzI8r2mLCpoBr9nHPfMbZoF6GwXHS-N9EMXoAHWsFHNOgPXMVya8TYJe1KONlr5cQ4fnDMAceSPOJ6MkYMlYsGdMMkj5sI7NAmED7L7A)

Own Project

Objective:

-   Build a circuit using LCD I2C 

-   Add a pushbutton

-   If button is pushed the screen text will scroll to the left

-   If button is not pressed the screen text will scroll to the right

Code

#include <LiquidCrystal_I2C.h>

#include <Wire.h> 

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

int switchState = 0;

void setup() {

  pinMode (2,INPUT);

  lcd.init();

  lcd.backlight();

  lcd.print("Darlene & Shanies!");}

void loop() {

  switchState = digitalRead(2);

    if (switchState== LOW){

  lcd.scrollDisplayRight();

  delay(500);

    }

   else {

     lcd.scrollDisplayLeft();

  delay(500);

   }

   }

Day 7

Objective:

-   Tinkercad 3D design

-   Design a case for your arduino uno

-   Make in tinkercad a H-bridge

-   Make in tinkercad a h-bridge with potentiometer

![](https://lh6.googleusercontent.com/oQZKiFlIeyHRrVvHBEFQxFraQkA8l39UNEglsqwFY0wafaVa0GSQrAF3iqlO-R2geUyq39_12sFiM_PsJ1Fm-uH2w_hlQyDITzYdxBaCvxR3D5toCVjw6TmMYFHcK2_eAlWYWDwsLPovdTPnsdSzVg)![](https://lh3.googleusercontent.com/qRImX7gSNKJTR408yLAbZxoBalahIKwfqVUjRbAnLgNVk3Kcu5VNcHXLexYDguES5o1WgirhlfpqRuF2e3RsHPvuPEJ1N807knQPO17aHgn3MPUOzkS7vHK0ez-o4NLDpQgGRZkhlB7D8PRkK7cVfg)![](https://lh5.googleusercontent.com/5znc8KBUkLrLH56VPxFT8inzcgENEzN7yFoCiYDPLdLa9n_WPZBrbeFzIzZTB-el27ZZj01DFD--WvIqHfaNMYbQI_RotgqBU7iCTWMH4ljU8Zlii8_Bcq8oknvWtDGxtercIyClae7YSgy76Om9wA)

H-bridge with potentiometer

Component list:

-   Arduino Uno

-   H-bridge L293D

-   DC Motor 

-   Power supply

-   Jumpers 

-   Breadboard

Code BlockCODE

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

pinMode(pwm,OUTPUT) ; //(pwm=pulse-width modulation)we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

   Serial.begin(9600);

}

void loop() {

  int duty= map(analogRead(A0),0,1023,-255,255);

  Serial.println(duty);

  analogWrite(pwm,abs(duty));// abs break

  if (duty<0){

//turn Clockwise

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

}

if (duty<0){

  // turn counterclockwise

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

}

if (duty== 0){

  // break

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

}

}

![](https://lh6.googleusercontent.com/hfePG7IgpXK6HEa5-booXOFTwQt4IRevF6srw6qAaq5ratYkrgTMMw9UeD7VTlVR73ayr-jcBl0s3adzpy5r_YnRU58ppxePtLZK3asBbSkhRGravFZXr7nC_jLyiH7v_Rs-yHKWSzpPW-q2T4R4Mg)

H-bridge 

Component list:

-   Arduino Uno

-   H-bridge L293D

-   DC Motor 

-   Power supply

-   Jumpers 

-   Breadboard

![](https://lh6.googleusercontent.com/_W0j2OjpBiexReqn2xmNVdec-3riRqxTPSap7oOQ6yaPb8B1j2lWF_ZBffEh93fuEIlhlGYUj-C17T3D4fgBY5Tea2CTbQRPcVeZ8xkgs_zQHA91Qv7Oa9HWsOzdXokpxwf7kIdba-aM3zVs7NsHvw)

Code BlockCODE\
const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

pinMode(pwm,OUTPUT) ; //(pwm=pulse-width modulation)we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

}

void loop() {

   //For Clockwise motion , in_1 = High , in_2 = Low

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

   analogWrite(pwm,250) ;

   /* setting pwm of the motor to 255 we can change the speed of rotation

   by changing pwm input but we are only using arduino so we are using highest

   value to driver the motor */

   //Clockwise for 3 secs

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

   //For Anti Clockwise motion - IN_1 = LOW , IN_2 = HIGH

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

}

Work

Code

const int trigPin = 9;

const int echoPin = 10;

long duration;

int distance;

void setup() {

pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output

pinMode(echoPin, INPUT); // Sets the echoPin as an Input

Serial.begin(9600); // Starts the serial communication

}

void loop() {

digitalWrite(trigPin, LOW);

delayMicroseconds(2);

digitalWrite(trigPin, HIGH);

delayMicroseconds(10);

digitalWrite(trigPin, LOW);

duration = pulseIn(echoPin, HIGH);

distance= duration*0.034/2;

Serial.print("Distance: ");

Serial.println(distance);

}

![](https://lh5.googleusercontent.com/Y-tt32wK-kXcYYkesAPWBB8HRenkTSj7HOO2-yavCow63n2ijuGUNFBuYc6XI0bBvYYP5OQywJIzRyPGjGOgowO5n9lMC5G0DKZF03Arbv-yWIETa5Ft1t9LV4WzU4OZS3Vyr60uLgt5krCfrQHK3A)

![](https://lh5.googleusercontent.com/b1r2bUWA5w7-USOebp2ndluLXKdmESLprqPp4tO8CXnpcIxpMVG8E0w0dYYRE3jxlIlYCMITKGppt6fIMWhZ9Tut-H-HjzOwo6QRX5ZY8AHd61T0ZglxdG8N97930QQxyH1gNUTDSM0LW1MjWwxZLQ)

Project Research Environment

Arduino Uno Automatic hand sanitizer Dispenser

Problem

The Coronavirus COVID-19 has spread very quickly throughout the country in recent times. As a result, a number of measures have been taken. When you step into shops or companies etc. your temperature is often measured and you have to disinfect your hands. Many places use a spray bottle that a worker sprays for you or you have to spray it yourself. if the person entering the place has to do it himself, there is a high risk of spreading because many people touch the same bottle.

Solution 

Our Automatic hand sanitizer dispenser is contactless so that the virus can be spread less. It is easy to use and very effective.

How does it work?\
so you have to put your hand under the dispenser 10 cm or closer and the sanitizer will come out for 2 seconds.

### Supplies:

-   Arduino Uno

-   Jumper

-   Breadboard

-   9G Micro Servo

-   Ultrasonic Module HC-SR04 Distance Sensor

#### ![](https://lh4.googleusercontent.com/PfZRuWeH1ZP0Td85_dPHZlwM_Fc8zOpeX65oCjmjF6YgKFtW2ZKyIqL2_Rupeg_6thFlfThIvgXyZ4lfEXpq4bspNoCP8_ci15uNLQtIe4pW440sXEG3cSdPNm0AV4cTsSlXNLJGWdl_bHT0cSRl5Q)![](https://lh5.googleusercontent.com/szyrIBDYZTJPzk02H-1qJfumqrZX87LWSZr0C3KGj76ds8uQp-fMqFfXqmfVv-hQfMLnn3GdJP98fqUd0cz-9lQhkMTNNnPlis1yRKUP3RwRRfUi0ECF76hvk1qvfz8NpszA_8Hnz5pDtS4SmbNlcw)![](https://lh5.googleusercontent.com/VXr2ltLUN8lmIAt1mqZ9B_IZpPxm4Ua0Xal77x1OuNRWmEawpIkDo0FUN-A1c_oICGQNLxL0ioIZ2AaD2xQbcdswHUMtA25kvbhvW7NxZFoxK_VhNt6c2jGuknUp88FxOiPvTQm4g76UeV9Wq8t3wA)![](https://lh6.googleusercontent.com/wp2XBLKrPLgxDMKIp7MR3SzFuTtZgMDveRncOr0Jik-T1uQQHsLbdC6Pw1pgEAQRtiatqY1HKeZGgbwdy8vNoHMuqXglAmc9S9WQ8nyR_lBD14IQoK2UOf9bIN-FBWgX4LqNy5cook1uiuxh6sIRAA)![](https://lh3.googleusercontent.com/UUBA-oC-1Vx_bfpH_E-GgvgoJED3wptGJFO-zgtoQg0N2d2S2m2EoGrS89dJ6LNKuhxo1SuHI5L3tw8n7aXW-HuEqRI-L8St7wYtX2lKkBamiUhLCau1glEsmMRvmMRGaAKvMnNt2fup2mUf3YtHjA)![](https://lh3.googleusercontent.com/Jws6GQekRHVzzgLtDeEIPDy4HWTNjlqpLRbdcWVD_y90XoipiZN-dBfK5LHW9emHw3uoUj6UdssRmtgLKv-4-YF_ox6LTrLqOU-294UVfYubuOE9Lm1QH6gHi6CBObLtbagVc1hDjPAwJ6JoDzHi8A)

![](https://lh6.googleusercontent.com/Xn7GKVS446uW4CYNayXrNakpfpF5CCPkkc0jj6cBEMmOYZ1jB7aHD8R9c5kKsXrkYkHd6lqPx8zlbFcCOjdijfB1fIC1kEQ8_vjSLEA9s97WGaxYImOvhbm3oXYvA2fjQ7INENwsj5OAcvYpg9sCDQ) 

![](https://lh4.googleusercontent.com/3IPI9O_5QhHAerwHU-_o-IBIefevP_r-bqGIo3j5nH6va9d8NeJQ16g6muDoVq9JqeHqpFY22Fn7lk4_duMXTmEiRCRJ7H3XK8MDOJLZo2DSECnhdTgJAT3fJdqSiif5bHz3kblO3XLwuPximRANeA)

![](https://lh4.googleusercontent.com/DHTqmj9W_8c1j8USDn_m7abhFDgf4UudPexIRX3bAWEXZvpzTHh0PC9voH2ue7oBxT_odZ807DDSZWCM7ePEqetf_Va6brRZjv4W2rzg0Pu8v6hET5ePTbHfisJbstX_jyt2u23pN42-_EaZFsP5fQ)
=============================================================================================================================================================================================

![](https://lh6.googleusercontent.com/vehFE85jQytOcy4xelUa0xmWZH12PiwLTkGk4nGbKznqzSqviqdinh2Ye2dNu459GKE0yCojuQKqfoK0ERMMHrLpmJxKWGgY2SWMWDvQSPRrpr9MED0PQU3ta3b-8XUBCe-n6dQNIuendZcC-Fdv4w)![](https://lh5.googleusercontent.com/3OQnGsmNbF8-HWrgxpPvID9Bds_xmZhU0Zpso3GL2P90NVRQd6RgUUQzj9HtadW4ItPMFaw3iF5uO5MVyinHSl80cWizeW4H6xzvW60dicvsdsRROFZfnfDNloRRn-XbmaKtISqJS4KiGDxJRJ3Vpw)

![](https://lh4.googleusercontent.com/rzymBUlXuqB1X4AC4OYXR6WKExmbjm_FEEWhHbeh2ciQijcFxSbYfxyOVlihjYfF9Wl8f8CdwdrgCKtIDEW9hvo77jX5oVFEaU_FOiHdMNLczlbg2a3V9fDGu9rTRWXuXM78HMWV7_MLrBxSMg8yuQ)![](https://lh6.googleusercontent.com/e3wZj2j67URVeE4gfSzFYXPifn9L7ItOLfisuy_b9OozHyZCg5B9AFgxkRfmeXNVMRHxO3dq4_ISqL3qAyr4aiSIn0JFjlFp_bIRIO4EwY0Kv8LR8HwF2HI-ZOFk9QOpNgQIloMETyt560vSB_3Ivw)
==========================================================================================================================================================================================================================================================================================================================================================================================

 CODE

#include <Servo.h>

Servo servo1;

int trigPin = 10;

int echoPin = 11;

Int servoPin = 9;

long distance;

long duration;

void setup() 

{

 servo1.attach(servoPin); 

 servo1.write(90);

 pinMode(trigPin, OUTPUT);

 pinMode(echoPin, INPUT);

 Serial.begin(9600);

}

void loop() {

  Serial.print("Distance: ");

  Serial.print(distance);

  Serial.println("  cm");

  ultra();

if(distance <10 && distance >0 ) // if ultrasonic sensor detects less than 10cm but bigger than 0 (in case error).

  {

    Serial.println("Dispensing soap");  

    servo1.write(33); 

    delay(2000);

    // go back to rest state

    Serial.println( 90);

    servo1.write(90);

    delay(3000);

  }

}

void ultra(){

  digitalWrite(trigPin, LOW);

  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);

  delayMicroseconds(10);

  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  distance = duration*0.034/2;

  }