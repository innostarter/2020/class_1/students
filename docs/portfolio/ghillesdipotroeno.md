
About me

My name is Ghilles Dipotroeno

DAY 1

Objective :

-   Electronics Theory

-   Law of Ohm

-   Function of the components

-   Simulations Tinkercad

-   Series and Parallel Circuit

The Law of Ohm

Ohm's law states that the voltage or potential difference between two points is directly proportional to the current or electricity passing through the resistance, and directly proportional to the resistance of the circuit. The formula for Ohm's law is V=IR. This relationship between current, voltage, and relationship was discovered by German scientist Georg Simon Ohm.

Tinkercad

Before we build our circuits with our kits we have to build them in [Tinkercad](http://tinkercad.com) first.

It is a free online 3D modeling program that runs in a web browser.

Hands On

Series Circuit :

Components used :

-   Arduino Uno

-   Breadboard

-   LED

-   Jumpers

-   Resistor

-   Switches

After I removed my power source (you always have to remove your power source first before you remove, add or replace stuff on your Arduino and breadboard) I added two switches to my breadboard and wired them together in series. 

1.  Connect the anode (long leg of the LED) to the second switch 

2.  Connect the LED cathode (short leg of the LED) to ground.

3.  Power up the Arduino again and turn on the LED by pressing both switches 

4.  Since these are in series they both need to be closed for the LED to go on

![](https://lh6.googleusercontent.com/zu2-0MmuxsTbz8kVriCPWpv7TFnjuPW_3AAuCT1Ujju-Rp9QUNoDFLDrjSx0NKMeKKqtOkcrGuKsC4dFv0O2KvSLDCzheHeoXMu-udx0sI9GrzmeqrBXL_1JqkCvfrEzbBu7dGePj1jaAXtWZzKFac8)

Parallel Circuit :

Components used :

-   Arduino Uno

-   Breadboard

-   LED

-   Jumpers

-   Resistor

-   Switches

Now that I had learned to wire the switches in series, I had to wire them up in parallel.

1.  Keep the switches and LED where they are

2.  Remove the connection between the two switches

3.  Wire both switches to the resistor

4.  Attach the other end of both switches to the LED 

5.  Now you press either button

6.  The circuit is completed and the LED turns on

![](https://lh6.googleusercontent.com/RostBtGmpLVh79avGaFDwoHYoluDJAaui0j_2GbsFKas0ylwWi_VVmH2hHMh7STa04kQjhOTxTkBb26YzSVp7y_uv7xeiaHpXIYmIkT2bpEADIb426LXQIscVr5kI_C_aVVWDKXU71tJ3_J5HlvdmQg)

DAY 2

Objective :

-   Spaceship Interface

-   LDR Circuit

Spaceship Interface

![](https://lh6.googleusercontent.com/dIjplZxr5Z_lCX2KBlawS0uGpSQW3tSGrDm2J8_0RlePgUQ4lnOsRzmN3ui_tTR4eoFK3GXCeiFz6g6qnS22bjC5MbSpboZYYsPdq6iC29I2lgOTiI9IFbsiyLqObkzZdMSMk-jWzvYAQtgTvfd94Go)

LDR Circuit

![](https://lh4.googleusercontent.com/445aXNbWS_JZPQICDMd4ewK_EaWFccP7BZMZ_VsDEHBs7qUwn_uQ5J83OdOxQ0EGnxpTwnBHItTMTstw-RzJIeLAhXDXBHzFNprk77ywtdPH4u2WJC6r_ivZLf2MIc1Um9MIR_ZadfM5onoP2jZ3vls)

Hands On

Spaceship Interface :

Components used :

-   Arduino Uno

-   Breadboard

-   LEDs

-   Jumpers

-   Resistors

-   Switch

1.  I wired up the breadboard to the Arduino's 5V and ground connections. Placed two  red LEDs and one  green LED on the breadboard and then attached the cathode of each LED to ground through a 220 ohm resistor. Connected the anode leg of the green LED to pin 3 and connected the Red LEDs anodes to pin 4 and pin 5.

2.  I placed the switch on the breadboard and attached one side to power and the other side to digital pin 2 on the Arduino. I also needed to add a 10k ohm resistor from ground to the switch pin that's connected to the Arduino. That pull down resistor connects the pin to ground when the switch is open, so it reads LOW when there's no voltage coming in through the switch.

![](https://lh4.googleusercontent.com/E1PXRBk4Aq2Psc6JyoirXx2eDYzNKWX6u56ECePrrGWQh6NJkQkZyI4Abir193ZxDY0TUcLmB4jQxQBku8gV9ff7Zn2MIpfEs_2ogwkaSsE_vIdsIH9JAncTb7bGxJ8cEzwGFXwPmE0CXgtXJcxO2SY)

LDR Circuit :

Components used :

-   Arduino Uno

-   Breadboard

-   RGB LED

-   Jumpers

-   Resistors

-   Photoresistors 

-   Green, Red and Blue Gels

1.  I wired up my breadboard so I had power and ground on both sides

2.  I had to place the three  photoresistors on the breadboard so they cross the center divide from one side to the other. I then attached on end of each photoresistor to power. On the other side I attached a 10k ohm resistor to ground. The resistor is in series with the photoresistor, and together they form a voltage divider. The voltage at the point where they meet is proportional to the ratio of their resistances, according to ohm's law. As the resistance of the photoresistor changes when light hits it, the voltage at its junction changes as well. On the same side as the resistor, connect the photoresistors to Analog In pins 0,1 and 2 with hookup wire.

3.  I then had to take the three coloured gels and place one over each of the photoresistors. I placed the red gel over the photoresistor that is connected to A0, the green gel over the one connected to A1, and the blue gel over the one connected to A2. Each of these filters lets only light of a specific wavelength through to the sensor it's covering. The red filter passes only red light, the green filter passes only green light and the blue filter passes only blue light. This allowed me to detect the relative colour levels in the light that hit my sensors.

4.  The LED with four legs is a common cathode RGB LED. The LED has separate red, green and blue elements inside, and one common ground. By creating a voltage difference between the cathode and the voltage coming out of the Arduino's PWM pins (which are connected to the anodes through 220 ohm resistors), it'll cause the LED to fade between its three colours. I had to make note of what the longest pin on the LED was, placed it on the breadboard and connected the other three pins 9,10 and 11 in series with 220 oh resistors. I had to be sure to connect each LED to the correct PWM pin.

Code Block

Spaceship Interface Code :

// this is a comment

int switchState = 0;

void setup(){

pinMode(3,OUTPUT);

pinMode(4,OUTPUT);

pinMode(5,OUTPUT);

pinMode(2,INPUT);

}

void loop(){

 switchState = digitalRead(2);

if (switchState == LOW) { 

 // the button is not pressed

digitalWrite(3, HIGH); // green LED

digitalWrite(4, LOW); // red LED

digitalWrite(5, LOW); // red LED

 } 

else { // the button is pressed

digitalWrite(3, LOW); 

digitalWrite(4, LOW);

digitalWrite(5, HIGH);

delay(250); // wait for a quarter second

 // toggle the LEDs

digitalWrite(4, HIGH); 

digitalWrite(5, LOW);

delay(250); // wait for a quarter second

 } 

} // go back to the beginning of the loop

 LDR Circuit Code  :

const int greenLEDPin = 9;

const int redLEDPin = 11; 

const int blueLEDPin = 10;

const int redSensorPin = A0; 

const int greenSensorPin = A1; 

const int blueSensorPin = A2;

int redValue = 0; 

int greenValue = 0; 

int blueValue = 0;

int redSensorValue = 0; 

int greenSensorValue = 0; 

int blueSensorValue = 0; 

void setup() { 

Serial.begin(9600); 

pinMode(greenLEDPin,OUTPUT);

pinMode(redLEDPin,OUTPUT);

pinMode(blueLEDPin,OUTPUT);

}

void loop() {

 redSensorValue = analogRead(redSensorPin);

delay(5);

 greenSensorValue = analogRead(greenSensorPin);

delay(5);

 blueSensorValue = analogRead(blueSensorPin);

Serial.print("Raw Sensor Values \t Red: ");

Serial.print(redSensorValue);

Serial.print("\t Green: ");

Serial.print(greenSensorValue);

Serial.print("\t Blue: ");

Serial.println(blueSensorValue);

redValue = redSensorValue/4;

 greenValue = greenSensorValue/4;

 blueValue = blueSensorValue/4;

Serial.print("Mapped Sensor Values \t Red: ");

Serial.print(redValue);

Serial.print("\t Green: ");

Serial.print(greenValue);

Serial.print("\t Blue: ");

Serial.println(blueValue); 

analogWrite(redLEDPin, redValue);

analogWrite(greenLEDPin, greenValue);

analogWrite(blueLEDPin, blueValue);

}

DAY 3

Objective :

-   Love O Meter 

-   Servo Motor

Love O Meter

![](https://lh4.googleusercontent.com/pv39wkkIevfhYIcxOz1Tj_bjwkJoCLhwEfoI0IEJjjYU72tZxKDGud647FVvyo5ZKx78oyftfHKJp-2BKMdt_KHavHfZJvR4JaJ_j65Y69jtstyeDORKD_IyJkCQgR_jZRbXdLnhHrUuu0MRaHeVOjM)

Hands On

Love O Meter :

Components Used :

-   Arduino Uno

-   Breadboard

-   LEDs 

-   Jumpers 

-   Resistors

-   TMP36

1.  First I wired up my breadboard to power and ground.

2.  I attached the cathode of each LED to ground using a 220 ohm resistor. I then connected the anodes of the LEDs to pins 2 through 4. These were the indicators of the project. 

3.  I placed the TMP36 on the breadboard with the rounded part facing away from the Arduino. I connected the left pin of the flat facing side to power, and the right pin to ground. I then connected the center pin to A0 on my Arduino. This is analog input pin 0.

![](https://lh6.googleusercontent.com/7GqNVlWzohTItwR2eDDD0AykFLy4JWcd7cqy2Sc-dW1Rk-TvOIiO9KpmFqtQ5Didl85Vs6kpBJVlNDYuwCrrvPBWDvq5wntgKPwpl9F9JiSuty8hpbX7eic5tCeOgM0ZxSlUps82Wv0FNrDcwUIr16w)

Servo Motor :

Components Used :

-   Arduino Uno 

-   Breadboard 

-   Jumpers

-   Potentiometer

-   Capacitors

-   Servo Motor

1.  I attached 5V and ground to one side of my breadboard from my Arduino.

2.  I placed a potentiometer on the breadboard and connected on side to 5V and the other side to ground. A potentiometer is a type of voltage divider. As I turned the knob, I changed the ratio between the middle pin and power. I could read this change on an analog input. I connected the middle pin to analog pin 0. This controlled the position of my servo motor. 

3.  The servo has 3 wires coming out of it. The first one is power, the second one is ground and the third is the control line that will receive information from the Arduino

4.  When a Servo Motor starts to move, it draws more current than if it were already in motion. That will cause a dip in the voltage on the board. So I placed a 100uf capacitor across power and ground right next to the male headers and that smoothened out any voltage changes that couldve occurred. I couldve also placed a capacitor across the power and ground going into the potentiometer. These are called decoupling capacitors because they reduce or decouple changes caused by the components of the rest of the circuit. I had to make sure that I was connecting the cathode to ground and the anode to power because if I put it backwards it could explode.

Code Block

Love O Meter Code :

const int sensorPin = A0;

const float baselineTemp = 20.0;

void setup(){

Serial.begin(9600); // open a serial port

 for(int pinNumber = 2; pinNumber<5; pinNumber++){

pinMode(pinNumber,OUTPUT);

digitalWrite(pinNumber, LOW);

 } 

}

void loop(){

int sensorVal = analogRead(sensorPin);

Serial.print("Sensor Value: "); 

Serial.print(sensorVal);

float voltage = (sensorVal/1024.0) * 5.0;

Serial.print(", Volts: ");

Serial.print(voltage);

Serial.print(", degrees C: "); 

 // convert the voltage to temperature in degrees

 float temperature = (voltage - .5) * 100;

Serial.println(temperature);

if(temperature < baselineTemp){

digitalWrite(2, LOW);

digitalWrite(3, LOW);

digitalWrite(4, LOW);

}else if(temperature >= baselineTemp+2 && 

 temperature < baselineTemp+4){

digitalWrite(2, HIGH);

digitalWrite(3, LOW);

digitalWrite(4, LOW);

 }else if(temperature >= baselineTemp+4 && 

 temperature < baselineTemp+6){

digitalWrite(2, HIGH);

digitalWrite(3, HIGH);

digitalWrite(4, LOW);

 }else if(temperature >= baselineTemp+6){

digitalWrite(2, HIGH);

digitalWrite(3, HIGH);

digitalWrite(4, HIGH);

 }

delay(1);

}

Servo Motor Code :

#include <Servo.h>

Servo myServo;

int const postPin = A0;

int potVal;

int angle;

void setup() {

myServo.attach (9);

Serial.begin (9600); 

}

void loop() { 

potVal = analogRead(potPin);

Serial.print ("potVal: ");

Serial.print (potVal);

angle = map(potVal, 0, 1023, 0, 179)

Serial.print (" , angle: ");

Serial.print (angle);

myServo.write(angle);

delay(15);

}

DAY 4

Objective :

-   Arduino LCD Basic Text

Hands On

Components Used :

-   Arduino Uno

-   LCD

-   Jumpers

-   Potentiometer

-   Resistor

-   Breadboard

Basic LCD Design :

![](https://lh4.googleusercontent.com/Qn-5aLotrJNK8oW-EOzviY0AwBAKLAf4GDfB-Csc1pypV2UTImzm-KSKlBsryEgapPmytmzeUH1Szk1Uo7C1lSbqaNZFGFaWD_Qamf9GA3mayk5DXKWc25SSCPzZpcboNifN63vog48C93NIoJ5tVpE)

LCD I2C

The advantage of LCD I2C is that the wiring is very simple. You only need two data pins to control the LCD.

GND ------------- GND

VCC ------------- 5V

SDA ------------- A4

SCL ------------- A5

Code Block

#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins

LiquidCrystal lcd(12,11,5,4,3,2);

void setup() {

// set up the LCDs number of columns and rows:

lcd.begin(16,2);

// print message to the LCD

lcd.print("i am chwnex!");

}

void loop(){

// set the cursor to column 0,line 1

// (note: line 1 is the second row, since counting begins with 0):

lcd.setCursor(0,1);

// print the number of seconds since reset:

lcd.print(millis() / 1000);

}

DAY 5

Objective :

-   Tinkercad 3D Design

-   H Bridge Motor Control

3D Design

We had to create a box or house u could say for our Arduino Uno using 3D design on [Tinkercad.](http://tinkercad.com)

![](https://lh5.googleusercontent.com/5KCNN248LUb-qhGwgn24Aa88ZWyfrDEp1cY6Amifrv6LUK2kgUVQPWmFa8bGvMHu0Tv8U-81JbSaRF7UjaMfE8oIunbMlCElslNDUkObTS15we5dDRJ16RJIVUhTvOMo9xmWQc70BP3yd04JTk0T76A)

![](https://lh5.googleusercontent.com/15Z8wTev7uvbfk8abto1O4ukip88tP_k8LwIBxA1BQX6_R_flRDbv_CJUUbq3P8A4UAQjRd8Vf5BcB5fKWHkwIj6a41BvIsMZetf975ST-h2DoAqYOphlMmKHAnZcEw9GD-cu8x66bk0IszXFqDTX1k)

Hands On

H-Bridge Motor (Without Potentiometer) :

Components Used:

-   Arduino Uno 

-   Breadboard 

-   Jumpers

-   H-bridge Motor driver

-   DC Motor

-   Power Supply

An H-bridge is a simple circuit that lets you control a DC motor to go backward or forward. A DC motor spins either backward or forward, depending on how you connect the plus and the minus. If you close switch 1 and 4, you have plus connected to the left side of the motor and minus to the other side. And the motor will start spinning in one direction. If you instead close switch 2 and 3, you have plus connected to the right side and minus to the left side. And the motor spins in the opposite direction.

![](https://lh5.googleusercontent.com/MFB3lFrAKF_S4y6in0ohLSFd24dZDQRQ2XAhEVg9quaNmHBKpaOEVc58KlUN6-cbB8fe5WFIRdF1DdBjGqAmW-G1C_ExwxYhYOE4z3L7k_rPa7OYER6_MAyLl7uHjklUIO0UrmoZbOIQ-ufyooVH9B8)

H-Bridge Motor (With Potentiometer) :

Components Used :

-   Arduino Uno 

-   Breadboard 

-   Jumpers

-   H-bridge Motor driver

-   DC Motor

-   Power Supply

-   Multimeter

-   PotentioMeter

![](https://lh4.googleusercontent.com/uBdx5YZ0WxKTJWalH3wPrlepDtLyteaLFtVJ7lhqyT2KSSDbbEMQjQD1gXtD3lOYKrFkW-RMQ0IPGa0SUUz0uEUdyGXeXXtKVlCB0SBgUA62-OUaTTrbifAhBRndZTdwTbYmvyqYdSzuV46fsPmSIaw)

Code Block

H-bridge Without Potentiometer :

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

}

void loop() {

   //For Clock wise motion , in_1 = High , in_2 = Low

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

   analogWrite(pwm,250) ;

   /* setting pwm of the motor to 255 we can change the speed of rotation

   by changing pwm input but we are only using arduino so we are using highest

   value to driver the motor */

   //Clockwise for 3 secs

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

}

H-bridge With Potentiometer :

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

   Serial.begin (9600) ;

}

void loop() {

  int duty = map (analogRead (A0),0,1023,-255,255);

    Serial.println(duty);

  analogWrite (pwm,abs(duty));

  if (duty>0){

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

  } 

  if (duty<0){

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,LOW) ;

  }

  if (duty==0){

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

}

}

FINAL PROJECT

For our final project me and shyfka decided to make a Temperature Meter.

Components Used:

-   Arduino Uno

-   Breadboard

-   Jumpers

-   Resistors

-   Push Button

-   LCD

-   Potentiometer

-   Mini Traffic Light

-   Temperature Sensor

![](https://lh3.googleusercontent.com/BDrf2IoQJobJyzygwoNygYfjtOWl9oQxqt_YglbsRZm1fX0h7DymRY_ZlbT-I9ZUCUWw5Z2WuIxTkH5VD0-DBCCntOFXfWyKxK2TWf9UxNLD1grRa46IGvHZgISoTPvxuO7JJRC1TPZ_S206G5qSKb4)

Whats Happening

Due to the recent Covid-19 outbreak we have decided to make a Temperature Meter.

When plugged in and the Push Button is pressed, it will measure the temperature and display it onto the LCD. The Mini Traffic Light is used to check if the temperature is high or low. If your temperature is high the red led will be on and if the temperature is low / ok the green led will turn on. The yellow led will turn on if the temperature is between high and low.

Final Project Prototype :

![](https://lh5.googleusercontent.com/ASJUz81O-rZOaGH7TYtZ_aqvDC8TodxQFZ1Ka0fIzb0IMo0RWLB1au2n-gPF8KjTMBomoVOWdjU-T95HqIA1_wEAWNHpbOfa8SCMMvPwaDCmI0kqmhMFljO6tW7Io1SrYp4nxaytEQo_eshywqtfQR8)

Code Block

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

const int yellow = 9; 

const int green= 10;

const int red = 11;

int switchState= 0;

const int inPin = 0;                   // A0 is where you connect the sensor

void setup()

{

 pinMode(green,OUTPUT);

  pinMode(yellow,OUTPUT); 

  pinMode(red,OUTPUT);

Serial.begin(9600);

  //lcd.begin(16,2);

  lcd.init();

  lcd.backlight();

  //lcd.print("Test2!");

}

void loop()

{

  lcd.scrollDisplayLeft();

  switchState= digitalRead(8);

  if(switchState == HIGH ){

  int value = analogRead(inPin); // read the value from the sensor

  //lcd.setCursor(0,1);

  float millivolts = (value / 1024.0) * 5000; 

  float celsius = millivolts / 10;

  //lcd.clear();

  //lcd.setCursor(0,0);

  lcd.print(celsius);

  lcd.print("C");

  //lcd.setCursor(0,1);

  lcd.print((celsius * 9)/5 + 32); //turning the celsius into fahrenheit

  lcd.print("F");

  delay(1000);

  Serial.println(celsius);

  if ('celsius' <37){

    digitalWrite(green,1); 

    delay(2000);

    digitalWrite(red,0); 

    digitalWrite(yellow,0); 

  }

    else if{

    digitalWrite(green,0); 

    delay(2000);

    digitalWrite(red,1); 

    digitalWrite(yellow,0); 

      }

}

else {

//if (switchState==LOW){

 Serial.println("Turn on to measure the temperature");

}

}