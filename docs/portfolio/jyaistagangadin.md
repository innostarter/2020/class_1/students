
My name is Jyaista Gangadin

13 years old 

Class B3

![](https://lh6.googleusercontent.com/akhHRMY3tFMkSEVDVa05iKmKx8MuKfq73-MhscQVONwXISp7acHa1TU50msY3DJNNIlzMgn7b1a4RgMcZFk1-vBf2SeBnjpfXbfHd8RShaevwBlTs6GpUe64QnLgNPljg5YWwA8Ue4Vm6KaK_Kv3Sw)

Day1

-   Objective:

-   Introduction to Arduino uno build basic blink led sample

-   Chang circuit to blink led connected to pin 12 instead of 13

-   Change the speed of the blinking of the leds by adjusting the delay in the code

-   Theory: Build a basic electronic circuit using the arduino uno to blink a led connected to digital pin 13. 

-   Handson: (Circuits) Pictures

-   Component list

-   Arduino uno 

-   Breadboard

-   Leds

-   Jumper

-   Resistor

-   Code Block (Code Explained)

-   Mistakes made? What have you learnt?

Hands On

I started out building my circuit using tinkercad circuits.

The components which I used was the arduino uno microcontroller a breadboard which is a solderless circuit  a led which is a light emitting diode a 220 ohm resistor and some jumper wires.

I made a power rail on my breadboard by connecting the 5v from the arduino to my plus rail on my bread board and gnd rail by connecting the gnd from the arduino uno.

I then connected my led anode led to my digital pin 13 in parallel with my resistor and the cathode leg to the ground pin

![](https://lh5.googleusercontent.com/knkuWITxqWUDkEQNajAyba73hAWAUEkOOO4hcpywUsL5mFOuCBuXnDnvxF6qSOPpKFNYeiYk0EKFxyzGg_jfiLmmVXJA_3joveSwJjaUxxcTCS71jY246jsPxQliiAFJf9ow1JCGfcwo63xAndabxQ)

Code Block

I then added my basic blink led sketch to blink the led connected to pin 13

I have 2 functions the void setup and the void loop

The setup runs once and the loop runs over and over again

In my setup I initialize the digital pin 13 as output

In my setup I turn the digital pin 13 high using the digitalWrite function for 1 second then low for 1 second and repeat.

// the setup function runs once when you press reset or power the board

void setup() {

  // initialize digital pin 13 as an output.

  pinMode(13, OUTPUT);

}

// the loop function runs over and over again forever

void loop() {

  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  delay(1000);                       // wait for a second

  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW

  delay(1000);                       // wait for a second

}

The task was to change the led on the circuit to digital pin 12 so in my code I had to change the 13 to 12.

We also had to make our leds blink either faster or slower so I changed the delay 

from 1000 which is 1 second to 250 so it blink faster.

Aantekenigen:

![](https://lh3.googleusercontent.com/WsS1Yg6GEuVxVmkt0OJ5s699Lf-1WLr_zgd87pyKZ8J8rcMl2qdVgtXv2yhdX8UXD8EyFK_v6RST-DFaWoCH9ysL1OJf8RAd39xYjOunODgfc1nJ2Tocs4C4sG7rx1sxNcTxOv219FJJ65GWdtVDOw)

               ![](https://lh5.googleusercontent.com/rHOReFqHdbqor0Ozmjiby2Fc8s4Id1GFKiSJinJ8_r0bwLJVQQ_6G1HHHuY9EUae1LT70_7R0WdzH6LWhZrx_3UYe3_kLWJkjBszcz7VhT-T41oA0LS6Q9JnArCocLP2CNO0Yn89-aXW9ai2dTJf_w)

     ![](https://lh5.googleusercontent.com/3pOC-FM0vLmcboVQEFoqrtATQZoM6WFfdPDdrjZD2AHgbG4t9IbEjWLtsF2lp3EqPikQy4fSp4BT2MztPgkEx2L6gvLYxKCbs8RthFax0G9jRlIfavPl0c1NqO9pe6fE1m5HwkV7RYWvnYEGBIEAdQ)

     ![](https://lh6.googleusercontent.com/QQ_xvfU-EoWvuRX3WJKRRTgHoTEn0NZA2fEWZAGmEW_-2qDeXAwaCzREl0zGbbFIFSQheerROGOn22UDnyrap5p_NWM_mwXRSAr6YSOnYMer_-xYTVgPbb1vlEBQs7CP-DaUdWRD_WTSvRC7YI0a_Q)

                            DAY 2

Introduction to the parts in my kit.

    ![](https://lh5.googleusercontent.com/VZcD5mb1E3pUH7vja73QZXmZhc3DOhxPsukeoIIz0tczlUJkpVYV9PcndLf5Xw_o_6RKbaD8DYA0zPkJFlsd-dCjKFedL0i9ee054eh-lXZvFDlj8cgeh7M2DKuRQg911wRSQRw-UeflyjwNSuW3EA)

![](https://lh5.googleusercontent.com/KQ3L6Q43j49cy9FEB6BNuj6EGuOvTlzkUYaJNcB1EyXqdSuycOPXhQFl0V0U-9y3-ypqYUR4qC07g6PqipbnogBTDILJuCBNoZyraE7g640lsM8exSfVZrhQYPJ-Ct9bXJEUTNgmMJGZH0rjNvTEdg)

![](https://lh4.googleusercontent.com/MK0TPc7f7Wet_xTl8ADe-lNeyHblapWRg2QCXc8P3oqH-WNlEsqoCj5SGRQdmOdqXCVFkuuEWF0dr4Iw-AhPW3PSHNXc1mPY2jl5SXXN19aKFk_j0AMmXS7sZBcMtUC6j9P8_eGMlMHLfEu0zWetDA)

                        ![](https://lh5.googleusercontent.com/V1QT-4_h0gNgD_IbP7DPBDxSw3VjGvt07C4VPTm4ug_PT4sTaknx7U6iIWZcZvxGrGdWiL504yY-LP7vqIT393VOgRmmkQjnLhEE2emzgbYDoXfPGtOvJqJVGHGClgI8tyyqe254c42Sr4TobJabTA)

![](https://lh3.googleusercontent.com/dmYIJLwqA6caZammflbQrpi3ViokpmFCul4arhss3M1BRxLVa34c8CU3A6cOFbH1ND_GPOz-jSC8HKbkJcm6HTnWDhOUs4IZLyc0WMlriI3vtzjJ1T8PXRfpTcS1QyBI3PBSztfIPnZn9eZxj8g0SA)

   Symbolen voor de componenten.

![](https://lh3.googleusercontent.com/wMaFXQr-3y6RRSWRvdIFlkihTAo4sZK3G9dMC8hgbd9EWCCZgK6vjslh_Y-yjtw8MSNdKLfljF_bU7CfnZPd9mjr0eN3FNJcELG-ri8UcIg5JHQp-1xhc3NXGk44zi_G1fdeu_8dzKQTih7hWah8Dg)

     Hoe je Arduino Uno in elkaar zit.

Elektriciteit stroomt van de plus pool naar de min pool. Ground kabel (de zwarte kabel) loopt van de min pool naar de min pool. Resister gaat vanuit de kant van plus naar de plus pool van het apparaat waar je het op aansluit. Zie vb hieronder.

![](https://lh6.googleusercontent.com/u7FYmbwR6oHH3L4qmnz2kX20_FXLod-0kW9APf1Oa7LuWsdr2ZswppNUd20D5g9hoSLTBzwYH3MAXsMBHnSNgnNxgAlA8TYywCfdf0f97LHhr5bbcXNUaRmh79qBXggWQd_qF9aYmKjJ54zIixtFag)

![](https://lh5.googleusercontent.com/LnijMstcObUBiYZmZPHA6qyHsGZGZuS_fh2KfFP5ZJ9IHJvvw_YOz-Q61rCwVv2bSNr9LIQ2W7my8Liq6QIMKll3VXqv6XzwbebYwqUbWL9HGOHfl05kxGqzy14Od7KiaMxItT1hg0-vs0LkoeawCg)

Elektriciteit gaat bruja naar het apparaat. Door de resistance wordt de elektriciteit zegmaar "schoongemaakt".

Daarna hebben we de Arduino IDE gedownload. Zie link:

Download the Arduino IDE from the following link

<https://www.arduino.cc/en/Main/Software> 

Scroll down and choose your Operating System (Mac or Windows)

Daarna hadden we een serie schakeling gemaakt in tinkercad en daarna met de arduino.

Zie tinkercad schets hieronder:

Daarna hadden we een parallele schakeling gemaakt in tinkercad en daarna met arduino.  Zie tinkercad schets:

![](https://lh6.googleusercontent.com/WufBSaK8JVsp-m7FsFkxyXi3AZ99xYqmbywXZbdbOfbHIuq3x49YP-4Zn_3HMg5Uzg-Fi7Bs50auL3MxF0ly4oOpOIK0fPIHc8bme2nKVMAxMNPyVNZ6Z9Q2wrINFIjA-SK5U7oqaHZt-LWzNA0-ng)

Code: void setup()

{

  pinMode(13, OUTPUT);

}

void loop()

{

  digitalWrite(13, HIGH);

  delay(1000); // Wait for 1000 millisecond(s)

  digitalWrite(13, LOW);

  delay(1000); // Wait for 1000 millisecond(s)

}

SPACESHIP INTERFACE.

De arduino leest "LOW" wanneer er geen spanning op gezet is.

Schets tinkercad:

![](https://lh4.googleusercontent.com/5iJzerLj4wcZAaCL8cBARA-5fmEbLnxJWcurcW0v9YoxsU67hq6r1NZyyPXcp9RzDqbcOIekYbrMGSKJ4iuQ2nm5Udhg1tKPxrC3CgZYwgrDv64-mHPqGaEy_RWhUm-OJ_nbj206oKBmfJbjKztyNA)

Code: <https://docs.google.com/document/d/1KUCND4thMRwepYGW_44ecSuR4qeSqcVNPOp-Kj7f6UU/edit?usp=sharing>

SERVO MOTOR:

![](https://lh3.googleusercontent.com/q8PsV3bTdl6nL8a4F5nwH1HCy1dSyFWE1qGkna947nRzajnnoBovHoutjGtWjyGkRSAd2EiTFAm0lLG88EIqNwdYvCYuHbMCLZL4ZEJ6RXF5XKSPq7yhxo7pHUhbHzaHs7bGRBpvFfBoBkE6_Hrfvg)

code:![](https://lh5.googleusercontent.com/MTVK-bWK6uXmKIxCt7psvJa57wc1Ow1HhidIJJR5VMMGs4aIZZ7KiCnDQf1f3q0xrnhFZkQsfMdwfrlDDBURuUkazuwasKURnp4_EBZkT13Rd1kLrpS96kx4sMeQm8a3YNcfI9_kexiYqM1-GOvgMg)

                                                           DAY 3. 

LDR- Rgb. 

Dat is een lamp met 3 kleine leds erin.(Groen rood en blauw)

Tinkercad schets:

![](https://lh4.googleusercontent.com/U9iv1XCq38jBoEKH0BLsNzt-LarkAIwT_Vg54vBpuy6aOCuRbs1Nk_oZKF4T5VXZfJrnSsfRCs5E03QhCoQhFitsSEzyCyOz-zQBpF7arQnEfDrRmf0vHSrFSmMegamUEiBz8TsqhqgxPpupxmSFYQ)

![](https://lh6.googleusercontent.com/T10KrUYjGkGOlS3S03ZWAssUj72YrfoDAaU4PAe9EgSuWL4TeSCVeLG4kYv3hY7jjtZjwl5f6i-b0crZpb2pH0ZiLH4wxSmGZ1afPqnNnN4-QVr6VwcfCqQxT6-10eUuCpggp344dF321pmqDnBgTA)

![](https://lh5.googleusercontent.com/lf8f6a2AT2FuFzmUUrl3sTF0OxUsOFeAqgp8jQdEYJWwN_lvFE--gXAsZos9xtLnezkc_t3C08lCBW6oD7wMrC_gWaypsSnYtwWkDNtEv50c-stWd5SfUuMuNfGabeNs9utYVct2NWUMy7kuz5iPxg)

Code: 

<https://docs.google.com/document/d/1Q21Y0YL0jwXjsFvbldaTmLzsdEUfiXXlQ6HyaR0BvbM/edit?usp=sharing>

![](https://lh6.googleusercontent.com/JHbmqKeHOnq4UscClNgGCsZSX_TxF7KBN56dIdKZUYmDgmxJHXbrvIBKnD8MXgNTwIbgaFXOaNcL1EOQkF5E76A0PmMN0KkrFicTfIuximiIBL8hfQec0_9XiYgWMGDDWQ8WWW2ZFgf6sikMbh0r_g)

![](https://lh5.googleusercontent.com/7C_m5gTub953917I-ggYLuHxZusrLvBvwtIXgZgDJD8HP12C9lMM686bwT3xMWLk4cRKVXDqfM6NwA9LWmDiLt3GvU-JdVk38vFDfFVVOK5tQg-cGQb1REXtsOZILiUaf28f4fBHHMJz9VfDz_bvAQ)

                          DAY4

Presentaties van de Documentation.

                                                 DAY 5.

 LCD12C.

![](https://lh4.googleusercontent.com/shOLj6QJ5dsYiUwYuA6natL52jhe7ujkPdeUeZae3fNyb856gjH43ogDlTQjbVhECgh76NALIeGlZ26GZv031_kjoURBW4Uz2iAK0k7kx2h6gXQ63D7xaQUhkMGcIzmpsZI-xRum_zDoRKrfbzLH4Q)

:

![](https://lh6.googleusercontent.com/b-BVy1jd0QdZqRnh7c2o53e8biPnOsph_RIh_EJSghjyYPCeI-nJq3zaMQGvsB-H0Cci3wE06bz2MCe6q9W1BMnzlY_s2w9i4eZeXi9X8rkYAUSVRXNm0iHIxeIs93Ol-jvBKCK0_xmA5ZP3TsP-kA)

![](https://lh6.googleusercontent.com/gT1dT7DeggQacb4GjnhaiTkZPhgTJsTqQcR0Yel4K4nlxqPSalftI91AUzyTCKgf671mg3N4PwOyusZL8UQrmV-ltv9QR0b4vKYTKZPxAqO9Mc_UVij5DGPr7eH5fmn4QzRybyLhU1C5ZWi0SQkenQ)

![](https://lh5.googleusercontent.com/ec_D0gwA1V1VcYOMxgzhLehG8hqw29BWGn3K27J4n_4dYj2HkcwEJS_DSDBbAquFQvSPWdia4d4aSNGQmXkuVr0PCJ1nS-0qnKJ1uJv1b8GMHyvF-EQOnhmtpC0eAhgGX8Ybcmjlx4xwLu4oGTZehg)

Tinkercad schets:

Project 1.

<https://mail.google.com/mail/u/0?ui=2&ik=540162729c&attid=0.1&permmsgid=msg-a:r2703160075122173593&th=170b633649723745&view=att&disp=safe&realattid=170b632e85e4640cf0e1>

Tinkercad schets:

code:

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

const int greenLEDPin = 9;

const int redLEDPin = 11; 

const int blueLEDPin = 10;

const int redSensorPin = A0; 

const int greenSensorPin = A1; 

const int blueSensorPin = A2;

int redValue = 0; 

int greenValue = 0; 

int blueValue = 0;

int redSensorValue = 0; 

int greenSensorValue = 0; 

int blueSensorValue = 0;

void setup() {

  lcd.init();

  lcd.backlight();

 // lcd.print("Hello World!");

 Serial.begin(9600); 

 pinMode(greenLEDPin,OUTPUT);

 pinMode(redLEDPin,OUTPUT);

 pinMode(blueLEDPin,OUTPUT);

}

void loop() {

  lcd.scrollDisplayLeft();

  delay(500);

 redSensorValue = analogRead(redSensorPin);

  delay(1000);

   greenSensorValue = analogRead(greenSensorPin);

  delay(1000);

 blueSensorValue = analogRead(blueSensorPin);

  Serial.print("Raw Sensor Values \t Red: ");

  Serial.print(redSensorValue);

  lcd.print("Raw Sensor Values \t Red: ");

  lcd.print(redSensorValue);

  lcd.scrollDisplayLeft();

  delay(500);

  Serial.print("Raw Sensor Values \t Green: ");

  Serial.print(greenSensorValue);

  lcd.print("Raw Sensor Values \t Green: ");

  lcd.print(redSensorValue);

  lcd.scrollDisplayLeft();

  Serial.print("Raw Sensor Values \t Blue: ");

  Serial.println(blueSensorValue);

  lcd.print("Raw Sensor Values \t Blue: ");

  lcd.print(blueSensorValue);

  lcd.scrollDisplayLeft();

  redValue = redSensorValue/4;

  greenValue = greenSensorValue/4;

  blueValue = blueSensorValue/4;

  Serial.print("Mapped Sensor Values \t Red: ");

  Serial.print(redValue);

  Serial.print("\t Green: ");

  Serial.print(greenValue);

  Serial.print("\t Blue: ");

  Serial.println(blueValue); 

  analogWrite(redLEDPin, redValue);

  analogWrite(greenLEDPin, greenValue);

  analogWrite(blueLEDPin, blueValue);

}

DAY 5

![](https://lh6.googleusercontent.com/rMsM6K2orwAG1vmLrBwyIYctyhumYu5ZR34uxg_aolFlsm12FdwrFuc462I8zPx6cGAz1Z9WuWyDkRAqyZhsWqIFn7XmI4gcQbVsJxBCdb5fuPk22krU1XsYQ16tkKtSNHd5CoSwxA_GRN3i7G3J_w)

Box voor arduino uno en deksel.

![](https://lh6.googleusercontent.com/DQqUmnxL_R-vwv3Tak69a2z4gPbTqVjnreKOvXlEyQhQ98NMfJLJTIEEeh121-D31PwNZUSJl-LcxjVnooYE6KSFbCd_Al-E-TKM3vANJ3R5Gjj9JhVdBWaBAsm3y0pgi0H9W8Bg3QLkZg65vialYg)

![](https://lh6.googleusercontent.com/dKMX_x_JAK-UmWVxMe0L2WtsH0OGRyviII4hmSq_2le5dr8pYNUnqPxTHRlLAKTxrgQaju4L1wn3I23UiqrWcUhPxIDlN9eAsofZciMkRx2L2CV3ejZX26p46UPaIda5KeAEqWzKpIhtPzIwu09V-Q)

H bridge.

Code:

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

}

void loop() {

   //For Clock wise motion , in_1 = High , in_2 = Low

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

   analogWrite(pwm,250) ;

   /* setting pwm of the motor to 255 we can change the speed of rotation

   by changing pwm input but we are only using arduino so we are using highest

   value to driver the motor */

   //Clockwise for 3 secs

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

}

H bridge with potentiometer

![](https://lh5.googleusercontent.com/sHi3zwWSA0mGpX6rIDH0oLwinSXbdjM07ynx98V6b-P6Sml4fpIB2CSGXBN40_hmL7D5hZolgvUa7UCqH30svtWS-OCxyH18pYxjDv4Pmwr-DHd1x98g_p-xtAmXnQ2asT-0eu8KVoiLLScFEQ9aSw)

Code:

const int in_1 = 8 ;

const int in_2 = 9 ;

const int pwm = 3; 

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm, OUTPUT); //we have to set PWM pin as output

   pinMode(in_1, OUTPUT); //Logic pins are also set as output

   pinMode(in_2, OUTPUT);

   Serial.begin(9600);

}

void loop() {

    int duty = map(analogRead(A0),0,1023,-225,225);

    Serial.println(duty);

    analogWrite(pwm,abs(duty));

  if(duty>0) {

     //turn right

    digitalWrite(in_1,LOW);

    digitalWrite(in_2,HIGH); 

   } 

   if(duty<0){

     //turn left

    digitalWrite(in_1,HIGH);

    digitalWrite(in_2,LOW); 

    }

  if (duty==0){

      //break

    digitalWrite(in_1,HIGH);

    digitalWrite(in_2,HIGH);

  }

}

![](https://lh6.googleusercontent.com/Akl63vBKwnIUn5BKDYkgQBExKbfYpyxKDewerHhHtrgM8UIlH3L0JY6iQTL-IE5_McXivUgg3Ne1ubzV-L4nnK7oA7Kvq-yty6pFW3gKgfwqEOAg5-VEO9BscWaOUP_Eb6s7p_NJj8HRy16evOTepQ)(Aantekening)

Final project

For my final project i made miss Back off!

Back off is a touch less temperature meter.

I used:

-   Arduino uno  * LCD screen  * buzzer

-   Some wires  * ultrasonic distance sensor

-   Bread board  * Touchless temperature meter (mlx 90614)

First i simulated everything in tinkercad

 I did the lcd screen with the touchless temperature meter first and then i simulated the buzzer with the ultrasonic distance sensor.

![](https://lh5.googleusercontent.com/4bTdmsj0UH244F7yLZod0T3Z6n-RO8-jFQZcJrbLBpMpdTsIRWqusBxOk8i1-xOCd8Mi4HoXsenQ7Qdb1_XY_rFrg7VdvUthqLFxuTuHCVdHZi71m8ffhVQgEG-b6xgbcKKNtuh4n3jqo-qw5T_ilw)

![](https://lh6.googleusercontent.com/IDx_PNomK4wuvAzqW81xYmSHkvifCFjVAPOKtbwRV7Gq6z4eQ9TgHWb2lO0aPYmPrflcs1cU_sJTWY2_zq0nq55LT97eWXMdClrusSvbk-qxvao16YbUJ-mn6NTVj01xKFZOxQwWGptcwhlIZpHwEQ)

Then i had two different codes. They worked

And then i started to built everything physically

![](https://lh4.googleusercontent.com/kLi-ZUhUIisPmV0kgU2RqasDd4bBe3W2XueUeQE40WFK89sZujqWBpC5KXwq1oGgqvm0UFMQF1v0oamrnWDkxYyzuS6RcCzPVFeej9DPGXNQv_U474xr3a-AjRUQFWMtbR6EeJtFCLJqXWlhBE0Q6Q)

I had some problems with that. The wires were coming out of the sensors so i had to soldure some of them. And then i started do design an enclosure for the lose components in tinkercad. 

![](https://lh3.googleusercontent.com/R2anL-DmILaYSlMJgB5mwrahIHZNpfCPq6RlbrStkkav58SmETXmiN4KKBby9PRQCPuU9IK4Ks2IBIsNFsjDu0BwEr7kUxa1RlBX5Ng4Nz1e0jHL-eBGlqyjuoAS2d1C3-LH9cjXH8_YjEfaLYiB9g) ![](https://lh4.googleusercontent.com/XH2crLR9mYUCxctozXSn0RKGJT7NGQcmZ8a6iA-QKv4WE78424Nv00gVBq0isR8Tns1_YRHxSnxSBsW90_8W3OaYOqCgY-mLKnIiWrWXWDMVdLt3UNMebtIyrsBvoadnOZB7qqyuaXKS62aKfWhhqg)

And then I 3d printed it with the anycubic 4max pro. 

I had some problems with that. The holes for the ultrasonic distance sensor were too far from each other so we had to use the drill. And the hole for the LCD screen was a little too small and too low.

And then I combined the codes and made i work.

After that I started packaging.

![](https://lh6.googleusercontent.com/J3LMjne9zJkazK4vgoj9qdw_8hkeGaUc7tprw7DVEBcHXt8PtwDxddACF6WKigDEgFSAu-O-Ss5RS71s2u2LuWwZRsULjKYl-ltROXPSplX0fEmFdoRWf_cof4lT9xGF44YXUR_a5LYFvknJlBC-UA)

![](https://lh4.googleusercontent.com/BarIpb6Vw7iiRD-g1pgT6Z5lMgKNeM1Wfs6pGdUGJ5vpqPsWHv0k1mREw0q_UaXz6cpB9NKurj_-eZq2TEmzspvqY-_TwlI8XMMhIsp6DdrtGSIOMh966Oib4wzgUWQ7_pWSdepEOm80Z1rnxzkHkA)

![](https://lh6.googleusercontent.com/bf9gJPjtuKhoVgtw9A_AGb2pTEwpENPDKtagHdmQegQatwP41PdsjeuQOrWA3dfVm-Y_aDF-LV37JC6y_U967cHMuuXTNtxi6lJER-RL04nVrb7D-XHPkqlB4DuTp1sLr33zytTyXD3QavjW0GhYTA)

Then i put the code in it

CODE:

#include <Wire.h> 

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

#include <Adafruit_MLX90614.h>

Adafruit_MLX90614 mlx = Adafruit_MLX90614();

// defines pins numbers

const int trigPin = 9;

const int echoPin = 10;

const int buzzer = 6;

// defines variables

long duration;

int distance;

//int safetyDistance;

int threshold = 30;

/*************************************************

 * Public Constants

 *************************************************/

#define NOTE_B0  31

#define NOTE_C1  33

#define NOTE_CS1 35

#define NOTE_D1  37

#define NOTE_DS1 39

#define NOTE_E1  41

#define NOTE_F1  44

#define NOTE_FS1 46

#define NOTE_G1  49

#define NOTE_GS1 52

#define NOTE_A1  55

#define NOTE_AS1 58

#define NOTE_B1  62

#define NOTE_C2  65

#define NOTE_CS2 69

#define NOTE_D2  73

#define NOTE_DS2 78

#define NOTE_E2  82

#define NOTE_F2  87

#define NOTE_FS2 93

#define NOTE_G2  98

#define NOTE_GS2 104

#define NOTE_A2  110

#define NOTE_AS2 117

#define NOTE_B2  123

#define NOTE_C3  131

#define NOTE_CS3 139

#define NOTE_D3  147

#define NOTE_DS3 156

#define NOTE_E3  165

#define NOTE_F3  175

#define NOTE_FS3 185

#define NOTE_G3  196

#define NOTE_GS3 208

#define NOTE_A3  220

#define NOTE_AS3 233

#define NOTE_B3  247

#define NOTE_C4  262

#define NOTE_CS4 277

#define NOTE_D4  294

#define NOTE_DS4 311

#define NOTE_E4  330

#define NOTE_F4  349

#define NOTE_FS4 370

#define NOTE_G4  392

#define NOTE_GS4 415

#define NOTE_A4  440

#define NOTE_AS4 466

#define NOTE_B4  494

#define NOTE_C5  523

#define NOTE_CS5 554

#define NOTE_D5  587

#define NOTE_DS5 622

#define NOTE_E5  659

#define NOTE_F5  698

#define NOTE_FS5 740

#define NOTE_G5  784

#define NOTE_GS5 831

#define NOTE_A5  880

#define NOTE_AS5 932

#define NOTE_B5  988

#define NOTE_C6  1047

#define NOTE_CS6 1109

#define NOTE_D6  1175

#define NOTE_DS6 1245

#define NOTE_E6  1319

#define NOTE_F6  1397

#define NOTE_FS6 1480

#define NOTE_G6  1568

#define NOTE_GS6 1661

#define NOTE_A6  1760

#define NOTE_AS6 1865

#define NOTE_B6  1976

#define NOTE_C7  2093

#define NOTE_CS7 2217

#define NOTE_D7  2349

#define NOTE_DS7 2489

#define NOTE_E7  2637

#define NOTE_F7  2794

#define NOTE_FS7 2960

#define NOTE_G7  3136

#define NOTE_GS7 3322

#define NOTE_A7  3520

#define NOTE_AS7 3729

#define NOTE_B7  3951

#define NOTE_C8  4186

#define NOTE_CS8 4435

#define NOTE_D8  4699

#define NOTE_DS8 4978

int melody[] = {

  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4

};

// note durations: 4 = quarter note, 8 = eighth note, etc.:

int noteDurations[] = {

  4, 8, 8, 4, 4, 4, 4, 4

};

void setup() {

Serial.begin(9600); // Starts the serial communication

pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output

pinMode(echoPin, INPUT); // Sets the echoPin as an Input

pinMode(buzzer, OUTPUT);

mlx.begin();

lcd.init();                      // initialize the lcd 

lcd.backlight();

  // iterate over the notes of the melody:

}

void loop() {

 lcd.setCursor(0,0);

 lcd.print("Target ");

 lcd.print(mlx.readAmbientTempC());

 lcd.print(" C");

 lcd.setCursor(0,1);

 lcd.print("Body Temp  ");

 lcd.print(mlx.readObjectTempC());

 lcd.print(" C");

 delay(1000);

if (mlx.readObjectTempC() > 38 ) {

  lcd.clear();

  lcd.setCursor(0,0);

  lcd.print("Danger");

  lcd.setCursor(0,1);

  lcd.print("Temp Too High");

  tone(buzzer, 1000); // Send 1KHz sound signal...

  delay(1000);        // ...for 1 sec

  noTone(buzzer);     // Stop sound...

  delay(1000);        // ...for 1sec

}

else {

}

int dist = ultrasonic();

if (dist <= threshold){

  lcd.print(threshold);

  Serial.println(dist);

 Serial.println(threshold);

for (int thisNote = 0; thisNote < 8; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.

    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.

    int noteDuration = 1000 / noteDurations[thisNote];

    tone(6, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.

    // the note&apos;s duration + 30% seems to work well:

    int pauseBetweenNotes = noteDuration * 1.30;

    delay(pauseBetweenNotes);

    // stop the tone playing:

   // noTone(6);

  }

}

else{

  noTone (6);

}

}

int ultrasonic(){

// Clears the trigPin

digitalWrite(trigPin, LOW);

delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds

digitalWrite(trigPin, HIGH);

delayMicroseconds(10);

digitalWrite(trigPin, LOW);

// Reads the echoPin, returns the sound wave travel time in microseconds

duration = pulseIn(echoPin, HIGH);

// Calculating the distance

distance= duration*0.034/2;

return distance;

}

And my final project:

![](https://lh5.googleusercontent.com/F_mRMY0Vfw4SizkJOyyXfgm9uSAbOhGDT4nsdh_oxgKvrA8797fgkx50Fehe8c5FqRBaeNkNqyoSJtYYKvhclAdL1RHFU_PtBVTbN67V39KBB8-SQLm6Zf2Joo5kysChROqxJE1tDCHEuTvfreGm9A)

   And then it worked :)

And then i had my presentation 

<https://youtu.be/sUHy0N9cfXI>