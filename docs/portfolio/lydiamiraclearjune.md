
Learning Arduino

![](https://lh4.googleusercontent.com/NRNJpsMR2txiaEduYiNPEckmZvjDGe-9cNHchM0G5qJhdpyhrN_q8VIX4usTlPa3Ba3yzVeMKK_ggXeMVKHrYUlDZGLD_Ua3dTX5KnC-X2rkNhiQs-NH1thdCQ5M-ChaYtapRFo2ScZVhaDHuhHnXaQ)

Name: Lydiamiracle Arjune

Program: Codettes Inno Starter

About me

![](https://lh4.googleusercontent.com/c5TrQ2PD-JOZqB5pXLyAotMRWPKG5-MoZVbQl7O5zXnQQ24S4D80hnEQZv4aNd3zIZgKcepL8TOyhVvkPcly5mQKCgyp_WLgUFaL8nFHA_J_UjzTSiNJxwNkfkHKvTc3HS2vQm9dVHWVZPhDMGMOry8)

My name is Lydiamiracle Arjune. I am 13 years old.

I attend CR Frowein school, and I like doing school stuff and all kinds of crafts when I am free.

I like music and enjoy playing the violin and i like drawing a lot.

But I don't know what I want to become yet.

So I'm doing the best I can to study further.

Day 1

-   Objective

-   Introduction to arduino uno and build a basic blink led circuit.

-   Change circuit to blink led. Connect to pin 12 then 13

-   Change the speed of the blinking of the leds by adjusting the delay in the code

Theory: 

Build a basic electronic circuit using the 

Arduino uno to blink a led connected to digital pin 13.

Components list

-   Arduino uno

-   Breadboard

-   Led

-   Jumper wires

-   Resistor

-   USB cable

Here are the explanations of the components we used on day one.

A LED : is a light emitting diode, a semiconductor device that emits

 light  when an electrical current passes through it.

A resistor : is a passive electrical component to create resistance in the flow of electric current.

A breadboard : it is a board where you can place your components

and  link them to other components with wires.

An Arduino : is an open source electronics platform based on easy to use hardware and software .

Jumper wire :   These are wires that have connector pins at each end, allowing them to be used to connect two points to each other without soldering.

In tinkercad the circuit is built as in the picture here below. Handson, (circuit) Pictures

![](https://lh6.googleusercontent.com/ryD_-IlJ3MkodpFbKYK6DR5yLC6MZYfugDScRrMpmTzKGhQFRlbZrM1-DA_uQapPg4pVKMtOQLlwrF_Te8rSWiFodC_HTES5B52GwRQHj4sgh5NsvYC1XYXcMCCGYp7RdqwTm4jKjFNGKOx16AkoVQc)

The code block for this circuit is:

void setup()

{

  pinMode(13, OUTPUT);

}

void loop()

{

  digitalWrite(13, HIGH);

  delay(1000); // Wait for 1000 millisecond(s)

  digitalWrite(13, LOW);

  delay(1000); // Wait for 1000 millisecond(s)

}

                  Stil day 1;

We talked about the Arduino Uno and then opened the arduino kit.

The arduino uno has a built-in blink circuit with code. This circuit is given as an example in Arduino IDE.

We then build the basic blink circuit on a breadboard using the aforementioned components. 

Next we had to change the output pin from 13 to 12. We also changed the delay time to let the led blink faster.

![](https://lh3.googleusercontent.com/tR5PiLMISuV9Vee4aXGutrsHWBJKAV33Dga2d2n93BtnIPwYBGbpZ0fv9MlIQDqRWy9ay74tnX3rE-uj8IQKd7K-BTDPdPzZA3qRYHFtw3wO0msictrAy1aFH5COCMFUzRP0XGykFSRph2oBbRGwdSU)

Here below is the code block we used in arduino IDE:

/ the setup function runs once, when you press reset or power the board

void setup() {

  // initialize digital pin LED_BUILTIN as an output.

  pinMode(12, OUTPUT);

}

// the loop function runs over and over again forever

void loop() {

  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)

  delay(1000);                       // wait for a second

  digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW

  delay(1000);                       // wait for a second

}

Void setup is a code that runs 1 time 

Void loop is a code that runs in a circle.

Day 2 series circuits Spaceship interface.

Components list

-   Arduino uno

-   Breadboard

-   Leds

-   Jumper wires

-   Resistors

-   USB cable

-   Pushbutton

Here are the explanations of the components we used on day two.

A LED : is a light emitting diode, a semiconductor device that emits

 light  when an electrical current passes through it.

A resistor : is a passive electrical component to create resistance in the flow of electric current.

A breadboard : it is a board where you can place your components

and  link them to other components with wires.

An Arduino : is an open source electronics platform based on easy to use hardware and software .

Jumper wire :   These are wires that have connector pins at each end, allowing them to be used to connect two points to each other without soldering.

 Push button : is a device designed to close or open an electric circuit when

a button or knob is depressed, and to return to normal position when it is

released.

![](https://lh4.googleusercontent.com/KtHssQtDTGEAqKz6ikc75WH5bSjhdSQDz2xsukanHfGFwpuu6Q2TRr7PlVDdvvliw2qK10jp_cfU7jHsjrDk0pWDxgD4JuppmgSi-l5g_LX36dI-J2sYrYZwapCrOp32aQ4lNj81DY6d0GxBhUdNTjg)

A series circuit is a circuit with a single loop.

-   All the resistors or other components are arranged in a line and share the same current which is also the current from the source.

-   The source voltage is divided across the resistors or components, so the total source voltage is the sum of all the voltages across each component.

Here below is a series circuit of two leds and two resistors built on the breadboard.

![](https://lh5.googleusercontent.com/xJypAAsgOW0Xacduf-TVnqj77__PmYvwajbeVUHdetmN45P6_1ppat8xQjOzVAGr3fe745ZfQYn_b73slRHZznVpOZruV3RHRztCEP1hpl3SKmqBmW1Y8CHl_PiV-xO6K-Bkmw-LlLfqMQWqE2gAxFU)

![](https://lh5.googleusercontent.com/PSHWnjPRDjTTn_4AfR4ziDmYRoEVoDtGWzsiFPQZKtcL3ra9tAUjt26gYT5CSg9gkXH1r98PQw6t5TsWhNaMJgh6Kc0ITxnPDCn3jWkxd6Jws3lmHIniD7O5RwBWYajwFx2lG-d9cnonoxRIy3ZNP9k)

A parallel circuit has multiple loops

-   The current from the source flows partially through each branch of the circuit.

-   The voltage across all parallel components is the same as the source voltage.

Here below I have a picture of a parallel circuit built on a breadboard.

![](https://lh3.googleusercontent.com/TvRVlKm4WK7nEFUHruo7p2DWt_iCQvxy_FpBk8ve_zNicDC6K4IeS7z6bscA0JC6Lk7P5DfcTWOFvuxQHcXxrXT6Wtrte3333oso2RzJkjwniNrwqOL0jknfMjLrDW8B1Lm5xAtDc1QwE1qloODwP-M)

Ohm's Law
=========

![](https://lh6.googleusercontent.com/uZHqRgHTyKuFlm9eoVLRj6mj1Hm18-FDuN0iMjhAnvvDmxTa1T16iznOZjHgP6mG5ZKGlQ081RVP6jn4gIAu-94bLap3QEuI-G_dftFxQFh4_RAJ23GAXkwTeqfDTw6LCveVzDMnNaNWlE0IKxiISwo)

Ohm's  law states that the current through a conductor between two points is directly proportional to the voltage across the two points.

This constant of proportionality is called the Resistance.

 Day 3

The RGB Led.

Theory: 

Build a basic electronic circuit using the 

Arduino uno to light the RGB in the different colours

The RGB LED is an LED that is able to shine in different colours having three small LED's inside. RGB stands for the three colours "red", "green" and "blue".

Components list

-   Arduino uno

-   Breadboard

-   Jumper wires

-   Three 330 Ohm Resistors

-   USB cable

-   RGB LED

Here are the explanations of the components we used on day three.

A resistor : This is a passive electrical component to create resistance in the flow of electric current.

A breadboard : This is a board where you can place your components

and  link them to other components with wires.

An Arduino : This is an open source electronics platform based on easy to use hardware and software .

Jumper wires : These are wires that have connector pins at each end, allowing them to be used to connect two points to each other without soldering.

RGB : Inside the RGB LED are three separate LEDs available, which can be turned on and off individually and shine in three or more

different colours.

![](https://lh3.googleusercontent.com/RlhhP6IzqOSKyL5rZwJi3EKXtGSRZN79RuDOzGJwf6u5IeOZbD-oqhthU5uEprHN5U2pjw53iIVq8LTpchnJZ5-GnAcGMnHGrRACfbg8FSwIPXgHf-0U8I6jXJv7RHJsWRAueaA94ecoROTfQsYx4j4)![](https://lh6.googleusercontent.com/UXKVOUUm4uYUwWCvY_-BUcu2sk4nyaJi8D7Xpqre3BpmqKHpmqORNyBoKIKoJbPPIhmDDKKZ76Ct5vJ3vIXAebeD1pjGJ5-6UiDWyrCHpCg12KI2yKhUeWjgY6P59V1ttnEa6F3DvLciEnUNHQS8Il0)

Here below is the circuit of an RGB built on a breadboard where you have the same RGB LED light in the colours red, green and blue.

![](https://lh4.googleusercontent.com/C1Q50SnLMowfRil_hZmpbvPJCAHC5iEDeuZp8nChaU1pcvi8hfwzg_6aonUn_xIRk_XMwXs1whg2FQGqHdhLDjvvuAS1IaNsDWOH0ZDYihzaLBfDGqM-cGH8seI0dvIQ66muH2DbFa_XtZKUgDng0S4)

![](https://lh4.googleusercontent.com/I7rXBeixJbSv1kGkjXFDtVK_fk0I5-BuCrs0fY5je6AyL5TLTjNGvijY9O_f2x2oYGoPyxRLDKyJ9xCuUY7JbRcqp0UDW4ev9r-76Df8jZdrApT50zT3VU8drfUvMw_BKiU0RnKoOmggMd0kHhTsg4k)

![](https://lh5.googleusercontent.com/1tnAJ_n6HmyG48Zj6hmKQ8-k423gO8c_UreFImjosiJzkWqDh4U1hL-pv6C0qNwQl1mtINwUIGyaIfMGolWTuzXdqd8Pcs872UsPjLegZzrchVBZbJiHXLDfRw3iv8KczhfO_Kcp8Py3a9ib8kOwNVk)

Here below is the code block I used in arduino IDE:

int red_light_pin= 11;

int green_light_pin = 10;

int blue_light_pin = 9;

void setup() {

  pinMode(red_light_pin, OUTPUT);

  pinMode(green_light_pin, OUTPUT);

  pinMode(blue_light_pin, OUTPUT);

}

void loop() {

  RGB_color(255, 0, 0); // Red

  delay(1000);

  RGB_color(0, 255, 0); // Green

  delay(1000);

  RGB_color(0, 0, 255); // Blue

  delay(1000);

 }

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)

 {

  analogWrite(red_light_pin, red_light_value);

  analogWrite(green_light_pin, green_light_value);

  analogWrite(blue_light_pin, blue_light_value);

}

We also worked with a LDR (Light Dependent Resistor) and some LEDs to light up when it is dark and turn off when it is bright or at day light.

Another word for LDR is a photoresistor.

A picture of a LDR.

![](https://lh6.googleusercontent.com/oWIJs0Lv6Jk-ks5eJbCgWqKTFmdEp3_axtIvoHmX1IUzEwuhFWG-yqkMwnpi7sdMqP4vSDY28MTJGUKHtfkQTQkhE7XZzMn31eEogeJpp5X79HfFe-K5SIxuxFf8ooB2U3yiCaW8FvEN259vAhFgmvc)

Components list

-   Arduino uno

-   Breadboard

-   Led

-   Jumper wires

-   Resistor

-   USB cable

-   LDR

Here are the explanations of the components we used on day four.

A LED : is a light emitting diode, a semiconductor device that emits

 light  when an electrical current passes through it.

A resistor : is a passive electrical component to create resistance in the flow of electric current.

A breadboard : it is a board where you can place your components

and  link them to other components with wires.

An Arduino : is an open source electronics platform based on easy to use hardware and software .

Jumper wire :   These are wires that have connector pins at each end, allowing them to be used to connect two points to each other without soldering.

LDR : A  LDR is also called a (photocell, or light dependent resistor.)

A variable resistor that changes its resistance based on the amount of 

Light that falls on its face.

Here below the circuit on a breadboard with two leds controlled by the LDR. First the lights are off and then turned on when the circuit is placed in the dark.

![](https://lh5.googleusercontent.com/FYRZeoFsiBAMGdx30N9OZZghptHa0LTTg0O_WWneC5mUQFBX3ndsxLR15L1k8w1Zvy4zdPiyYX8JpI1cE-qEYZWb_XJuyx_9CHnJuHkS7jKfzewzhq11LOzbSmMmkg_O7NXJIXqZkhTZUM8vQbnzwwc)

![](https://lh4.googleusercontent.com/N30UuFw7kI4U6ZELeG2RJZOL1VW-8xmw8wcbY_5_OLRT0YZGFA7bbLDVxkSqt1_sp7UMP24y3ykVlrrOA2p8I7DvBvsQ6DT5aguy7upw3neClNlxs6JioyGyoUk0gXO-xTgxyuxXxFcF16ewxq-2icw)

As per the circuit diagram, we have made a [voltage divider circuit](https://circuitdigest.com/calculators/voltage-divider-calculator) using LDR and 100k resistor. The voltage divider output goes to the analog pin of the Arduino. The analog Pin senses the voltage and gives some analog value to Arduino. The analog value changes according to the resistance of LDR. So, as the light falls on the LDR the resistance of it gets decreased and so the voltage value increases.

Intensity of light ↓ - Resistance↑ - Voltage at analog pin↓ - Light turns ON

### Code Explanation:

#define relay 10

int LED = 9;

int LDR = A0;

Setting up the LED and Relay as Output pin, and LDR as input pin.

pinMode(LED, OUTPUT);

pinMode(relay, OUTPUT);

pinMode(LDR, INPUT);

Reading the voltage analog value through the A0 pin of the Arduino. This analog Voltage will be increased or decreased according to the resistance of LDR.

int LDRValue = analogRead(LDR);

Giving the condition for dark and bright. If the value is less than 700 then it is dark and the LED or Light turns ON. If the value is greater than 700 then it is bright and the LED or light turns OFF.

if (LDRValue <=700)

{

digitalWrite(LED, HIGH);

digitalWrite(relay, HIGH);

Serial.println("It's Dark Outside; Lights status: ON");

}

else

{

digitalWrite(LED, LOW);

digitalWrite(relay, LOW);

Serial.println("It's Bright Outside; Lights status: OFF");

}

                           Day 4

On day 4 we worked with a Love o meter(A meter that can measure temperature)

My components list:

-   Arduino uno

-   Breadboard

-   Resistors

-   jumper wires

-   Temperature sensor

Here are the explanations of the components we used on day four.

A LED : is a light emitting diode, a semiconductor device that emits

 light  when an electrical current passes through it.

A resistor : is a passive electrical component to create resistance in the flow of electric current.

A breadboard : it is a board where you can place your components

and  link them to other components with wires.

An Arduino : is an open source electronics platform based on easy to use hardware and software .

Jumper wire :   These are wires that have connector pins at each end, allowing them to be used to connect two points to each other without soldering.

Temperature sensor : this sensor changes its voltage output depending on the temperature of the component.

Here below I have a picture of a love-o meter.

![](https://lh3.googleusercontent.com/6ajlhvosqBYNPDcx9_j53WMxpfkBG55RpUChkpJ2aVf0J0rB1EXivOXzYeVJI9plIut6i-iouJjnkXHcyPbDb3zrOfbvfNd2Sr4lHrN229LU2BAyXY4AJ66pIb7onrKcd3UC-AhtJSXMffryIGCc50A)

Here below is the code block I used in arduino IDE for the Love o meter :

const int sensorPin = A0;

// room temperature in Celsius

const float baselineTemp = 20.0;

void setup(){

  // open a serial connection to display values

  Serial.begin(9600);

  // set the LED pins as outputs

  // the for() loop saves some extra coding

  for(int pinNumber = 2; pinNumber<5; pinNumber++){

    pinMode(pinNumber,OUTPUT);

    digitalWrite(pinNumber, LOW);

  }

}

void loop(){

  // read the value on AnalogIn pin 0 

  // and store it in a variable

  int sensorVal = analogRead(sensorPin);

  // send the 10-bit sensor value out the serial port

  Serial.println("sensor Value: ");

  Serial.println(sensorVal);

  // convert the ADC reading to voltage

  float voltage = (sensorVal/1024.0) * 5.0;

  // Send the voltage level out the Serial port

  Serial.println(", Volts: ");

  Serial.println(voltage);

  // convert the voltage to temperature in degrees C

  // the sensor changes 10 mV per degree

  // the datasheet says there's a 500 mV offset

  // ((voltage - 500mV) times 100)

  Serial.println(", degrees C: "); 

  float temperature = (voltage - .5) * 100;

  Serial.println(temperature);

  // if the current temperature is lower than the baseline

  // turn off all LEDs

  if(temperature < baselineTemp){

    digitalWrite(2, LOW);

    digitalWrite(3, LOW);

    digitalWrite(4, LOW);

  } // if the temperature rises 2-4 degrees, turn an LED on 

  else if(temperature >= baselineTemp+2 && temperature < baselineTemp+4){

    digitalWrite(2, HIGH);

    digitalWrite(3, LOW);

    digitalWrite(4, LOW);

  } // if the temperature rises 4-6 degrees, turn a second LED on  

  else if(temperature >= baselineTemp+4 && temperature < baselineTemp+6){

    digitalWrite(2, HIGH);

    digitalWrite(3, HIGH);

    digitalWrite(4, LOW);

  } // if the temperature rises more than 6 degrees, turn all LEDs on

  else if(temperature >= baselineTemp+6){

    digitalWrite(2, HIGH);

    digitalWrite(3, HIGH);

    digitalWrite(4, HIGH);

  }

  delay(100);

}/*

Day 6 LCD CIRCUIT PROJECT.

A basic arduino LCD text.

My component list.

-   Arduino uno

Microcontroller development board

-   LCD

Liquid Crystal Display is a graphic display which is able to display character.

-   Potentiometer: A variable resistance with three pins.

-   Jumper wires 

![](https://lh5.googleusercontent.com/ktClvgAdKMI2okUWWNAQ4wWAeWEMkyOfzS9_A_tFmxAS9Ll4e17KLg0Q_kElSsSnOCMmlOIia90HlIpmwt7Kzbd7ASNF6nYSPEe5yDpkxYjE_HXA_7OynONRxERwjWDnc_ATxQRdERrHI20GDhKg3IY)

LCD screen:is a type of alphanumeric or graphic display based 

On liquid crystals.

Here below i have the code block i used  for the picture here above

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5, d6, d7)

void setup() {

 lcd.begin(16,2); // Initializes the interface to the LCD screen, and specifies the dimensions (width and height) of the display }

}

void loop() { 

 //lcd.print("Arduino"); // Prints "Arduino" on the LCD 

 lcd.print("Hello_world"); // Prints "Arduino" on the LCD 

 delay(3000); // 3 seconds delay 

 lcd.setCursor(2,1); // Sets the location at which subsequent text written to the LCD will be displayed 

 lcd.print("LCD Tutorial"); 

 delay(3000); 

 lcd.clear(); // Clears the display 

 lcd.blink(); //Displays the blinking LCD cursor 

 delay(4000); 

 lcd.setCursor(7,1); 

 delay(3000); 

 lcd.noBlink(); // Turns off the blinking LCD cursor 

 lcd.cursor(); // Displays an underscore (line) at the position to which the next character will be written 

 delay(4000); 

 lcd.noCursor(); // Hides the LCD cursor 

 lcd.clear(); // Clears the LCD screen 

}

   Day 7 h-bridge  motor control. 

My  Components  :

-   Arduino uno

-   Dc motor

-   Potential meter

-   Breadboard

-   Multimeter

-   Jumper wires

-   Power supply

-   H-bridge motor driver

![](https://lh3.googleusercontent.com/Md8hyFBq50FCB-L86w2zubES9nkMhXd2DXcY3kZtpgnI4zBqhYfcbAn3SSsyyd37Nxw8xzF7WrU7GxtXmUaLs722_YO420QmejI_GMweHnWA5ZNl5hphvn-yvlPKV2igoQ4wFPtECgXpgzC23o5WLDs)

Here below I have the code of the picture above.

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

  Serial.begin(9600);

}

void loop() {

   int duty = map (analogRead(A0),0,1023,-255,255);

     Serial.println(duty);

    analogWrite(pwm,abs(duty));

  if(duty>0){

      digitalWrite(in_1,LOW);

      digitalWrite(in_2,HIGH);

  }

  if(duty<0){

      digitalWrite(in_1,HIGH);

      digitalWrite(in_2,LOW);

  }

  if(duty==0){

digitalWrite(in_1,HIGH);

     digitalWrite(in_2,HIGH);

  }

}

![](https://lh6.googleusercontent.com/EX1LsIRVtR40U8x5OmqUYa_lUO9qbfhSPuMEW4JDJ1OvB2TPyXMIzV5QmWNtvFoVBQgRWcP4tD2e7tYaDvAhdFYDayZkuKAA3YYTDFVT0rThozYiV0vTMTlq5JUE1Z3PlbP6p41idxRaGWQTTfDMf04)

Here below I have the code of a circuit without a potential meter.

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

}

void loop() {

   //For Clockwise motion , in_1 = High , in_2 = Low

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

   analogWrite(pwm,250) ;

   /* setting pwm of the motor to 255 we can change the speed of rotation

   by changing pwm input but we are only using arduino so we are using highest

   value to driver the motor */

   //Clockwise for 3 secs

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

   //For Anti Clockwise motion - IN_1 = LOW , IN_2 = HIGH

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

}

I also had to design and build a case around my arduino uno .

![](https://lh6.googleusercontent.com/deXXSZ1mvTqSpjIsdpJ3AwkEDagDLm4Cdbd1eV38s0xAVSqfKDPhrjays_gmcEvA_uiceMaQa5gyzPlssu5GPAD0pzmZYOMJVCf70bDCOcXlkZrgR_pR-AWoeLXjbEEaf_7AEGz_zE5IBIfqhJ9XWG0)

First I went on tinkercad and then went on 3D designs

And started building my arduino enclosure case.

My mistake ?

I switched the polarity of some of the LEDs and got the connections wrong. 

I had some problems with writing the codes.

I forgot some of the lessons that I learned.

I still do not understand some of the coding.

My Final Inno Starter Project   ![](https://lh4.googleusercontent.com/LLkzYh7FupOI_u2m0Ak2NKg33vgqS8nTu3KHhdqy9so3ydUBzIeminJcuZbKHB-uGQ3YAFmK3z3yzZ_6i43M2gXqBd6Y_9l7ecu0Oywyud_sTPwIG61fOx39pE2VQVoQV0jsaud-HuarkKQ4AP92KoM)

Introduction

My final project is about a temperature humidity sensor which can be used for  monitoring  and controlling the temperature and humidity for a chicken coop or any other environment. The measurements will be displayed on an lcd screen using the Arduino (Funduino) Uno. The temperature and humidity measurements must remain between a certain level. Therefore a dc motor is attached to the circuit to keep the chicken coop cool.

To do this project you have to be familiar with the DHT11 sensor, which is a humidity and temperature sensor and the operation of the DC- motor.

List of components

The components for this project are:

- An Liquid crystal display (lcd screen),

- A DHT11 humidity-temperature sensor,

- DC motor

- Breadboard,

- Arduino (funduino) uno  

- Jumper wires.

The LCD screen:

 ![](https://lh6.googleusercontent.com/rcMX2F1N46igI2ZPf2lkYdbt3SUngIImpLRSWvW2_LBkE-LYyyq7glVnIi2UnrBj7lDbr6qgnsQk9VdbK-cUzpM-S72MPBLeSUFiQIP3jyPLNKXSE7eH3j7q9hKbDc3qOZgpPIeYtDv-YWLr8l5poNg)

LCDs are very popular and widely used in electronics projects for displaying information. There are many types of LCD. The LCD I2C has been created to simplify the wiring. Actually, LCD I2C is composed of a normal LCD, an I2C module and a potentiometer.

### Pinout

LCD I2C uses I2C interface, so it has 4 pins:

-  GND pin needs to be connected to GND (0V).

-  VCC pin the power supply for the LCD, needs to be connected to VCC (5V).

-  SDA pin I2C data signal

-  SCL pin I2C clock signal

### LCD Coordinate

LCD I2C 16x2 includes 16 columns and 2 rows.

The DHT11 sensor:  

 ![](https://lh3.googleusercontent.com/BFoyNrWO-2OMWgsdRhRtq2gMFzpMSHuLI3LZkS_EwU-u8H0rWzUhTtO2oBFOkQ0ZJZtteocHeB4y6BrcuKFhdhQPi2G7KwXmTJP80x0HqkVgyuBB9UkpcxqiA0cqnbxkEDZ3PJlCzh1qornBKnxoNxQ)![](https://lh3.googleusercontent.com/9UlZYqApTnJ2wMhM7EGIts0g734I38iqy2GI-gF33cn3e3s_WXrsyArf_iwWXMc4xVL0DkPiGXcOO4lWfDBx7YRjO4s_hHb6o19PuLlDl4ds-uVwM4PJMTRzA4LtpC_twvMSrgSgVxhpNLOIW3xVZEQ)

The DHT11 sensor has 3 pins; negative (ground), signal pin and VCC positive.`

The  [DHT11 Temperature and Humidity](https://techzeero.com/sensors-modules/dht11-temperature-humidity-sensor/)  Sensor senses, measures and regularly reports the relative humidity in the air. It measures both moisture and air temperature. The warmer the air is, the more moisture it can hold, so relative humidity changes with fluctuations in the temperature. Humidity sensors detect the relative humidity of immediate environments in which they are placed. They measure both moisture and temperature in the air and express relative humidity as a percentage of the ratio of moisture in the air to the maximum amount that can be held in the air at the current temperature. As the air becomes hotter, it holds more moisture, so the relative humidity changes with the temperature.

The temperature range of DHT11 is from 0 to 50 degree Celsius with a 2-degree accuracy. Humidity range of this sensor is from 20 to 80% with 5% accuracy. The sampling rate of this sensor is 1Hz .i.e. it gives one reading for every second.  DHT11 is small in size with operating voltage from 3 to 5 volts. The maximum current used while measuring is 2.5mA.

This sensor is used in various applications such as measuring humidity and temperature values in heating, ventilation and air conditioning systems. Weather stations also use these sensors to predict weather conditions.  The humidity sensor is used as a preventive measure in homes where people are affected by humidity. Offices, cars, museums, greenhouses and industries use this sensor for measuring humidity values as a safety measure.

The DC motor

![](https://lh5.googleusercontent.com/SulSlhuwtNYQtX3OVzbICtO9KVtAGBYg-UPQ3P_vyFkvDVHVui6aeD3wX0pi3JxXtL33vof4N9CRdcOPgPQLBvvfUpC4fMvBwNeknroEe4IDyfq6LyFXKDhL3O7TWz7S6J7bEzBJ_IVksKbdBqKC3wc)

A DC motor is any of a class of rotary [electrical motors](https://en.wikipedia.org/wiki/Electrical_motor) that converts direct current electrical energy into mechanical energy. The most common types rely on the forces produced by magnetic fields. Nearly all types of DC motors have some internal mechanism, either electromechanical or electronic, to periodically change the direction of current in part of the motor.

Building the circuit

 ![](https://lh3.googleusercontent.com/PHwJqyIWDCsKHBSK71xQkjBx_R2zIJQ3CjNISPGLYL4npOFrSASiVOmPe9PPxP8KJFJ8otZiTchxEIORXm-Yy-EvIYjJcqiHudAM0y5Qdm_wTuN7s0OI7JF7EbEe-8U6sfndITsIovf-um6KPO_kxew)

The Arduino 5 V pin is connected to the breadboard positive. The DHT11 sensor signal pin is connected on the Arduino pin 7. The ground is connected to the Arduino ground and the VCC pin is connected to the breadboard positive.

The LCD SDA pin is connected to Arduino (analog) pin A5.

The LCD SCL pin is connected to Arduino (analog) pin A4.

The LCD ground pin is connected to Arduino ground.

The LCD display VCC pin is connected to the breadboard positive.

 The code block

#include <Wire.h>

#include <LiquidCrystal_I2C.h>

#include "DHT.h"

#define DHTPIN 7 

#define PWMPIN 3

#define DIRA 4

#define DIRB 5

//#define DHTPIN 7     // what pin we're connected to

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

byte arms[8] = {

  0b00100,

  0b01010,

  0b00100,

  0b00100,

  0b01110,

  0b10101,

  0b00100,

  0b01010

};

// Uncomment whatever type you're using!

#define DHTTYPE DHT11   // DHT 11

// Initialize DHT sensor for normal 16mhz Arduino

DHT dht(DHTPIN, DHTTYPE);

void setup() {

  Serial.begin(9600); 

  dht.begin();

  Wire.begin();

  lcd.init();

  lcd.backlight();

  lcd.createChar(1, arms);  

  pinMode(PWMPIN,OUTPUT);

  pinMode(DIRA,OUTPUT);

  pinMode(DIRB,OUTPUT);

}

void loop() {

  // Wait a few seconds between measurements.

  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!

  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)

  float h = dht.readHumidity();

  // Read temperature as Celsius

  float t = dht.readTemperature();

  // Read temperature as Fahrenheit

  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).

  if (isnan(h) || isnan(t) || isnan(f)) {

    Serial.println("Failed to read from DHT sensor!");

    return;

  }

  // Compute heat index

  // Must send in temp in Fahrenheit!

  float hi = dht.computeHeatIndex(f, h);

  Serial.print("Humidity: "); 

  Serial.print(h);

  Serial.print(" %\t");

  Serial.print("Temperature: "); 

  Serial.print(t);

  Serial.print(" *C ");

  Serial.print(f);

  Serial.print(" *F\t");

  if(t>30)

    {   lcd.setCursor(0, 0);

        lcd.print("T:");

        lcd.print(t);

        lcd.setCursor(8, 0);

        lcd.print("H:");

        lcd.print(h);

        lcd.setCursor(7, 1);

        lcd.print(" ON ");

        lcd.write(1); 

        analogWrite(PWMPIN,200); // enable on

        digitalWrite(DIRA,HIGH); //one way

        digitalWrite(DIRB,LOW);

        delay(20);

        lcd.setCursor(0, 1);

        lcd.print("FAN");

        lcd.setCursor(4, 1);

        lcd.print("1");

        delay(1000);

    }

   if(t<30)

    {

        lcd.setCursor(0, 0);

        lcd.print("T:");

        lcd.print(t);

        lcd.setCursor(8, 0);

        lcd.print("H:");

        lcd.print(h);

        lcd.setCursor(7, 1);

        lcd.print(" ON ");

        lcd.write(1); 

        analogWrite(PWM PIN,100); // enable on

        digitalWrite(DIRA,HIGH); //one way

        digitalWrite(DIRB,LOW);

        delay(10);

        lcd.setCursor(0, 1);

        lcd.print("FAN");

        lcd.setCursor(4, 1);

        lcd.print("2");

        delay(1000);

    }

    if(t<25)

    {  

        lcd.setCursor(0, 0);

        lcd.print("T:");

        lcd.print(t);

        lcd.setCursor(8, 0);

        lcd.print("H:");

        lcd.print(h);

        lcd.setCursor(7, 1);

        lcd.print(" FAN OFF");

        analogWrite(PWMPIN,0); // enable on

        digitalWrite(DIRA,HIGH); //one way

        digitalWrite(DIRB,LOW);

        delay(20);

        lcd.setCursor(0, 1);

        lcd.print("3"); 

        delay(1000);

    }

}

First I had to install an extra library for the code for the DHT11 sensor and the LCD to work. Then the code was made and the circuit tested.

3D Design Enclosure

![](https://lh5.googleusercontent.com/OqhOK4sZQ7eSxRLuUo9UG-kdH0v0itzsYvn0kx0l4xK8Y2kqw-Qt--tPFmtPxuffHpVwnzXgBBKMND8MnsLXTCcFxDYwK2Si1i2QP-welrNZdDbLZFPg9ZXwyZhW7DUBYpoi3NV4NHgEtD6KCnwxvMM)

My Tinkercad Circuit.

![](https://lh6.googleusercontent.com/hSI3ylo31gRviKq80zoYnaHyIx9HOuMlgbkrtDVuliOzxcqwwIjSE-kqeCAi2rZxNBDPeMGIK2u2lNLK-stdQ7Ou2zHbnaf7AGY6I0EGX7j_EYUKa2evc_fGQRqOPp6p3tAoH_gTuHp-xM0rs0ZbgME)

![](https://lh3.googleusercontent.com/f55pKNqUbHqskjfRVV7hTBcL07w9--m_altzJTcmyrSn53OJSsc2dMiZukol9aO9pS5OsWWfzgttSJeDaWKR4p24lkIgBR8nUrknn7N9o8TVUcVmrwcAvRnFYZmnE8r7d1Tuot5-vas3IppwIJScrCM)

Here I have a picture of my final project.

Conclusion

This final project was very exciting and challenging. The coding part was difficult and I had to read a lot of codes and go through a lot of examples to understand the code for this project. I have learned a lot while doing the project. With the help of the arduino uno you can keep any environment cool.