
This is me :)

![](https://lh4.googleusercontent.com/7dWNt6lgmgCMsnTr0rAs7NygDitoLEZNlVswPbTUZ6a9uW5sm7ZdqqAhFx4lQVEsfFY-C6DISneQxHQsJxn6_1-PQ4_30HTWigBoVjy2t9ovDwPFwLbz1yh5bEo8RGaQ2Ynn5NdujOkoywMqU_HU01g)

Hi I'm Shanice and I'm 15 years old.

I'm a participant of Kosmos Codetts Inno Starter 2020.

I started to join this course because I'm very interested in                                                                                                                                                                                            building things in technical field and I like to work creatief.

Day 1

-   Objective:

-   Introduction to Arduino uno build basic blink led sample

-   Chang circuit to blink led connected to pin 12 and of 13

-   Change the speed of the blinking of the leds by adjusting the delay in the code.

-   Theory: Build a basic electronic circuit using the arduino uno to blink a led connected to digital pin 13. 

-   Handson: (Circuits) Pictures

-   Component list

-   Arduino uno 

-   Breadboard

-   Leds

-   Jumper

-   Resistor

-   Code Block (Code Explained)

-   Blink

-   Turns an LED on for one second, then off for one second, repeatedly.

Hands on 
---------

I started out building my circuit using tinkercad circuits.

The components which I used was the arduino uno microcontroller a breadboard which is a solderless circuit a led which is a light emitting diode a 220 ohm resistor and some jumper wires.

I made a power rail on my breadboard by connecting the 5v from the arduino to my plus rail on my bread board and gnd rail by connecting the gnd from the arduino uno.

I then connected my led anode leg to my digital pin 13 in parallel with my resistor and the cathode leg to the ground pin

![](https://lh6.googleusercontent.com/vNFdJjcPUGlS6I-cxqBZh_CfqX-qh_LusDbNtfxjhgcT_ECwFJEc7rhb0dBRubySexFlWOlKMSTDh_MI7D1Y1Fvk_xCMVSy7ORgLX8Dw4v2Cn-uf-bI_2UU9XNEx82iKu30JgScUurZZ3NWhwZVc8TY)

### ![](https://lh4.googleusercontent.com/Pr40IEsaK0DgaJEoBKChlVedJoyRxQquXdWNrLb84u7zyiO0ly8bIxdymiVM53VUdi83khvNovygIBSmSQrC0cOkug9Z4f8M0JHLzqo2LSypy2_yDoFyPW-77NYRzQl62VoqZUBk_Kx4bE73KZWgqdQ)  on arduino I typed my code and then copy pasted it to tinkercad to make my subject blink.

code of blink
-------------

void setup()
------------

{

  pinMode(12, OUTPUT);
----------------------

}

void loop()
-----------

{

 digitalWrite(12, HIGH);
------------------------

  delay(1000); // Wait for 1000 millisecond(s)
----------------------------------------------

  digitalWrite(12, LOW);
------------------------

  delay(1000); // Wait for 1000 millisecond(s)
----------------------------------------------

}

Code Block

I then added my basic blink led sketch to blink the led connected to pin 13

I have 2 functions the void setup and the void loop

The setup runs once and the loop runs over and over again

In my setup I initialize the digital pin 13 as output

In my setup I turn the digital pin 13 high using the digitalWrite function for 1 second then low for 1 second and repeat.

// the setup function runs once when you press reset or power the board

void setup() {

  // initialize digital pin 13 as an output.

  pinMode(13, OUTPUT);

}

// the loop function runs over and over again forever

void loop() {

  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  delay(1000);                       // wait for a second

  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW

  delay(1000);                       // wait for a second

}

The task was to change the led on the circuit to digital pin 12 so in my code I had to change the 13 to 12.

We also had to make our leds blink either faster or slower so I changed the delay from 1000 which is 1 second to 250 so it blink faster.

                      Day 2 

serie circuit
-------------

Objective:

-   Build a series circuit on tinkercad

-   If you push both buttons the led will turn on

-   Build a parallel circuit on tinker 

-   If you push one of the buttons the led will turn on

-   Build the spaceship interface on tinkercad

### component list : arduino uno, breadboard, led,  2 pushbuttons, resistor, jumper, voeding.

hands on
--------

I made a power rail on my breadboard by connecting the 5v from the arduino to my plus rail on my breadboard and gnd rail by connecting the gnd tho the arduino.
---------------------------------------------------------------------------------------------------------------------------------------------------------------

then I connected the led to the pushbutton and both pushbuttons are connected to each other. then I connected a resistor from the plus rail to 1 button.Then I connected the shorter leg from the led to the negative rail. I used a nutriton 5v and connected it to the plus rail and the neg rail.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

![](https://lh6.googleusercontent.com/vHGZd_-ZgF_C8GDpnFHzjicqtbCkZ2NWeA2hByf8_ZYBgDsXTxBkdE6C75Nn6fvyPXfMLOz_9B6pbnRPAiJ4yncBmbMZtTzhPTdwNYBwOkpq3_OGMtWglqFbQWcF3eB5jIq1foCDBHVM_HgdSAWF6wA)

code serie circuit
------------------

### void setup()

### {

###   pinMode(13, OUTPUT);

### }

### void loop()

### {

###   digitalWrite(13, HIGH);

###   delay(1000); // Wait for 1000 millisecond(s)

###   digitalWrite(13, LOW);

###   delay(1000); // Wait for 1000 millisecond(s)

### }

parallel circuit
----------------

### component list: arduino uno, breadboard, resistor, 2 pushbuttons, led, jumpers.

hands on : i made a power rail on my breadboard by connecting the 5v from the arduino to the plus rail on my breadboard and gnd rail by connecting the gnd from the arduino.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

I used a led and 2 pushbuttons which I put in the same rail. The pushbuttons are connected to each other. Then I connected a resistor from the plus rail to the push button. I connected both pushbuttons to the longer leg of the led.On the shorter leg of the led I connected a jumper to the neg rail.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

![](https://lh6.googleusercontent.com/_SN333ebQ8-TwpEy0wiCm1xjmpyK96_7A0W7mDp9vSXh8V1CDbg5qyGLFmMB9I8VrnUaeQVYAE8PHLlTf6FRhwbV72NRP1yM8GG-6f0IxEgcaacSnDxqTeJp8hybw9FXCyPODxG_zDyMleKzYDEBwbk)

code parallel circuit:
----------------------

### void setup()

### {

###   pinMode(13, OUTPUT);

### }

### void loop()

### {

###   digitalWrite(13, HIGH);

###   delay(1000); // Wait for 1000 millisecond(s)

###   digitalWrite(13, LOW);

###   delay(1000); // Wait for 1000 millisecond(s)

### }

Day 3

spaceship interface
-------------------

Objective: 

-   Build the spaceship interface using the kit

-   The green led should be on

-   If you push the button the 2 red led should be toggling

-   Build  LDR RGB using the kit

-   The photoresistor that gets light should transfer to the LDR and should give the corresponding color

Theory: Spaceship Interface 

Build a circuit connecting the LEDs to 3, 4 and 5. Put a resistor of 10kΩ connected to the pushbutton.

component list: arduino uno, breadboard,3 leds,pushbutton, resistor, jumpers.
-----------------------------------------------------------------------------

### hands on: I made a power rail on my breadboard by connecting the 5v from the arduino to my plus rail on my breadboard and the gnd rail by connecting the gnd from the arduino uno.

### I used 3 leds, 2 red ones and 1 green. I connected the first red one to pin 5, then the 2nd red one to pin 4 and the green led to pin 3. then I connected a jumper from the pushbutton to pin 2 . then a resistor from the pushbutton to the neg rail. then another jumper from the pushbutton to the plus rail.

### ![](https://lh6.googleusercontent.com/DFoSoaJgG_85B2tnLNX7O0h0h3IUnuLo1cGZqu9__RTzu9OrUV0n3dMApcR_ASah61Js20txNkoVltUg8kE8JNsWqk_zcgmWQB6__rRrB916vVjGQC1wTcHzCKOLi4ph-hHXPN6x1JsW_ZOtD2xD-lI)

code spaceshipinterface

int switchState = 0;

void setup(){

  pinMode(3,OUTPUT);

  pinMode(4,OUTPUT);

  pinMode(5,OUTPUT);

  pinMode(2,INPUT);

}

void loop (){

  switchState = digitalRead(2);

  // this is a comment

  if (switchState == LOW) {

    // the button is not pressed 

   digitalWrite(3, HIGH); // green led

    digitalWrite(4, LOW); // red led

    digitalWrite(5, LOW); // red led

  }

  else { // the button is pressed

    digitalWrite(3, LOW);

    digitalWrite(4, LOW);

    digitalWrite (5, HIGH);

    delay(250); //wait for a qaurter second 

    // toggle the LEDs

    digitalWrite(4, HIGH);

    digitalWrite(5, LOW );

    delay (250); // wait for a quarter second

  }

} // go back to the beginning of the loop

LDR RGB

Build the circuit connecting the  photoresistors (greenLED)to Pin 9, (redLED) Pin11, (blueLED) Pin10.

components : breadboard, arduino uno, photoresistors, jumpers, resistors, transistor.
-------------------------------------------------------------------------------------

hands on : 
-----------

### I made a power rail on my breadboard by connecting the 5v from the arduino to my plus rail on my breadboard and gnd by connecting the gnd from the arduino uno.

### I put 3 resistors that are connecting the 2 halfs of the breadboard  and they are 10 kilo ohm. the first one I connected to pin 9, the 2nd to pin 10 and the 3rd to pin 11. then I connected a jumper between the first and 2nd resistor to the negative rail. then I place the transistor in the same rail of the resistor and the jumper.but that's on the other half of the board. I connected a jumper from the plus rail to the other half of the board so that half can get voltage too. I place 3 resistors from 200 ohm on the first half of the board. then connected the first resistor to analog 2, the 2nd resistor to A1 and the 3rd resistor to A0. the photo resistors are connecting the 2 halfs of the breadboard too and are in the same rail of the resistor of 200 ohm.the foto resistors are getting voltage of the 2nd half of the breadboard.

![](https://lh6.googleusercontent.com/-2EzUO8B6S29geUM5DuI1BXMVvJnSwftf76Yvo_G3irh3C3ZBYsaURFlygdtKIhFhunmd_Vj-Ud2MhRB-eACr-4y2NJS-qxZ2tayzsd6yLXXjufsIFvB59iMqjQbpJuqurLU7T4EjZHonASMTbBBlvA)

the effect :

if I put  blue gel on one of the resistors, the transistor shows blue light. if i put put green gel one a resistor and red gel on the other one, then the transistor shows green and red light.

### code of this circuit : 

### const int greenLEDPin = 9;

### const int redLEDPin = 11; 

### const int blueLEDPin = 10;

### const int redSensorPin = A0; 

### const int greenSensorPin = A1; 

### const int blueSensorPin = A2;

### int redValue = 0; 

### int greenValue = 0; 

### int blueValue = 0;

### int redSensorValue = 0; 

### int greenSensorValue = 0; 

### int blueSensorValue = 0; 

### void setup() { 

### Serial.begin(9600); 

###  pinMode(greenLEDPin,OUTPUT);

###  pinMode(redLEDPin,OUTPUT);

###  pinMode(blueLEDPin,OUTPUT);

### }

### void loop() {

###  redSensorValue = analogRead(redSensorPin);

### delay(50);

###  greenSensorValue = analogRead(greenSensorPin);

### delay(50);

### blueSensorValue = analogRead(blueSensorPin);

### Serial.print("Raw Sensor Values \t Red:"); 

### Serial.print(redSensorValue);

### Serial.print("\t Green:");

### Serial.print(greenSensorValue);

### Serial.print("\t Blue:");

### Serial.println(blueSensorValue);

### redValue = redSensorValue/4;

### greenValue = greenSensorValue/4;

### blueValue = blueSensorValue/4;

### Serial.print("Mapped Sensor Values \t Red: ");

### Serial.print(redValue);

### Serial.print("\t Green: ");

### Serial.print(greenValue);

### Serial.print("\t Blue: ");

### Serial.println(blueValue); 

### analogWrite(redLEDPin, redValue);

### analogWrite(greenLEDPin, greenValue);

### analogWrite(blueLEDPin, blueValue);

### }

               analog write
===========================

int redPin=9;

int bright=127; // 0-255 "2^8 bit number / 2^8 different positions or increments that you can do

                // 0 = 0v  

// 255 = 5v

//127 = 2.5v

void setup() {

  // put your setup code here, to run once:

pinMode (redPin,OUTPUT);

}

void loop() {

  // put your main code here, to run repeatedly:

  analogWrite (redPin,bright);

}

map

Analoginput = 2^10 = 0-1023 (10 bits)
-------------------------------------

Analogoutput = 2^8 = 0-255 (8 bits)
-----------------------------------

day 4

love o meter 
-------------

Objective: 

-   Build the love-o-meter (indicates the temperature) using the kit

-   If the temperature is lower than the base temperature the LEDs should be off

-   If the temperature is  higher/ equal to the base temperature+2 & if the temperature is lower than the base temperature +4 one led should be on

-   If the temperature is higher/ equal to the base temperature+4 & if the temperature is lower than the base temperature+6 two LEDs should be on

-   If the temperature is higher than base temperature + 6 all three LEDs should be on

### components :  arduino uno, breadboard, jumpers, resistors,3 leds, temperature sensor.

hands on : 
-----------

### I made a power rail on my breadboard by connecting the 5v from the arduino uno to the to my plus rail on my breadboard and gnd rail by connecting the gnd from the arduino uno.

I have 3 leds. on each short leg of the led I connected a resistor of 200 ohm to the negative rail. Then I connected on each longer leg of the leds a jumper. The jumper of led 1 is connected to pin 4, led 2 is connected to pin 3 and led 3 is connected to pin 2. Then I placed the TMP36 on the breadboard ( the order of the pins is important ). Then I connected the left leg of the TMP36 to the negative rail, the right leg to the plus rail and the middle leg to analog 0. ( I connected a jumper from the negative rail in the same rail because the breadboard has 2 halfs, the same I did with the plus rail. )
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

![](https://lh3.googleusercontent.com/JTb3oV_b5R5rINHT9_3QbgkQ1BpuJ1is0gWxjQYOabgDVnIglFwBjizs0741g0JPmDcjFzNgbd8fPCuv9DGEkNJr8GDtDtVmbfuRKjwGwSjzyoLeWQBllwmfi7Trnx060u-g1DHg_PYPaDygMteRDtg)

###  code of the love-o-meter

###  Serial.begin(9600); // open a serial port

###  for(int pinNumber = 2; pinNumber<5; pinNumber++){

### pinMode(pinNumber,OUTPUT);

### digitalWrite(pinNumber, LOW);

###  } 

### void loop(){

### int sensorVal = analogRead(sensorPin);

### Serial.print("Sensor Value: "); 

### Serial.print(sensorVal);

### float voltage = (sensorVal/1024.0) * 5.0;

### Serial.print(", Volts: ");

### Serial.print(voltage);

### Serial.print(", degrees C:"); 

###  // convert the voltage to temperature in degrees

###  float temperature = (voltage - .5) * 100;

### Serial.println(temperature);

### if(temperature < baselineTemp){

### digitalWrite(2, LOW);

### digitalWrite(3, LOW);

### digitalWrite(4, LOW);

### }else if(temperature >= baselineTemp+2 && 

###  temperature < baselineTemp+4){

### digitalWrite(2, HIGH);

### digitalWrite(3, LOW);

### digitalWrite(4, LOW);

###  }else if(temperature >= baselineTemp+4 && 

###  temperature < baselineTemp+6){

### digitalWrite(2, HIGH);

### digitalWrite(3, HIGH);

### digitalWrite(4, LOW);

###  }else if(temperature >= baselineTemp+6){

### digitalWrite(2, HIGH);

### digitalWrite(3, HIGH);

### digitalWrite(4, HIGH);

###  }

### delay(1);

### } 

                                            Day 5
-------------------------------------------------

micro servo
-----------

### components : bread board, arduino uno, jumpers, capacitor, servo motor, potentiometer.

### hands on :

### I attached 5v and ground to one side of the bread board from the arduino uno.

### so I placed a potentio meter and above a condensator. then I connected the right leg of the condensator to the negative rail and the left leg to the  the plus rail. The middle leg of the potentio meter is connected to analog  0. I placed another potentio meter which the right leg is connected to the neg rail, the middle leg connected to the plus rail and the left leg is connected to pin 9.THEN I placed  the servo meter beside the board and connected the ground of the servo meter to the right leg of the potentio meter and the voltage to the middle leg of the potentio meter and the signal to the left leg. ( i connected the 2 halfs of the bread board ( the plus rail and the neg rail ).

![](https://lh5.googleusercontent.com/u71RZwTQOWYuf3d2LeZcxgma1w_KaDvlm-y2cZm7LxkBPnOGcbcOixQQBgPicj4_1eQ65Tv2Agfk1R3L4X9sHrlBx5ttFh8eHsMHgHOkozjkpuBdilnFUdXkksJ__HymW4opzmbL5TzaPUtyAp0gdik)

###                                           code of the micro servo

#### #include <Servo.h>

#### Servo myServo;

#### int const potPin = A0;

#### int potVal;

#### int angle;

#### void setup () {

####   myServo.attach(9);

####   Serial.begin(9600);

#### }

#### void loop() {

####   potVal = analogRead(potPin);

####   Serial.print("potVal: ");

####   Serial.print(potVal);

####   angle = map(potVal,0,1023,0,179);

####   Serial.print(", angle: ");

####   Serial.println(angle);

####   myServo.write(angle);

####   delay(15);

#### }

#### Day 6

Arduino Lcd Basic Text

Objective:

-   Build a LCD circuit

-   Build a LCD circuit (with shift)

Components list:

-   Jumpers

-   LCD

-   Potentiometer

-   Arduino Uno

CODE

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5,  d6, d7)

void setup() { 

lcd.begin(16,2); // Initializes the interface to the LCD screen, and specifies the dimensions (width and height) of the display } 

}

void loop() { 

 //lcd.print("Arduino"); // Prints "Arduino" on the LCD 

lcd.print("Hello_world"); // Prints "Arduino" on the LCD 

 delay(3000); // 3 seconds delay 

 lcd.setCursor(2,1);  // Sets the location at which subsequent text written to the LCD will be displayed 

 lcd.print("LCD Tutorial"); 

 delay(3000); 

 lcd.clear(); // Clears the display 

 lcd.blink(); //Displays the blinking LCD cursor 

 delay(4000); 

 lcd.setCursor(7,1); 

 delay(3000); 

lcd.noBlink(); // Turns off the blinking LCD cursor 

 lcd.cursor(); // Displays an underscore (line) at the position to which the next character will be written 

 delay(4000); 

 lcd.noCursor(); // Hides the LCD cursor 

 lcd.clear(); // Clears the LCD screen 

}

![](https://lh5.googleusercontent.com/rdmhJ6pQRaEeTuQ0YuQte-YAWAWqgD1FM9efaVfRdFyc9NwL_AtU7G5SaeJWhGqeKm9lOvYTLfBw2gRiTkCauhRw5za95eghvp6QVTUifiQRC9sgCVrRWjOF_YAp776iMf1XgZGoSvCGD--4lvpgx7o)

LCD I2C

 Components list:

-   Jumpers

-   LCD

-   Potentiometer

-   Arduino Uno

CODE

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

void setup() {

  lcd.init();

  lcd.backlight();

  lcd.print("Hello World!");

}

void loop() {

  lcd.scrollDisplayLeft();

  delay(500);

}

![](https://lh3.googleusercontent.com/4011BodlXdaPzcv316VBMFt67_QsugdvYZqRLH0kgV5pHtJjH05D-c0PGUQNgp380YTRXup2_uIQxA0uIhPIJe3OFXT5jZ5T9dhXO31ADDRnD8Xv1gSkgRsDawsoVf15u2PGsYbgbONOu_pO38krVfM)![](https://lh4.googleusercontent.com/eaTiQj605osOFG5Vs3Z4u9xwI3wbjfbm2osU_4sQFCpTfAKOeLXxpXSeuZ6h5-z7JbCbT4_AqXOPb_RNye4jrYDxjdOjOXDbKQY7Eqq7Y5yfkyEay8EDUc9FaGlpX1OsBVzrYrLOoBlvzRxf6skQLRE)![](https://lh6.googleusercontent.com/qwzi0r-1e39aVv0pNcC1rfG71nt4ezJ7iImE9KW4kecMOjIOF4975_NejUzMfi4j-FEqoivLoQaclKTVWSKKTdq5jbV0RBAn4lCWqeCuRRRYNKQFGJMKEHpMZP6wv0TiHz4iUsgGAZeYc5i9GX-NIe4)![](https://lh5.googleusercontent.com/zzk-h9vk2M2jWRyxMoiMz7mSGlHYXucX5k3g_i73Acq9G5rEqRXkOeCbqoYJnzZGHnIgyX6qFqJE-2SMpusXqFKVSGIA_qZT9q_5cb8TnjjxzDUyXTlAc_47o_3Y_Ny2lHqSWwAoi_y4Qq5Ad9j5Xo8)

Own Project 

Objective:

-   Build a circuit using LCD I2C 

-   Add a pushbutton

-   If button is pushed the screen text will scroll to the left

-   If button is not pressed the screen text will scroll to the right

Code

#include <LiquidCrystal_I2C.h>

#include <Wire.h> 

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

int switchState = 0;

void setup() {

  pinMode (2,INPUT);

  lcd.init();

  lcd.backlight();

  lcd.print("Darlene & Shanies!");}

void loop() {

  switchState = digitalRead(2);

    if (switchState== LOW){

  lcd.scrollDisplayRight();

  delay(500);

    }

   else {

     lcd.scrollDisplayLeft();

  delay(500);

   }

   }

Day 7

Objective:

-   Tinkercad 3D design

-   Design a case for your arduino uno

-   Make in tinkercad a H-bridge

-   Make in tinkercad a h-bridge with potentiometer

![](https://lh5.googleusercontent.com/_xTE0ztpBO2zByIrJ6amlGKhqC02q9u72vJ4O9PZ6Wp7VUYUXw_1lP07cQDL2LLH7xsSxUmHIvL6Mi3X2Sgezw4yYg4KuSfvD91qKhGYNfBnysH7Rt9365C0xMs5lMTTyz94m9FI7GpS0q81bVs_ePM)

                                     Final project
==================================================

TEAM 
=====

Darlene Gefferie

Shanice Ramdien.

PROJECT
=======

Arduino Uno Automatic hand sanitizer Dispenser

Problem

The Coronavirus covid-19 has spread very quickly throughout the country in recent times. As a result, a number of measures have been taken. When you step into shops or companies etc. your temperature is often measured and you have to disinfect your hands. Many places use a spray bottle that a worker sprays for you or you have to spray it yourself. if the person entering the place has to do it himself, there is a high risk of spreading because many people touch the same bottle.

Solution 

Our Automatic hand sanitizer dispenser is contactless so that the virus can be spread less. It is easy to use and very effective.

CODE
====

#include <Servo.h>

Servo servo1;

int trigPin = 10;

int echoPin = 11;

long distance;

long duration;

void setup() 

{

servo1.attach(9); 

 servo1.write(90);

 pinMode(trigPin, OUTPUT);

 pinMode(echoPin, INPUT);// put your setup code here, to run once:

 Serial.begin(9600);

}

void loop() {

  Serial.print("Distance: ");

  Serial.print(distance);

  Serial.println(" cm");

  ultra();

 if(distance <10 && distance >0 )// if ultrasonic sensor detects less than 10cm but bigger than 0 (in case error).

  {

    Serial.println("Dispensing soap");  

    servo1.write(55); //servo rotates at full speed to the right

    delay(2000);

    // go back to middle position

    Serial.println( "Reset");

    servo1.write(90);

    delay(3000);

    Serial.println ("Pause");

  }

}

void ultra(){

  digitalWrite(trigPin, LOW);

  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);

  delayMicroseconds(10);

  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  distance = duration*0.034/2;

  }

#### ![](https://lh5.googleusercontent.com/bagVg8uzQhpeOgAY0x40Xz0dRKb_nyRTbJMBGlnrMeWsReqaKn7WHZAL0LVDDvnJ1TliLHW-Ucn2-mATMNYy0uLTbgOj-s3KjMO036sXaRDNVc2sdtpNqtLgFBekYGre0s89KSNfSol_x3tV-Lrlo4E)

![](https://lh3.googleusercontent.com/A-zLNxmFE9qZ8DF_bhjW6wo_fF1OBnb29n1TBJbjKIcTLJI_ZRG8QQV_MQUmnNLPXkkotT7q-0mUx83LiP2aYN8vOwH3cFPQp_2sRX5_cV3N8fZOxxuaTPjuGq27kctBQeUZxPDvnWhvNHdd2Wr4PMs)

![](https://lh4.googleusercontent.com/pq9r6Smto5ilWsmdAH1P8DdfZALPeqgGJOkAeIMBAz7ufkiXO8l3priZMuxrgJP3mULTI-NxUrSU7-SYCOsWW1du2xIyb0fWluO8gOr-Eg36UrUL32W3wGKg7upRsIgBzf1NZ1YN0lF7WSuUVcRWGiY)![](https://lh4.googleusercontent.com/_EDiIAExZDi-noIYBGeA6PQXbiZ8UcLki7TwhspmvVtw51c9HiPBD6yyeaqtnZoxSg2_TzTFu3s-eZVzwsLy0U1tFhbJi-L2jfczNQ38pViE0Wq5pTF1IWW4qoglF7RPzNars9cblDWV_NW7Ek0NFwY)

####  (final project ) Autospencer

#### ![](https://lh6.googleusercontent.com/uCf8U8dq9MewqjWPhvcT5i_gCtlfi0OKJaiqommX1-lC0AUB5vVJ9NhbCliwdZyNOc11B6-LNYYoOnMOWVrDvgJXYPaSXzZ0fK3-HGbWmZ8OexKzYaEKv9rnD-gt-Ni5r9OuBTsx0wgB_QDjwMCftlE)