
About me

 Hi, I'm Shayenne Overeem ,18 years old and a student at the Natuur Technisch Instituut. I love reading and finding out about new things. I have a passion for coding and programming. There was a time where i didn't know anything about coding or what programming was.But I loved the idea of making websites and applications.Then i got selected at Natin, study field :ICT, There I learned about the arduino and breadboard and now i want to know more about this and embedded programming .That is why I chose to sign up for this program .

![](https://lh6.googleusercontent.com/do6bWb3YMVNgNMFbkWqFrfEar_kjP6VlIfFHlImY5VJZk2wqNJggvc8GwS-_SSBBY1K7YuOo4xwa69kU8EEv4YBQDmZmAgWNu31lgSvTSIdLlw6qz7ShG-ON64mzTVF_ORljlUeIsBcHlQDZN4Fn-6c)

Day 1         Date:17-02-2020
=============================

Objective:
----------

-   Introduction  Introduction to Arduino uno build basic blink led sample

-   Change circuit to blink led connected to pin 12 and of 13

-   Change the speed of the blinking leds by adjusting the delay in the code.

Theory:
-------

                  Build a basic electronic circuit using the arduino uno to blink a led           arduino uno to blink a led connected to digital pin 13                              

 Handson:Circuits
-----------------

-   Component list
    --------------

-   Arduino uno

-   Breadboard                    

-   Leds

-   Jumpers

-   Resistor

Hands on
--------

I first opened the box which had the different components I needed,then I took out the arduino,the breadboard and some jumpers and 1 led.

.![](https://lh3.googleusercontent.com/nCLfk_jsEz-N3WBBkf0o9g6xEFKgkb4deGdEUAcO4XtUAnzB16eR1HjtoKklrg9lSCZ9E9NEda4dGwUEnNCT_KIRWvpxQgoBqc9KKpy4pL3W_8dT4LToEUEWfBdrqs7NN6u6JNOmvuh7jXzbX7xa6TM)![](https://lh4.googleusercontent.com/sKemVgbIx9DwJ3Ej6zHVAzQSKKSjtzwNyLZXL_TklDtP8hT0zo_N1HkvtRtti3Gq9lI6X3Hr7qSIkDm143iK2Nzfhxo3xv53p4Mz-_qTBU1l9WvupyrtiIotgdJPjP580fW-Mard4rIX5o7kv1YL82c)

Block Code:
-----------

-   ### BLINK DEFAULT

// the setup function runs once when you press reset or power the board

void setup() {

  // initialize digital pin LED_BUILTIN as an output.

  pinMode(LED_BUILTIN, OUTPUT);

}

// the loop function runs over and over again forever

void loop() {

  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)

  delay(1000);                       // wait for a second

  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW

  delay(1000);                       // wait for a second

}

-   ###     BLINK PIN 12

void setup()

{

  pinMode(12, OUTPUT);

}

void loop()

{

  digitalWrite(12, HIGH);

  delay(250); // Wait for 1000 millisecond(s),Change delay: 250

  digitalWrite(12, LOW);

  delay(250); // Wait for 1000 millisecond(s)

I have 2 functions called the void setup and the void loop.

The setup runs once and the loop runs over and over again. 

// the setup function runs once when you press reset or power the board

void setup() {

  // initialize digital pin 13 as an output.

  pinMode(13, OUTPUT);

}

// the loop function runs over and over again forever

void loop() {

  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  delay(1000);                       // wait for a second

  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW

  delay(1000);                       // wait for a second

}

The task was to change the led on the circuit to digital pin 12 so in the code I had to change the 13 to 12.

We also had to make our leds blink either faster or slower so I changed the delay from 1000 which is 1 second to 250 so it blink faster

MISTAKES MADE? What have you learned

I learned about the code ,and the 2 functions.

Day 2            Date:21-02-2020
================================

Serie Circuit
-------------

Objective
---------

-   We got introduced to Series and Parallel Circuits

-   We got to build a few circuits to understand how the current flowed between Series and Parallel

-   We got taught how to  enumerate  the current with The Ohm's Law

-   Atlast we got taught how to use a voltmeter and a multimeter in Tinkercad

Theory
------

We had to build a series and parallel circuit and determine with a multimeter ,what actually happens to the current.

Hands On: (serie)
-----------------

![](https://lh5.googleusercontent.com/ABsUI7i_QZ36bSF5YoUNniyz3YeE_VSJ4ffwryf2WS2WS_OJ-obtMkUfLQJVI_3nxIIGpQGeSmL6Bcd51ORnt7yCedcyZ8jlwFcYGg7BZxHmWzJA0MHotLBISB3ZPzGJBlttX79zT_Gv9B3VSocICZE)

First we added one switch to make the basic LED circuit.Then we added another switch on the breadboard and wire them together. Above you can see a s Circuit.

Component list
--------------

-   Arduino Uno

-   Breadboard(solderless)

-   Jumpers(3)

-   Resistor (220 Ohm)

-   LED

-   Pushbuttons (2)

Code Block
----------

There was no code used in this circuit.

Parallel Circuit
----------------

Objective
---------

-   We got introduced to Series and Parallel Circuits

-   We got to build a few circuits to understand how the current flowed between Series and Parallel

-   We got taught how to  enumerate  the current with The Ohm's Law

-   Atlast we got taught how to use a voltmeter and a multimeter in Tinkercad

Theory
------

We had to build a series and parallel circuit 

Hands On: (Parallel)
--------------------

![](https://lh4.googleusercontent.com/p0crPGReCA9Rq0XAVrDMWnwgpZmNWQE4tRSTvNvAydTnKQGpq_slKLn40JK_DUyOLLUra23Dn0Ko9reit_PjqzKNZG2YC870YuIUj2dkWkNVue3kKSrx4TALVGQrVSbCPubhtloMQz4iGQvS-z-8TCE)

First we added one switch to make the basic LED circuit.Then we added another switch on the breadboard and wire them together. Above you can see a parallel Circuit.

Component list
--------------

-   Arduino Uno

-   Breadboard(solderless)

-   Jumpers(3)

-   Resistor (220 Ohm)

-   LED

-   Pushbuttons (2)

Day 3 24-02-2020  

Spaceship Interface
-------------------

Objective
---------

-   We learned  about Spaceship Interface

-   We were taught how the codes of spaceship interface works and what they mean.

-   We learned how to build a Spaceship Interface circuit

Theory
------

    Build a Spaceship Interface circuit using Arduino Uno and a Breadboard

Hands on (Spaceship):![](https://lh4.googleusercontent.com/CWcBS34GtUw9j6G9heqWCSreQAOMjnjtFjhaMPo3C21gMy8Zl-JlPt4LkKGVtFGJALnZanmvOqXYCnnDcgX0sqaAJsxODMdoJ7aKjOvWHQjIFFLEuHHo8Veti3YcQdjbOpODcJlFR5DGsHsnpjAuj4U)

The spaceship was built with 5 steps:

-   Power and ground should be wired up (the red and black jumper)

-   The LED's get placed on the breadboard.

-   Then the resistors (220 Ohm) get placed  on the board.

    Day 4 28-02-2020
--------------------

LDR (RGB)
---------

Objective
---------

-   We were introduced to LDR(RGB) and what its function is

-   We were taught how to map the code

Theory
------

Build an LDR circuit and find out what color the LDR will show when the light intensity is increased or decreased.

Handson (Circuits): Pictures
----------------------------

![](https://lh5.googleusercontent.com/jKx3mA7mU-ppG4eXYE88bGlgo7WhBs2vX90-uDpqM3eBsY9MftXJOAk6-RsQAogAUFr11TmCZACf0x7C7ICkGD1KhhaNJswerqtUZ5vmsTum_LrEQqWIrEH6szE-2y5JyT-NzHffDZza9pMoqxeaKU0)

Day 5 2-03-2020
---------------

      Worked on documentation and presented Day 1 before the class.

Day 6 6-03-2020    
-------------------

Arduino LCD circuit
-------------------

Objective
---------

-   We learned an easier code for the LCD

Theory
------

Build an Arduino LCD circuit

Handson (Circuits): Pictures
----------------------------

Code Blocked (Code Explained)
-----------------------------

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

void setup() {

  lcd.init();

  lcd.backlight();

  lcd.print("Hello World!");

}

void loop() {

  lcd.scrollDisplayLeft();

  delay(500);

}

Component list
--------------

-   Arduino Uno

-   Potentiometer

-   USB Cable

-   Jumpers (9)

-   Breadboard (solderless)

Build your own project: LCD and temperature sensor
--------------------------------------------------

Objective
---------

Theory
------

For my build your own project I use a temperature sensor and LED lamps. If the temperature is higher than the baseline(31) the screen will show that the  temp. Is high and the lamp will light up.And if the temp is lower ,the screen will show temp is low and the led will not light up

Handson (Circuits): Pictures
----------------------------

Code Blocked (Code Explained)
-----------------------------

I2C Board of LCD Arduino

GND <---> GND

VCC <---> 5V

SDA <---> A4

SCL <---> A5

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

const int sensorPin = A0;

const float baselineTemp = 31.0;

void setup() {

  pinMode (11,OUTPUT);

  digitalWrite(11, LOW);

  lcd.init();

  lcd.backlight();

  Serial.begin(9600);

}

void loop() {

  int sensorVal = analogRead(sensorPin);

  Serial.print("Sensor Value: "); 

  float voltage = (sensorVal/1024.0) * 5.0;

  Serial.println(", Volts: ");

  Serial.print(voltage);

  Serial.println(", degrees C: "); 

  float temperature = (voltage - .5) * 100;

  Serial.println(temperature);

  Serial.println(sensorVal);

 if(temperature < baselineTemp){

   digitalWrite(11, LOW);

   lcd.print ("Temperature is HIGH");

 }

 if (temperature = baselineTemp && temperature > baselineTemp+1){

   digitalWrite(11, HIGH);

   lcd.print("Temperature is LOW");

   lcd.scrollDisplayLeft();

   delay(500);

 }

}

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   Temperature sensor (1)

-   Jumpers (18)

-   USB Cable

Day 7 9-03-2020
---------------

3D design (Tinkercad)
---------------------

Objective
---------

-   We learned how to use tinkercad for 3D designing

-   How to group figures

Theory
------

    Build a 3D design for your Arduino uno

Handson (Circuits): Pictures
----------------------------

![](https://lh4.googleusercontent.com/dUIkDSvD2Ugtg0a7kdvgQ5h58twKrpfe6lm5ZmWq6tpiKd6-3iJwr0scfoVRilrnsoaHTJWSxgPL4qUC6HVUtJmpLX0QDY9qJV3o1arKUCzuzFXkx9-IRPPl28plrxWZnj1SifVZnBfVbzuH4KUrXmM)

H Bridge
--------

Objective
---------

-   We learned what H bridge is 

-   And how to build this (theory)

Theory
------

  A H-Bridge is a circuit  used to move a motor in one way(Forwards or Backwards).

Handson (Circuits): Pictures
----------------------------

![](https://lh5.googleusercontent.com/-ad4GnXzV36a8jsmLoHzRK9jcJkJvl6mZKtBfmuVhhG4WWRH95T_pskE9AKfpgoAhp7UyvH2h-ixUS-xMidlnUPfs0pa6gPE5FLZeriTsqxglZIj2HRnZxMImIxbDR1SJYGTbRZxC6gVmem3Q78kBts)![](https://lh3.googleusercontent.com/mAnmuRXMeMpCsWjwZ5MO5r0K8QStXB9B17s28K8nfXoWtxxRz-5wlc6X4Atce02-jOnA5vquro7GRCBtdUGBSSwybpN1oZzK3rCkJn6krFYzN-O8tLFNqeAXDSxw4oWNwRdEwerOPdfrYGsoe75Mkyw)

Final Project Research 
-----------------------

For the final project we chose Patient monitoring . It is needed because patients are often alone and sometimes it could go wrong and then you have this kind of device that can warn the doctors or nurses .

### Materials required

1.  [Arduino Uno](https://quartzcomponents.com/products/arduino-uno) and [Programming Cable](https://quartzcomponents.com/products/arduino-uno-programming-cable)

2.  [ESP8266 Wi-Fi module](https://quartzcomponents.com/products/esp8266-01-wifi-module)

3.  [LM35 temperature sensor](https://quartzcomponents.com/products/lm35-temperature-sensor)

4.  [Pulse rate sensor](https://quartzcomponents.com/products/heart-beat-pulse-sensor-module)

5.  [Push button](https://quartzcomponents.com/products/push-button-4pin-tactile-micro)

6.  [10k Resistor](https://quartzcomponents.com/products/10k-ohm-1-4-watt-resistor)

7.  [Male-female wires](https://quartzcomponents.com/products/male-to-male-and-female-to-male-combo-wires-set-of-10-10)

8.  [Breadboard](https://quartzcomponents.com/products/gl-12-830-points-solderless-breadboard)

ThingsSpeak: Write API keys: HSA6QNG0GBQI49K4|| Read API:4NUVI3I4ZVU5ORGD

Because the sensors may not come on time ,I have decided to build another project that is a small IOT greenhouse.

Link video:<https://www.youtube.com/watch?v=ow1IkOC8fyY>

Link website:<https://wickeytips.blogspot.com/2016/04/iot-greenhouse-project.html>

NOTE: I did use other sensors like the LilyGo TTGO HiGrow plant sensor.

Building Greenhouse 11-09-2020
------------------------------

The greenhouse is made out of wood.

Pictures shown below:

![](https://lh6.googleusercontent.com/NZJVDfTy9sLcC1sXJeOfyK7djQJNx3N4BR1oo0fQYZCTTjHiI3kR0G4bf5jocuNEHYkpZ4ARhHxfyd4Ua2dJii3TZQZvOw3ypychWphoZM58umcL_M9AIm5KxtJQ1ESkTm4unZlOtZeIWSoEgQSOVpw)

ENCLOSURE
---------

The enclosure is a 3D printing.

First I made a design in tinkercad.

Picture below:

![](https://lh5.googleusercontent.com/4J5ujAhsXK7R8pVrXgeWEpeWARj5-jQoIQTcNJBT6BnPAy_3wywTFGJ_1A_lS2VPKUvKhtRMpgcWhVvWyJ_QBLzn6LUpdor0SunyZoBaT-VJjl9yw_FRTXOtUVOyZnVJlOPEcu3Hd9AMideMaMFwhSo)

Then i downloaded the stl file.

The steps to print the enclosure are:

1\. Import stl into cura

2\. Check parameters make sure cooling is on on advanced tab

3\. Save as gcode on sd card

4\. Put sd card into printer 

5\. Go to axis home all 

6\. Go to print select ur file

7\. Print

The cura parameters are:

![](https://lh5.googleusercontent.com/u7G5Eh6PLaaPPL8GZjduOxhpbY6uDWc_CIkE8z09aBfj1cBL_ZQ2zvxh03v4a9q0eSU1O1TybzPpMsl6PhjQjHm8SrO9Aeiq6d8Hd5RgHSIUNvKwo0oi09pNYJDSq7oFzGnbaVkpcZ_c1t0sSv9so-w)

:

Coding Arduino IDE 12-09-2020
-----------------------------

Reference video:<https://www.youtube.com/watch?v=7w6_ZkLDxko&t=1265s>

#include <Arduino.h>

#include <Wire.h>

#include <BH1750.h>

#include <DHT.h>

#include <Adafruit_BME280.h>

#include <WiFi.h>

#include <NTPClient.h>ì

#include <ArduinoJson.h>

#include <SD.h>

#include <SPI.h>

#include <PubSubClient.h>

#include "driver/adc.h"

#include <esp_wifi.h>

#include <esp_bt.h>

// Logfile on SPIFFS

#include "SPIFFS.h"

//           rel = "1.2"; // Corrected error if Network not available, battery drainage solved by goto sleep 5 minutes

//           rel = "1.3"; // Corrected error if MQTT broker not available, battery drainage solved by goto sleep 5 minutes

//           rel = "1.4"; // Calibrate SOIL info messages where to be done

//           rel = "1.5"; // Implemented logfile in SPIFFS, and optimizing code. (DHT11 not implementet, but it only affects Air Temperature and Air Humidity, which I currently not use in my project. It will be implemented in a later release.

//           rel = "1.6"; // Implemented MQTT userid and password, and adapted so DHT11, DHT12 and DHT22 can be used.

const String rel = "1.7"; // Correcting error, that counter sleep5no was not updated.

// *******************************************************************************************************************************

// START userdefined data

// *******************************************************************************************************************************

// Turn logging on/off - turn read logfile on/off, turn delete logfile on/off ---> default is false for all 3, otherwise it can cause battery drainage.

const bool  logging = false;

const bool  readLogfile = false;

const bool  deleteLogfile = false;

String readString; // do not change this variable

// Select DHT type on the module - supported are DHT11, DHT12, DHT22

//#define DHT_TYPE DHT11

#define DHT_TYPE DHT12( because the sensor has a DHT12)

//#define DHT_TYPE DHT22

(EERSTE DEEL)

Note: I used another code ,because for this one you needed  HomeAssistant

The code I used is from the lilygo website(Julie helped me with some issues I had ,such as the wrong ESPdash and my ArduinoJson)

CODE

#include <algorithm>

#include <iostream>

#include <Arduino.h>

#include <WiFi.h>

#include <AsyncTCP.h>

#include <ESPAsyncWebServer.h>

#include <ESPDash.h>

#include <ESPmDNS.h>

#include <Button2.h>

#include <Wire.h>

#include <BH1750.h>

#include <DHT12.h>

#include <Adafruit_BME280.h>

#include <WiFiMulti.h>

#include "esp_wifi.h"

#define SOFTAP_MODE

// #define USE_18B20_TEMP_SENSOR

// #define USE_CHINESE_WEB

// Simple ds18b20 class

class DS18B20

{

public:

    DS18B20(int gpio)

    {

        pin = gpio;

    }

    float temp()

    {

        uint8_t arr[2] = {0};

        if (reset()) {

            wByte(0xCC);

            wByte(0x44);

            delay(750);

            reset();

            wByte(0xCC);

            wByte(0xBE);

            arr[0] = rByte();

            arr[1] = rByte();

            reset();

            return (float)(arr[0] + (arr[1] * 256)) / 16;

        }

        return 0;

    }

private:

    int pin;

    void write(uint8_t bit)

    {

        pinMode(pin, OUTPUT);

        digitalWrite(pin, LOW);

        delayMicroseconds(5);

        if (bit)digitalWrite(pin, HIGH);

        delayMicroseconds(80);

        digitalWrite(pin, HIGH);

    }

    uint8_t read()

    {

        pinMode(pin, OUTPUT);

        digitalWrite(pin, LOW);

        delayMicroseconds(2);

        digitalWrite(pin, HIGH);

        delayMicroseconds(15);

        pinMode(pin, INPUT);

        return digitalRead(pin);

    }

    void wByte(uint8_t bytes)

    {

        for (int i = 0; i < 8; ++i) {

            write((bytes >> i) & 1);

        }

        delayMicroseconds(100);

    }

    uint8_t rByte()

    {

        uint8_t r = 0;

        for (int i = 0; i < 8; ++i) {

            if (read()) r |= 1 << i;

            delayMicroseconds(15);

        }

        return r;

    }

    bool reset()

    {

        pinMode(pin, OUTPUT);

        digitalWrite(pin, LOW);

        delayMicroseconds(500);

        digitalWrite(pin, HIGH);

        pinMode(pin, INPUT);

        delayMicroseconds(500);

        return digitalRead(pin);

    }

};

#define I2C_SDA             25

#define I2C_SCL             26

#define DHT12_PIN           16

#define BAT_ADC             33

#define SALT_PIN            34

#define SOIL_PIN            32

#define BOOT_PIN            0

#define POWER_CTRL          4

#define USER_BUTTON         35

#define DS18B20_PIN         21                  //18b20 data pin

BH1750 lightMeter(0x23); //0x23

Adafruit_BME280 bmp;     //0x77

DHT12 dht12(DHT12_PIN, true);

AsyncWebServer server(80);

Button2 button(BOOT_PIN);

Button2 useButton(USER_BUTTON);

WiFiMulti multi;

DS18B20 temp18B20(DS18B20_PIN);

#define WIFI_SSID   "your wifi ssid"

#define WIFI_PASSWD "you wifi password"

bool bme_found = false;

void smartConfigStart(Button2 &b)

{

    Serial.println("smartConfigStart...");

    WiFi.disconnect();

    WiFi.beginSmartConfig();

    while (!WiFi.smartConfigDone()) {

        Serial.print(".");

        delay(200);

    }

    WiFi.stopSmartConfig();

    Serial.println();

    Serial.print("smartConfigStop Connected:");

    Serial.print(WiFi.SSID());

    Serial.print("PSW: ");

    Serial.println(WiFi.psk());

}

void sleepHandler(Button2 &b)

{

    Serial.println("Enter Deepsleep ...");

    esp_sleep_enable_ext1_wakeup(GPIO_SEL_35, ESP_EXT1_WAKEUP_ALL_LOW);

    delay(1000);

    esp_deep_sleep_start();

}

bool serverBegin()

{

    static bool isBegin = false;

    if (isBegin) {

        return true;

    }

    ESPDash.init(server);

    isBegin = true;

    if (MDNS.begin("soil")) {

        Serial.println("MDNS responder started");

    }

    // Add Respective Cards

    if (bme_found) {

#ifdef USE_CHINESE_WEB

        ESPDash.addTemperatureCard("temp", "BME传感器温度/C", 0, 0);

        ESPDash.addNumberCard("press", "BME传感器压力/hPa", 0);

        ESPDash.addNumberCard("alt", "BME传感器高度/m", 0);

#else

        ESPDash.addTemperatureCard("temp", "BME Temperature/C", 0, 0);

        ESPDash.addNumberCard("press", "BME Pressure/hPa", 0);

        ESPDash.addNumberCard("alt", "BME Altitude/m", 0);

#endif

    }

#ifdef USE_CHINESE_WEB

    ESPDash.addTemperatureCard("temp2", "DHT12传感器温度/C", 0, 0);

    ESPDash.addHumidityCard("hum2", "DHT12传感器湿度/%", 0);

    ESPDash.addNumberCard("lux", "BH1750传感器亮度/lx", 0);

    ESPDash.addHumidityCard("soil", "土壤湿度", 0);

    ESPDash.addNumberCard("salt", "水分百分比", 0);

    ESPDash.addNumberCard("batt", "电池电压/mV", 0);

#else

    ESPDash.addTemperatureCard("temp2", "DHT Temperature/C", 0, 0);

    ESPDash.addHumidityCard("hum2", "DHT Humidity/%", 0);

    ESPDash.addNumberCard("lux", "BH1750/lx", 0);

    ESPDash.addHumidityCard("soil", "Soil", 0);

    ESPDash.addNumberCard("salt", "Salt", 0);

    ESPDash.addNumberCard("batt", "Battery/mV", 0);

#endif

#ifdef USE_18B20_TEMP_SENSOR

    ESPDash.addTemperatureCard("temp3", "18B20温度/C", 0, 0);

#endif

    ESPDash.addTemperatureCard("temp3", "18B20 Temperature/C", 0, 0);

    server.begin();

    MDNS.addService("http", "tcp", 80);

    return true;

}

void setup()

{

    Serial.begin(115200);

#ifdef SOFTAP_MODE

    Serial.println("Configuring access point...");

    uint8_t mac[6];

    char buff[128];

    esp_wifi_get_mac(WIFI_IF_AP, mac);

    sprintf(buff, "T-Higrow-%02X:%02X", mac[4], mac[5]);

    WiFi.softAP(buff);

#else

    WiFi.mode(WIFI_STA);

    wifi_config_t current_conf;

    esp_wifi_get_config(WIFI_IF_STA, &current_conf);

    int ssidlen = strlen((char *)(current_conf.sta.ssid));

    int passlen = strlen((char *)(current_conf.sta.password));

    if (ssidlen == 0 || passlen == 0) {

        multi.addAP(WIFI_SSID, WIFI_PASSWD);

        Serial.println("Connect to defalut ssid, you can long press BOOT button enter smart config mode");

        while (multi.run() != WL_CONNECTED) {

            Serial.print('.');

        }

    } else {

        WiFi.begin();

    }

    if (WiFi.waitForConnectResult() != WL_CONNECTED) {

        Serial.printf("WiFi connect fail!,please restart retry,or long press BOOT button enter smart config mode\n");

    }

    if (WiFi.status() == WL_CONNECTED) {

        Serial.print("IP Address: ");

        Serial.println(WiFi.localIP());

    }

#endif

    button.setLongClickHandler(smartConfigStart);

    useButton.setLongClickHandler(sleepHandler);

    Wire.begin(I2C_SDA, I2C_SCL);

    dht12.begin();

    //! Sensor power control pin , use deteced must set high

    pinMode(POWER_CTRL, OUTPUT);

    digitalWrite(POWER_CTRL, 1);

    delay(1000);

    if (!bmp.begin()) {

        Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));

        bme_found = false;

    } else {

        bme_found = true;

    }

    if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {

        Serial.println(F("BH1750 Advanced begin"));

    } else {

        Serial.println(F("Error initialising BH1750"));

    }

}

uint32_t readSalt()

{

    uint8_t samples = 120;

    uint32_t humi = 0;

    uint16_t array[120];

    for (int i = 0; i < samples; i++) {

        array[i] = analogRead(SALT_PIN);

        delay(2);

    }

    std::sort(array, array + samples);

    for (int i = 0; i < samples; i++) {

        if (i == 0 || i == samples - 1)continue;

        humi += array[i];

    }

    humi /= samples - 2;

    return humi;

}

uint16_t readSoil()

{

    uint16_t soil = analogRead(SOIL_PIN);

    return map(soil, 0, 4095, 100, 0);

}

float readBattery()

{

    int vref = 1100;

    uint16_t volt = analogRead(BAT_ADC);

    float battery_voltage = ((float)volt / 4095.0) * 2.0 * 3.3 * (vref);

    return battery_voltage;

}

void loop()

{

    static uint64_t timestamp;

    button.loop();

    useButton.loop();

    if (millis() - timestamp > 1000 ) {

        timestamp = millis();

        // if (WiFi.status() == WL_CONNECTED) {

        if (serverBegin()) {

            float lux = lightMeter.readLightLevel();

            if (bme_found) {

                float bme_temp = bmp.readTemperature();

                float bme_pressure = (bmp.readPressure() / 100.0F);

                float bme_altitude = bmp.readAltitude(1013.25);

                ESPDash.updateTemperatureCard("temp", (int)bme_temp);

                ESPDash.updateNumberCard("press", (int)bme_pressure);

                ESPDash.updateNumberCard("alt", (int)bme_altitude);

            }

            float t12 = dht12.readTemperature();

            // Read temperature as Fahrenheit (isFahrenheit = true)

            float h12 = dht12.readHumidity();

            if (!isnan(t12) && !isnan(h12) ) {

                ESPDash.updateTemperatureCard("temp2", (int)t12);

                ESPDash.updateHumidityCard("hum2", (int)h12);

            }

            ESPDash.updateNumberCard("lux", (int)lux);

            uint16_t soil = readSoil();

            uint32_t salt = readSalt();

            float bat = readBattery();

            ESPDash.updateHumidityCard("soil", (int)soil);

            ESPDash.updateNumberCard("salt", (int)salt);

            ESPDash.updateNumberCard("batt", (int)bat);

#ifdef USE_18B20_TEMP_SENSOR

            //Single data stream upload

            float temp = temp18B20.temp();

            ESPDash.updateTemperatureCard("temp3", (int)temp);

#endif

        }

    }

PICTURES

![](https://lh5.googleusercontent.com/MKbtCcnuIFhD0RaGCpOm-zgR7kKgkbzMd2yXoAivEn6c3eH_aLpcNtOfstgBHsOs5mnR-ber0CgtqUw-0C4m-9yQ47SbeOJ4RZIF59ypDI4Euiimt-f06qVXi1OjVGYoAshcTurwa_iehOR3lBfiTss)

Dashboard of the sensor

 ENCLOSURE![](https://lh5.googleusercontent.com/E9rvT7dWbh8a1C2ECVcmgdTl1GIRD1TLjhOB9OLEj_yvt6i5QFuqtA2lCTNjHhTAvGNuO7wEDX4pv5JqanPCNHBrM-DWoIbvddmqeBbGDb8PWI3SaILveW734nCGHFrsHCje1db41a8pextYg00y_WI)

Link to google slides of the Iot greenhouse:https://docs.google.com/presentation/d/10OrkxtFIgcncYc8CQ0a_6TWJjGlXbYRqDAxIsPNsY0M/edit?usp=drivesdk