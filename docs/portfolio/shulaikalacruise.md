
About Me

My name: La cruise Shulika

i am :23 years old 

student on : Natin i am studing mechanical engineering

![](https://lh3.googleusercontent.com/e6Y4UnlN6Vl95hAKbLtzuoeWR8c4p6xIdJWuse3HWw5WR1LiryNUC3gEl1FT9PdnkFvyjchB-Oog8AGwEF7I9hY4tsB-dytNirdU0hDQVbwKrJlG5NOtpGRxtHcFacTl-v1ZhW-Fufercmz7D_OOJ-Y)

Day1 (what i learn and did )

Day 1(Introduction): Basic blink LED
====================================

We were introduced to Arduino Uno and build a basic blink LED sample

We were to change the LED 

We were to change the speed of the blinking of the LEDs by adjusting the delay in the code. (e.g. from 1000 milliseconds to 2000 milliseconds)

Objective:practice i've learn how to

-   to Arduino uno to build basic blink led 

-   then to Change circuit to blink led connected to pin 12 and of 13

-   Change the speed of the blinking of the leds by adjusting the delay in the code

-   Theory: Build a basic electronic circuit using the arduino uno to blink a led connected to digital pin 13. 

-   Handson: (Circuits) Pictures

-   Component list

-   Arduino uno 

-   Breadboard

-   Leds

-   Jumper

-   Resistor

Hands On

I started out building my circuit using tinkercad circuits.

The components which I used was the arduino uno microcontroller a breadboard which is a solderless circuit  a led which is a light emitting diode a 220 ohm resistor and some jumper wires.

I made a power rail on my breadboard by connecting the 5v(+) from the arduino to my plus rail on my bread board and ground(-) rail by connecting the gnd from the arduino uno.

I then connected my led anode leg to my digital pin 13 in parallel with my resistor and the cathode leg to the ground pin 

-   build in tinkercad, check in tinkercad if the led blink by copy paste the code.

![](https://lh3.googleusercontent.com/1LS6ttzKo_asib_zU7ixFbzYivxulgivfeuGdKcx-gFb7DbXoHCcrEkq3arkxMJ5xhULXHSv4iECoCmkxTto2LEvWFZfgAG9FH82LbUX-B86iBxNACdVe0aWkM_hx3m8hrTt0I2VBKbAbJwgzNjTkbI)

this is the Code Block

I then added my basic blink led sketch to blink the led connected to pin 13

I have 2 functions the void setup and the void loop

The setup runs once and the loop runs over and over again

In my setup I initialize the digital pin 13 as output

In my setup I turn the digital pin 13 high using the digitalWrite function for 1 second then low for 1 second and repeat.

// the setup function runs once when you press reset or power the board

void setup() {

  // initialize digital pin 13 as an output.

  pinMode(13, OUTPUT);

}

// the loop function runs over and over again forever

void loop() {

  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  delay(1000);                       // wait for a second

  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW

  delay(1000);                       // wait for a second

}

The task was to change the led on the circuit to digital pin 12 so in my code I had to change the 13 to 12.

group members and i  had to make our leds blink either faster or slower so I changed the delay from 1000 which is 1second to 250 so it faster arduino.![](https://lh4.googleusercontent.com/GC1km61Ntt1L_345mF5FNAE5-PETjLbR-oxfdWscbRjHWX7sb5nx2D0Tm1dezF4sr3XN-1iF-Ds92mBfksuNyL8tJWs3aXHGMkNNKGLPkCcGU-jITo-VIw9REE5WP9JDYB6QaqMH7sOqdSYNmlyxD80)![](https://lh4.googleusercontent.com/3b372KojojygA5ioIVx_VOpx7-7fjjujq4npiYFU3zkZMk2BzRRr2f1G9Ryp02kuQmWybgYp2CGxR1ChASI--VLoTDPjOAL-hB5BcmYnsDDTN-ZoXc5A6sHO5rq_n6eZUrFmUvwzz3H_0-T2-uReRk0)in lesson 1 i've learn:

-   the horizontal raw connected electrically 

-   the vertical strips use for power and ground connections 

-   middle raw breaks the connection between the two sides of the board

-   Resistor component that resist the flowsof electrical energy

Day 3: Continuation of Spaceship interface
==========================================

                Spaceship Interface (Continuation)
--------------------------------------------------

Objective:
----------

-   We were introduced to Spaceship Interface

-   We were to change the code so other parts could work

-   We were to add extra parts to our circuit

-   We were taught what the codes of spaceship interface indicated

-   to build a Spaceship Interface circuit using the arduino u

Theory:
-------

-   build a Spaceship Interface circuit using the arduino uno\
![](https://lh5.googleusercontent.com/-pt2ncuQM9X0Dyj_0gf3-X96WQYOeNObge27eZtohgTFDmHQSXNkTTg7t3V5BTQixloHg_QSWPZ3NL2Nnz4ECsPYm4gHs-ImbqdLPfGbELYpFD4_FV3yjHP431cDfNIdCxyz-eDqoFgmni5ODrBQ_aI)

Component list
--------------

-   Arduino Uno

-   USB Cable

-   Breadboard (solderless)

-   LEDs (4)

-   Jumpers (10)

-   Resistors (220 and 10K Ohm)

-   Pushbutton

Code Blocked (Code Explained)
-----------------------------

int switchState = 0;

void setup(){

  pinMode(3,OUTPUT);

  pinMode(4,OUTPUT);

  pinMode(5,OUTPUT);

  pinMode(6,OUTPUT);

  pinMode(2,INPUT);

}

void loop(){

  switchState = digitalRead(2);

// uhhhh

  if (switchState == LOW) {

// the button is not pressed

    digitalWrite(3,HIGH); // green LED

    digitalWrite(4,LOW); // yellow LED

    digitalWrite(5,LOW); // red LED

    digitalWrite(6,LOW); // white lED

  }

  else {  // the button is pressed

    digitalWrite(3,LOW);

    digitalWrite(4,LOW);

    digitalWrite(5,HIGH);

    digitalWrite(6,LOW);

    delay(250);  //wait for a quarter second

    // toggle the LEDs

    digitalWrite(4,HIGH);

    digitalWrite(5,LOW);

    digitalWrite(6,HIGH);

    delay(250);  // wait for a quarter second

  }

} // go back to the beginning of the loop

LDR (RGB)

We were introduced to LDR(RGB) and what its function is
-------------------------------------------------------

We were taught how to map the code

Theory
------

 Build an LDR circuit and find out what color the LDR will show when the light intensity is increased or decreased.

![](https://lh3.googleusercontent.com/qxDhhthi-IzrVfG4nQhhhtzwTX4DnB27wv_i0k4_OI-0WxefDHanKrcdhPcZ-qmTdVq50hHWC-4SYC4hO3mHwwMFtvmZwILHrrxJQwvn_ctJLsvipGRbstkVrpLH3dizy9mdTO3Ex-hLsoYMIILri0c)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   we  put wire on the breadboard so it can have power and ground on both sides (just like other projects).

-   then we had to Place the photoresistors on the breadboard so it can  cross the center to divide from one side to the other.

-   Attach one end of each photoresistor to power.

-   base on the other side, a 10 KΩ resistor to ground.

-   then plase  an RGB LED to the breadboard.

-   after you have your Arduino programmed and wired up, the LED will be on off white color.

Once you have your Arduino programmed and wired up, the LED will probably be an off-white color.(Depending on the color of the light near you)

const int greenLEDPin = 9;

const int redLEDPin = 11; 

const int blueLEDPin = 10; 

const int redSensorPin = A0; 

const int greenSensorPin = A1; 

const int blueSensorPin = A2; 

int redValue = 0; 

int greenValue = 0; 

int blueValue = 0; 

int redSensorValue = 0; 

int greenSensorValue = 0; 

int blueSensorValue = 0; 

void setup() { 

 Serial.begin(9600); 

 pinMode(greenLEDPin,OUTPUT);

 pinMode(redLEDPin,OUTPUT);

 pinMode(blueLEDPin,OUTPUT);

}

void loop() {

 redSensorValue = analogRead(redSensorPin);

 delay(1000);

 greenSensorValue = analogRead(greenSensorPin);

 delay(1000);

 blueSensorValue = analogRead(blueSensorPin);

 Serial.print("Raw Sensor Values \t Red: ");

 Serial.print(redSensorValue);

Serial.print("\t Green: ");

Serial.print(greenSensorValue);

Serial.print("\t Blue: ");

Serial.println(blueSensorValue);

redValue = redSensorValue/4;

 greenValue = greenSensorValue/4;

 blueValue = blueSensorValue/4;

 Serial.print("Mapped Sensor Values \t Red: ");

 Serial.print(redValue);

 Serial.print("\t Green: ");

 Serial.print(greenValue);

 Serial.print("\t Blue: ");

 Serial.println(blueValue); 

 analogWrite(redLEDPin, redValue);

 analogWrite(greenLEDPin, greenValue);

 analogWrite(blueLEDPin, blueValue);

}

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   LDR(RGB)

-   Photosensors (3)

-   Resistors (220 and 10K Ohm)

-   Jumpers (16)

-   USB Cable

*What is PWM (Pulse Width Modulation) ?
---------------------------------------

Pulse width modulation (PWM), or pulse-duration modulation (PDM), is a method of reducing the average power delivered by an electrical signal, by effectively chopping it up into discrete parts.

Day 4 Continuation of LDR
=========================

"Love-O-Meter (Temperature sensor)"
-----------------------------------

We were introduced to Temperature Sensor
----------------------------------------

We were then tasked to build a circuit practically and theoretically
--------------------------------------------------------------------

Build a Temperature Sensor circuit and find out what happens

![](https://lh6.googleusercontent.com/azxa9OtMlwtEhkFWqvFa7ZFzgFXULcRcN80ueoTQgU1pdJhHJoWdOyetO11m67JX2RYcnr0scrOZzM6HH_BBUaCCF3E_LjMJrh8XJMZ0LtjPIaU4RZRUT848Q20YWu8oVTgZI9klyJYyLwlpgadlFng)

Components list
---------------

-   Arduino Uno

-   Breadboard (solderless)

-   Temperature sensor (TMP)

-   LEDs (3)

-   Resistors (220 Ohm)

-   Jumpers (8)

-   USB Cable

 const int sensorPin = A0;

  const float baselineTemp = 25.0;

  void setup(){

  Serial.begin(9600); // open a serial port

  for(int pinNumber = 2; pinNumber<5; pinNumber++){

   pinMode(pinNumber,OUTPUT);

   digitalWrite(pinNumber, LOW);

    } 

  }

void loop(){

int sensorVal = analogRead(sensorPin);

Serial.print("Sensor Value:"); 

Serial.print(sensorVal);

float voltage = (sensorVal/1024.0) * 5.0;

Serial.print(", Volts: ");

Serial.print(voltage);

Serial.print(", degrees C: "); 

// convert the voltage to temperature in degrees

float temperature = (voltage - .5) * 100;

Serial.println(temperature);

if(temperature < baselineTemp){

digitalWrite(2, LOW);

digitalWrite(3, LOW);

digitalWrite(4, LOW);

}else if(temperature >= baselineTemp+2 && 

temperature < baselineTemp+4){

digitalWrite(2, HIGH);

digitalWrite(3, LOW);

digitalWrite(4, LOW);

}else if(temperature >= baselineTemp+4 && 

temperature < baselineTemp+6){

digitalWrite(2, HIGH);

digitalWrite(3, HIGH);

digitalWrite(4, LOW);

}else if(temperature >= baselineTemp+6){

digitalWrite(2, HIGH);

digitalWrite(3, HIGH);

digitalWrite(4, HIGH);

}

delay(1000);

}

Potentiometer and Servo Motor
-----------------------------

We were introduced to the Potentiometer and the Servo Motor

We were to understand the coding and how the parts communicate with each other

Build a circuit with a Potentiometer and a Servo motor.

![](https://lh4.googleusercontent.com/CksqzaftjVxyl3dR7CRehW8_4EWlPi-aJSbrgVTNtsoM0_PrmmtB0VbHGbu992ACAQUzR8jyCAO_Ftejy00G0tJcpApYNMP6gSBspzIvJA91v-ZgdyUqYjErrcAdAw-xN8Ln1Zt1z-zBcMPSqjxjTSc)

#include<Servo.h>

Servo myServo;

int const potPin = A0;

int potVal;

int angle;

void setup() {

myServo.attach(9);

Serial.begin(9600);

}

void loop() {

potVal = analogRead(potPin);

Serial.print("potVal: ");

Serial.print(potVal);

angle = map(potVal,0,1023,0,179);

Serial.print(",angle: ");

Serial.println(angle);

myServo.write(angle);

delay(15);

}

                         Map

Analoginput = 2^10 = 0-1023 (10 bits)

Analogoutput = 2^8 = 0-255 (8 bits)

Component list
--------------

-   Arduino Uno

-   Breadboard (solderless)

-   Potentiometer

-   Servo motor

-   Capacitors (2)

-   Jumpers (11)

-   USB Cable

Day 5 Finishing Documentation
=============================

(Finishing LCD circuit homework from day4)

Basic Arduino LCD Text
----------------------

We were introduced to a basic Arduino LCD combination

We were to understand what the LCD was meant for

We were to build a Basic Arduino LCD circuit

![](https://lh5.googleusercontent.com/W878fqxy4cD8f6hju8K_xe-umA1ljk-XQvfsBRfWjZCaqTDmalXNj31pqd_qRP2a2k0sTmLXJ13zXQ2ICGRio76AN-ChvQU6bmVGlWRNnxCzX4qcwlfz7t50CJr-j72q8xxQDo2_o3Vl4-B5ImBDWgQ)

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5, d6, d7) 

void setup() { 

 lcd.begin(16,2); // Initializes the interface to the LCD screen, and specifies the dimensions (width and height) of the display } 

}

void loop() { 

 //lcd.print("Arduino"); // Prints "Arduino" on the LCD 

 lcd.print("Hello_shulaika"); // Prints "Arduino" on the LCD 

 delay(3000); // 3 seconds delay 

 lcd.setCursor(2,1); // Sets the location at which subsequent text written to the LCD will be displayed 

 lcd.print("Watchu_doin shulaika "); 

 delay(3000); 

 lcd.clear(); // Clears the display 

 lcd.blink(); //Displays the blinking LCD cursor 

 delay(4000); 

 lcd.setCursor(7,1); 

 delay(3000); 

 lcd.noBlink(); // Turns off the blinking LCD cursor 

 lcd.cursor(); // Displays an underscore (line) at the position to which the next character will be written 

 delay(4000); 

 lcd.noCursor(); // Hides the LCD cursor 

 lcd.clear(); // Clears the LCD screen 

}

Component list
--------------

-   Arduino Uno

-   LCD

-   Potentiometer

-   Jumpers(12)

-   USB Cable

Final project Automatic fish feeder 

![](https://lh6.googleusercontent.com/t7b2XrWMencHD4_sRd-Q7OQwnQHifX_gQ7e2KK48oZTka4016_FOJU32brpfP_AGETDzy8eDP9ubwWfpD_p39uUTAkw2gva6sUnxaODqgbOUtwvafurcNldvaFmwnGjzGbudbB0uFQ5f7flo4zvZYPk)

Final project: Auto Fishfeeder 
===============================

Theory 

I were introduced to a Arduino LCD combination

I had to build a Arduino LCD circuit of a automatic fishfeeder were "feeding on" was write on the LCD display  en its timering first i had to build in tinkercad and then like this on the foto after this we made it easily to put the LCD in the arduino\
![](https://lh4.googleusercontent.com/7XXGoyMMfD9_7Z01SEOl-bDr7Y6V3FyoN8WTcUiEsmNltSApB3IP25hsmBGR3vRZiUZnU-Ips_hIwinCYD_3PoC9RSPOQ8spkc5a1CswYzTL6kFmqReeET8Sptlh9pjgibqh_gkVKozPdpCM9QfdZxA)

Component list
--------------

-   Arduino Uno

-   LCD shield 

-   servo

-   Jumpers

-   USB Cable The Final of the feeder

        3D printing ![](https://lh6.googleusercontent.com/VQZiTzzK3gwemB0iIaYgDyWNnTBiVSiY2nHFq4Vv0NC0RqnCoCqUeVDA2T4sIYNZE70t7UGuq1Gb7so_i0U6MEgQNhs75Q7n5qjdEGWwxmF22MgfZvewbs9Ht7D3wES3i1h-lrxAFOYJZTApnNycJ0I)![](https://lh6.googleusercontent.com/PdsRRy_yDW77nYmy6vU7CDP7gX4R3VSzH3L86QfJXbsfa9uznZLmuHDwvF21QTRs-GED-An4gX2P1i_sFoBp7MWcMzKK5swEZsE4VG5m4fCXmZr5eOlN6N5wKHlzfcUduYOSSRXafdUEHnmAPpfcexI)![](https://lh3.googleusercontent.com/FDwzKDtZA39HRFIeCpZCJgwzo_XeKCcprObgK8A91gQAtCpu7n01mNPhT57PeXwXgHgBt7U9oyu5IN5JCMLykr8gvHGlFHfvpl5t4RJ0T7lh-hnrvgWvE1XGnpesQZZnm5HdjW3jD4BB1Xt09-1Ddw0)![](https://lh3.googleusercontent.com/nCl5xydOoN6c6zob6OAGV_bTZhimFjY-EhQkK-56rNxd_2Xsojb5NAQoDgWmJ3ULSiQurYfo-PNPviTc2vmTMrXLB2Q6wv-LaoB34o4iXTt-Tx77XBqzffOyAKRm_9GJlVLMs-R9gMrLwt-2HwGDlaI)![](https://lh3.googleusercontent.com/Gfh6_rM_6Q3EekYCtmwYIdjQ3woQssaERqZF_VRpRJxnFnGjj0mW_jkE3ve-e6hPygWtLh-Upp2i592Dwr7KCyy8RMnwKMoSAvBvlv1uWFOl8djQnEuEajvgvWFiqez3RzjdZWZ7qO30xLcgVVHr60c)

 code:![](https://lh6.googleusercontent.com/PeRfgoo80mBaaX7QVYscJ1LP6HdTUsuulEROKx9CZxeWfB_qyXD2s2R0o4BsXs0KZ-A4ku3wlek4i6LevyIpEq_92WHriLR6Hg4DOsPk4WQui_yKHKX365qIpocp3ANwHkq8Tyd2zYPpQV2fLEVkFcY)

// You can have up to 10 menu items in the menuItems[] array below without having to change the base programming at all. Name them however you'd like. Beyond 10 items, you will have to add additional "cases" in the switch/case

// section of the operateMainMenu() function below. You will also have to add additional void functions (i.e. menuItem11, menuItem12, etc.) to the program.

String menuItems[] = {"Run Feeder", "Wait Time", "Feed Time", "Feed Now", "Save Settings", "Hi Mom!"};

// Navigation button variables

int readKey;

int savedDistance = 0;

// Menu control variables

int menuPage = 0;

int maxMenuPages = round(((sizeof(menuItems) / sizeof(String)) / 2) + .5);

int cursorPosition = 0;

int wait_time_seconds = 0; // seconds

int wait_time_minutes = 0; // minutes

int wait_time_hours   = 8; // hours

int feed_time = 5; // seconds

int redraw = 0;

int defaultMenuItem1 = 1;

int pos = 0;

int servoPin = 2;

int pulse = 1500;

// Creates 3 custom characters for the menu display

byte downArrow[8] = {

  0b00100, //   *

  0b00100, //   *

  0b00100, //   *

  0b00100, //   *

  0b00100, //   *

  0b10101, // * * *

  0b01110, //  ***

  0b00100  //   *

};

byte upArrow[8] = {

  0b00100, //   *

  0b01110, //  ***

  0b10101, // * * *

  0b00100, //   *

  0b00100, //   *

  0b00100, //   *

  0b00100, //   *

  0b00100  //   *

};

byte menuCursor[8] = {

  B01000, //  *

  B00100, //   *

  B00010, //  *

B00001, // *

  B00010, //  *

  B00100, //   *

  B01000, //  *

  B00000  //

};

#include <Wire.h>

#include <LiquidCrystal.h>

#include <Servo.h>

#include <EEPROM.h>

// Setting the LCD shields pins

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

Servo myservo;

void setup() {

  // Initializes serial communication

  Serial.begin(9600);

  myservo.attach(2);

  myservo.write(78); // STOP SERVO

  // myservo.writeMicroseconds(1500);

  // Initializes and clears the LCD screen

  lcd.begin(16, 2);

  lcd.clear();

  // Creates the byte for the 3 custom characters

  lcd.createChar(0, menuCursor);

  lcd.createChar(1, upArrow);

  lcd.createChar(2, downArrow);

  // Load EPROM

  int available_saved_value = EEPROMReadInt(1);

  if (available_saved_value == 1252) {

    wait_time_seconds = EEPROMReadInt(3); // seconds

    wait_time_minutes = EEPROMReadInt(5); // minutes

    wait_time_hours   = EEPROMReadInt(7); // hours

feed_time = EEPROMReadInt(9); // seconds

  }

}

void loop() {

  mainMenuDraw();

  drawCursor();

  operateMainMenu();

  // menuItem1();

}

// This function will generate the 2 menu items that can fit on the screen. They will change as you scroll through your menu. Up and down arrows will indicate your current menu position.

void mainMenuDraw() {

  Serial.print(menuPage);

  lcd.clear();

  lcd.setCursor(1, 0);

  lcd.print(menuItems[menuPage]);

  lcd.setCursor(1, 1);

  lcd.print(menuItems[menuPage + 1]);

  if (menuPage == 0) {

lcd.setCursor(15, 1);

    lcd.write(byte(2));

  } else if (menuPage > 0 and menuPage < maxMenuPages) {

lcd.setCursor(15, 1);

    lcd.write(byte(2));

    lcd.setCursor(15, 0);

    lcd.write(byte(1));

  } else if (menuPage == maxMenuPages) {

    lcd.setCursor(15, 0);

    lcd.write(byte(1));

  }

}

// When called, this function will erase the current cursor and redraw it based on the cursorPosition and menuPage variables.

void drawCursor() {

for (int x = 0; x < 2; x++) { // Erases current cursor

    lcd.setCursor(0, x);

    lcd.print(" ");

  }

  // The menu is set up to be progressive (menuPage 0 = Item 1 & Item 2, menuPage 1 = Item 2 & Item 3, menuPage 2 = Item 3 & Item 4), so

  // in order to determine where the cursor should be you need to see if you are at an odd or even menu page and an odd or even cursor position.

  if (menuPage % 2 == 0) {

if (cursorPosition % 2 == 0) {  // If the menu page is even and the cursor position is even that means the cursor should be on line 1

    lcd.setCursor(0, 0);

      lcd.write(byte(0));

}

if (cursorPosition % 2 != 0) {  // If the menu page is even and the cursor position is odd that means the cursor should be on line 2

    lcd.setCursor(0, 1);

      lcd.write(byte(0));

}

  }

  if (menuPage % 2 != 0) {

if (cursorPosition % 2 == 0) {  // If the menu page is odd and the cursor position is even that means the cursor should be on line 2

    lcd.setCursor(0, 1);

      lcd.write(byte(0));

}

if (cursorPosition % 2 != 0) {  // If the menu page is odd and the cursor position is odd that means the cursor should be on line 1

    lcd.setCursor(0, 0);

      lcd.write(byte(0));

}

  }

}

void operateMainMenu() {

  int activeButton = 0;

  if (defaultMenuItem1 == 1) {

menuItem1();

    defaultMenuItem1 = 0;

activeButton = 1;

  }

  while (activeButton == 0) {

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 0: // When button returns as 0 there is no action taken

      break;

    case 1:  // This case will execute if the "forward" button is pressed

      button = 0;

      switch (cursorPosition) { // The case that is selected here is dependent on which menu page you are on and where the cursor is.

        case 0:

            menuItem1();

            break;

        case 1:

            menuItem2();

          break;

        case 2:

            menuItem3();

            break;

        case 3:

            menuItem4();

            break;

        case 4:

            menuItem5();

            break;

        case 5:

            menuItem6();

            break;

      case 6:

            menuItem7();

            break;

        case 7:

            menuItem8();

            break;

        case 8:

            menuItem9();

            break;

        case 9:

            menuItem10();

            break;

      }

        activeButton = 1;

        mainMenuDraw();

        drawCursor();

      break;

    case 2:

      button = 0;

      if (menuPage == 0) {

          cursorPosition = cursorPosition - 1;

          cursorPosition = constrain(cursorPosition, 0, ((sizeof(menuItems) / sizeof(String)) - 1));

      }

      if (menuPage % 2 == 0 and cursorPosition % 2 == 0) {

          menuPage = menuPage - 1;

          menuPage = constrain(menuPage, 0, maxMenuPages);

      }

      if (menuPage % 2 != 0 and cursorPosition % 2 != 0) {

          menuPage = menuPage - 1;

          menuPage = constrain(menuPage, 0, maxMenuPages);

      }

        cursorPosition = cursorPosition - 1;

        cursorPosition = constrain(cursorPosition, 0, ((sizeof(menuItems) / sizeof(String)) - 1));

        mainMenuDraw();

        drawCursor();

        activeButton = 1;

      break;

    case 3:

      button = 0;

      if (menuPage % 2 == 0 and cursorPosition % 2 != 0) {

          menuPage = menuPage + 1;

          menuPage = constrain(menuPage, 0, maxMenuPages);

      }

      if (menuPage % 2 != 0 and cursorPosition % 2 == 0) {

          menuPage = menuPage + 1;

          menuPage = constrain(menuPage, 0, maxMenuPages);

      }

        cursorPosition = cursorPosition + 1;

        cursorPosition = constrain(cursorPosition, 0, ((sizeof(menuItems) / sizeof(String)) - 1));

        mainMenuDraw();

        drawCursor();

        activeButton = 1;

      break;

}

  }

}

// This function is called whenever a button press is evaluated. The LCD shield works by observing a voltage drop across the buttons all hooked up to A0.

int evaluateButton(int x) {

  int result = 0;

  if (x < 50) {

result = 1; // right

  } else if (x < 195) {

result = 2; // up

  } else if (x < 380) {

result = 3; // down

  } else if (x < 790) {

result = 4; // left

  }

  return result;

}

// If there are common usage instructions on more than 1 of your menu items you can call this function from the sub

// menus to make things a little more simplified. If you don't have common instructions or verbage on multiple menus

// I would just delete this void. You must also delete the drawInstructions()function calls from your sub menu functions.

void drawInstructions() {

  lcd.setCursor(0, 1); // Set cursor to the bottom line

  lcd.print("Use ");

  lcd.write(byte(1)); // Up arrow

  lcd.print("/");

  lcd.write(byte(2)); // Down arrow

  lcd.print(" buttons");

}

void menuItem1() { // Function executes when you select the 2nd item from main menu

  int activeButton = 0;

  int redraw = 1;

  int period = 0;

  int counter = 0;

  int time_to_feed_hours   = wait_time_hours;

  int time_to_feed_minutes = wait_time_minutes;

  int time_to_feed_seconds = wait_time_seconds;

  while (activeButton == 0) {

if (redraw == 1) {

        lcd.clear();

        lcd.setCursor(0, 0);

        lcd.print("Feeding In: ");

        lcd.setCursor(0, 1);

      if (time_to_feed_hours < 10) lcd.print("0");

        lcd.print(time_to_feed_hours);

        lcd.print(":");

      if (time_to_feed_minutes < 10) lcd.print("0");

        lcd.print(time_to_feed_minutes);

        lcd.print(":");

      if (time_to_feed_seconds < 10) lcd.print("0");

        lcd.print(time_to_feed_seconds);

      // lcd.setCursor(0, 1);

      // lcd.print(feed_time);

      // lcd.print("/");

        //lcd.print(feed_time);

        //lcd.print(time_to_feed);

        //lcd.print("s");

        lcd.setCursor(15, 1);

      // lcd.print(counter+1);

      if (period == 1) {

          lcd.print(".");

      } else {

          lcd.print(" ");

      }

      redraw = 0;

}

delay(100);

counter = counter + 1;

if (counter > 10) {

    counter = 0;

    if (period == 0) {

      period = 1;

    } else {

      period = 0;

    }

      time_to_feed_seconds--;

    if (time_to_feed_seconds == 0 && time_to_feed_minutes == 0 && time_to_feed_hours == 0) {

      // Feed here.

        feedFish();

        time_to_feed_hours   = wait_time_hours;

        time_to_feed_minutes = wait_time_minutes;

        time_to_feed_seconds = wait_time_seconds;

    }

    if (time_to_feed_seconds < 0) {

        time_to_feed_minutes--;

        time_to_feed_seconds = 59;

    }

    if (time_to_feed_minutes < 0) {

        time_to_feed_hours--;

        time_to_feed_minutes = 59;

    }

    if (time_to_feed_hours < 0) {

      // Feed Fish Here!

        time_to_feed_hours = 0;

    }

    redraw = 1;

}

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

/**

 * WAIT TIME

 */

void menuItem2() { // Sets the wait time.

  int activeButton = 0;

  int redraw = 1;

  int selection = 0;

  while (activeButton == 0) {

if (redraw == 1) {

      lcd.clear();

      lcd.setCursor(0, 0);

      lcd.print("Wait Time: ");

    if (selection == 0) {

        lcd.print(" Hour");

    }

    if (selection == 1) {

        lcd.print(" Mins");

    }

    if (selection == 2) {

        lcd.print(" Secs");

    }

      lcd.setCursor(4, 1);

    if (wait_time_hours < 10) lcd.print("0");

      lcd.print(wait_time_hours);

      lcd.print(":");

    if (wait_time_minutes < 10) lcd.print("0");

      lcd.print(wait_time_minutes);

      lcd.print(":");

    if (wait_time_seconds < 10) lcd.print("0");

      lcd.print(wait_time_seconds);

    redraw = 0;

}

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 1:  // Right button

        selection++;

      if (selection > 2) selection = 0;

      redraw = 1;

      break;

    case 2:  // Up button

      if (selection == 2) wait_time_seconds++;

      if (wait_time_seconds > 60) {

          wait_time_seconds = 0;

          wait_time_minutes++;

      }

      if (selection == 1) wait_time_minutes++;

      if (wait_time_minutes > 60) {

          wait_time_minutes = 0;

          wait_time_hours++;

      }

      if (selection == 0) wait_time_hours++;

      // wait_time_seconds = wait_time_seconds + 100;

      redraw = 1;

      break;

    case 3:  // Down button

      if (selection == 0) wait_time_hours--;

      if (wait_time_hours < 0) wait_time_hours = 0;

      if (selection == 1) wait_time_minutes--;

      if (wait_time_minutes < 0) wait_time_minutes = 0;

      if (selection == 2) wait_time_seconds--;

      if (wait_time_seconds < 0) wait_time_seconds = 0;

      redraw = 1;

      break;

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

/**

 * FEED TIME

 */

void menuItem3() { // Feed Time

  int activeButton = 0;

  int redraw = 1;

  while (activeButton == 0) {

if (redraw == 1) {

      lcd.clear();

      lcd.setCursor(0, 0);

      lcd.print("Feed Time");

      lcd.setCursor(0, 1);

      lcd.print(feed_time);

    redraw = 0;

}

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 2:  // Up button

        feed_time++;

      redraw = 1;

        break;

    case 3:  // Down button

        feed_time--;

      if (feed_time < 0) feed_time = 0;

      redraw = 1;

      break;

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

/**

 * MANUAL SERVO

 */

void menuItem4() { // Function to manually activate the servo.

  int activeButton = 0;

  int redraw = 1;

  lcd.clear();

  lcd.setCursor(0, 0);

  lcd.print("Manual Feeding!");

  feedFish();

  while (activeButton == 0) {

if (redraw == 1) {

      lcd.clear();

      lcd.setCursor(0, 0);

      lcd.print("Manual Feeding!");

    redraw = 0;

}

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 1:  // This case will execute if the "forward" button is pressed

        feedFish();

      redraw = 1;

      //button = 0;

        //activeButton = 1;

      //break;

}

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

/**

 * SAVE SETTINGS

 */

void menuItem5() { // Function executes when you select the 5th item from main menu

  int activeButton = 0;

  EEPROMWriteInt(1, 1252);

  EEPROMWriteInt(3, wait_time_seconds);

  EEPROMWriteInt(5, wait_time_minutes);

  EEPROMWriteInt(7, wait_time_hours);

  EEPROMWriteInt(9, feed_time);

  lcd.clear();

  lcd.setCursor(0, 0);

  lcd.print("Settings Saved!");

  while (activeButton == 0) {

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

    }

button = evaluateButton(readKey);

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

void menuItem6() { // Function executes when you select the 6th item from main menu

  int activeButton = 0;

  lcd.clear();

  lcd.setCursor(0, 0);

  lcd.print("Love You Mom!");

  while (activeButton == 0) {

int button;

readKey = analogRead(0);

    if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

void menuItem7() { // Function executes when you select the 7th item from main menu

  int activeButton = 0;

  lcd.clear();

  lcd.setCursor(3, 0);

  lcd.print("Sub Menu 7");

  while (activeButton == 0) {

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

void menuItem8() { // Function executes when you select the 8th item from main menu

  int activeButton = 0;

  lcd.clear();

  lcd.setCursor(3, 0);

  lcd.print("Sub Menu 8");

  while (activeButton == 0) {

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

void menuItem9() { // Function executes when you select the 9th item from main menu

  int activeButton = 0;

  lcd.clear();

  lcd.setCursor(3, 0);

  lcd.print("Sub Menu 9");

  while (activeButton == 0) {

int button;

readKey = analogRead(0);

if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

void menuItem10() { // Function executes when you select the 10th item from main menu

  int activeButton = 0;

  lcd.clear();

  lcd.setCursor(3, 0);

  lcd.print("Sub Menu 10");

  while (activeButton == 0) {

int button;

readKey = analogRead(0);

    if (readKey < 790) {

      delay(100);

    readKey = analogRead(0);

}

button = evaluateButton(readKey);

switch (button) {

    case 4:  // This case will execute if the "back" button is pressed

      button = 0;

        activeButton = 1;

      break;

}

  }

}

/**

 * Send the command to turn the servo and feed the fish.

 */

void feedFish() {

  lcd.clear();

  lcd.setCursor(0, 0);

  lcd.print("Feeding Now!");

  lcd.setCursor(0, 1);

  lcd.print("For ");

  lcd.print(feed_time);

  lcd.print(" secs!");

  // myservo.write(77); // CW SLOW

  myservo.write(76); // CW LESS SLOW

  delay(feed_time * 1000);

  myservo.write(78); // STOP

}

// Ref: http://forum.arduino.cc/index.php?topic=37470.0

void EEPROMWriteInt(int p_address, int p_value)

{

   byte lowByte = ((p_value >> 0) & 0xFF);

   byte highByte = ((p_value >> 8) & 0xFF);

     EEPROM.write(p_address, lowByte);

     EEPROM.write(p_address + 1, highByte);

}

//This function will read a 2 byte integer from the eeprom at the specified address and address + 1

unsigned int EEPROMReadInt(int p_address)

   {

   byte lowByte = EEPROM.read(p_address);

   byte highByte = EEPROM.read(p_address + 1);

   return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);