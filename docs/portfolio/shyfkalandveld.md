
LANDVELD SHYFKA

![](https://lh4.googleusercontent.com/6mGk5ixHpNYyLCactZ9rTqG1hZBgSO2C4ZiA3zx6pDv-xk-GrJAm2R6ea6LAqiPoH4xxYmZqN0vryCtP2owCIfWmI16zvcU4MJd7X1IW-fCmg9GoSXnpqCJbArnYNB-OiZJcvS5hDJywnUY_f_mq6cA)

My name is Shyfka Landveld and I attend Natin. I am studying ICT, and I am 17 years old. I love coding and want to be a software engineer. i didn't know much about embedded programming so i decided to sign up for this course to gain knowledge about embedded programming.

DAY 1(21-02-2020)

-   Objective:

-   Law of Ohm

-   Function of the components

-   Simulations tinkercad

-   serieschakeling

-   parallel schakeling

-   Theory: I build a serie en parallelschakeling in tinkercad.

-   Handson: (Circuits) Pictures 

serieschakeling

circuit description:The two switches are in series. This means that the same electrical current flows to both of them. for the light to turn on both buttons should be pressed.

![](https://lh5.googleusercontent.com/MSceGEaBAPqDRt33E0_oeLW1tV30ejkjjDQG3ZaVTx6UZPqUG7MkK9-Jm4xGD5QqPAsM1uae-DgJsJl9zCJOOYv6-5-YT2dAgYVrKyG4hoCqL14JMbR3vzHPgT9d7BKJd9wHkgonNlmauqhW8VuLMas)

I started out building my circuits using tinkercad circuits. The components which i used were the arduino uno microcontroller, a breadboard, a led(light emitting diode), 2 push buttons and a resistor of 220 ohm and 2 two jumpers. I made a power rail on my breadboard by connecting 5v from the arduino to plus rail on the breadboard and ground rail by connecting the ground from the arduino uno. I connected:

1.  the resistor on the breadboard in the plus 

2.  the led somewhere on my breadboard

3.  then connected the anode to ground with a jumper

4.  2 push buttons in the middle of the breadboard

5.  then a jumper from the first pushbutton to begin of the second one

6.  another jump from the end of the second pushbutton to the cathode of the led.

parallel schakeling

![](https://lh5.googleusercontent.com/1hmMdEAwjaPya_y5fzBZjy3knBR_kff9HMuoS5udv7KGuWexQeQYmA3z7O8-3E9dHt_ReYBiIMqaL0G3Y5l7fNR1Oo6SvKXMCZwSpsqS_vsY3om0Z6XYctPEyv7AR1yEA1eLShcOnTdJjiOF5ekP8vw)

These two switches are in parallel.this means that the electrical current is split. if either switch is pressed the led will light up. 

1.Keep the switches and LED where they are as in the serie circuit

2.Remove the connection between the two switches.

3.Wire both switches to the resistor.

4.Attach the other end of both switches to the LED,

5.Now when you press either button

6.The circuit is completed and the light turns on.

DAY2. MAANDAG 24 FEBUARI 2020

SPACESHIP INTERFACE

-   Objective:

-   Pushbuttons

-   simulation physical

Theory: I build the spaceship interface

component list:

1.  arduino uno microcontroller

2.  resistor

3.  breadboard

4.  jumpers

5.  leds

![](https://lh5.googleusercontent.com/gL_J7fdRD-p0-iBiJd7Rjc_koT8nwSzrGhhIxOpO0wz5P9OCLVHbjpVks5ipr7QrV993kqbRLC7xuTFhGVmkGHjsibXIZs-zmy8ACDbquAx6fea5dGxql4wCpkEG9ipJ6sZyRu3MtUhD2qozfnx6l4k)

I connected:

1.  5v jumper

2.  ground jumper

3.  a push button

4.  4 leds

5.  then to each led a resistor with the value of 220 ohm to each cathode site of the led

6.  to the cathode side the jumpers

below the link of the simulations in tinkercad

<https://www.tinkercad.com/things/dCbIaHT9yqe-spaceship-interface/editel>

![](https://lh3.googleusercontent.com/VUH8LJgiadygExH07GmpHzfxabxjXPuZiazpk6Nl1StlwyL9MjLZhjVjlmFPfUfywUlOsRmYDLqAJvjfKoj303Rncw3j7naF8Y01kqBDjORQGB4PWjsIYvwm0EXfrXiH09PZDJXBJW_K_46KVir8yfw)

code spaceship interface:

int switchState= 0;

void setup(){

pinMode(3,OUTPUT);

pinMode(4,OUTPUT);

pinMode(5,OUTPUT);

pinMode(2,INPUT);

}

void loop(){

switchState= digitalRead(2);

if(switchState == LOW){

digitalWrite(3,1);

digitalWrite(4,0);

digitalWrite(5,0);

digitalWrite(6,0);

}

else{

digitalWrite(3,0);

digitalWrite(4,1);

digitalWrite(5,0);

digitalWrite(6,0);

delay(1000);

digitalWrite(4,0);

digitalWrite(5,1);

digitalWrite(6,0);

delay(1000);

digitalWrite(4,0);

digitalWrite(5,0);

digitalWrite(6,1);

delay(1000);

}

}

LIGHT DEPENDENT RESISTOR

theory: i built a simulation physically with light dependent resistors

components:

-   arduino uno

-   breadboard

-   jumpers

-   photocells

-   resistors

-   rgbd

I connected:

1.  a jumper from the 5v arduino plug to the plus rail     of the breadboard

2.  the jumper from the gnd plug to the minus rail of the breadboard.

3.  then I connected to lrd's to the breadboard

4.  then the resistor on the plus side inline with the lrd's

5.  and on the anode side i connected the jumpers from each resistor to the digital inputs 9,10,11,12

6.  at the other of the lrd's jumpers to the plus side

7.  the rgb on the breadboard.

8.  on the three ports i plugged

![](https://lh4.googleusercontent.com/FNVxTmprxsNqlNdhnlKZ9ROFtXUnkbGI9G1bfboOtx7TUJHgn9992GxN-GDhai0m0bnRzgPsNWxth_tCtScRqanC56LibP3Oz6mCFH48uO2kGDOGjy6MjWd44V3_cy9cqpqLAzWQb5_qNtp_pNbfHqQ)

![](https://lh4.googleusercontent.com/N3bx5noQCOp8QfPJl8EckvQoTukMr0EDvR3SXJ6NoRuJ-uRsPeBjHQA6Hg8vIiZRTpKcbZ_lqi7dTH6grP_sXwLrVfB_Mg8_Fiq6fcxntq4Y4vcy9KnpDl0JTdqCU51qz_5ZmSK0OtLGs8dNzxsxAYs)

DAY 3. VRIJDAG 28 FEBR 2020

On day 3 we first continue with the light dependent resistor.we were able to find out the specific colors of the rgb led.After that we continue with the temperature resistors.

so the objective for day 3 are:

-   temperature sensor

-   servomotor

components list:

-   arduino uno

-   breadboard

-   jumpers

-   temperature resistor

-   servomotor

-   potentiometer

![](https://lh4.googleusercontent.com/sTQIpHhQlq6axEm7pvFZjQZq4ViaY6P7mvn1V2yY_Yo-3_qdzEHPb5YLEQeGYIxEoak8e4P8h1QTmft6zDzYR9TdzftoDggNaUOt8fwcG2u0XYhfVuyn5OkPjJBWJYesDhrdxnU-bjOxdXxdCKwmXUs)

<https://www.tinkercad.com/things/lSuuUhOOfNt-temperature-sensor/editel>

code:

const int sensorPin = A0;

const float baselineTemp = 25.0;

void setup(){

Serial.begin(9600); // open a serial port

 for(int pinNumber = 2; pinNumber<5; pinNumber++){

pinMode(pinNumber,OUTPUT);

digitalWrite(pinNumber, LOW);

 } 

}

void loop(){

int sensorVal = analogRead(sensorPin);

Serial.print("Sensor Value: "); 

Serial.print(sensorVal);

float voltage = (sensorVal/1024.0) * 5.0;

Serial.print(", Volts: ");

Serial.print(voltage);

Serial.print(", degrees C: "); 

 // convert the voltage to temperature in degrees

 float temperature = (voltage - .5) * 100;

Serial.println(temperature);

if(temperature < baselineTemp){

digitalWrite(2, LOW);

digitalWrite(3, LOW);

digitalWrite(4, LOW);

}else if(temperature >= baselineTemp+2 && 

 temperature < baselineTemp+4){

digitalWrite(2, HIGH);

digitalWrite(3, LOW);

digitalWrite(4, LOW);

 }else if(temperature >= baselineTemp+4 && 

 temperature < baselineTemp+6){

digitalWrite(2, HIGH);

digitalWrite(3, HIGH);

digitalWrite(4, LOW);

 }else if(temperature >= baselineTemp+6){

digitalWrite(2, HIGH);

digitalWrite(3, HIGH);

digitalWrite(4, HIGH);

 }

delay(1);

}

MAPPING.(WANNEER JE MET VERSCHILLENDE SCHALEN WERKT DIE MET ELKAAR MOETEN COMMUNICEREN VB 8 BITS EN 10 BITS)

![](https://lh6.googleusercontent.com/NElN4osqpPRcrn3sSNWPxmddkuBM3PTWQbR16rHPW03R5hnrebtBJ2CtmmPSLSq75MfJrjFp_tEpzItkFiSBP5WflUUVWTEFpcD9pg-ibklvPzu_WUtb8nvzvYAgFGcOloHT5ZxasbeGTv-4tt-9wgo)

<https://www.tinkercad.com/things/eYMGDVDSvSw-sero-pot/editel>

DAY4. We worked on documentation, we went through some codes and held presentations.

home work

![](https://lh3.googleusercontent.com/7y7hhBn7HQBVfkloy1yJm_ruMgGJNnyZI1LbcLnAeK6LncJSzohBvolkHyk4qqVbYmN49V2zV5H_yK-iMdKM4pGtOLkm_DlRubjHKufhYLh-df4Pw306Kq2t9j6Os7TX9iZKK0YLvPotpgM3PBzns0k)

<https://www.tinkercad.com/things/5GNjNe5XyJN-copy-of-final-project/editel?tenant=circuits>

code:

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5, d6, d7)

void setup()

{ Serial.begin(9600);

  lcd.begin(16,2);

}

void loop()

{

 lcd.print("Hello_Shyfka"); // Prints "Arduino" on the LCD 

 delay(3000); // 3 seconds delay 

 lcd.setCursor(2,1); // Sets the location at which subsequent text written to the LCD will be displayed 

 lcd.print("LCD Tutorial"); 

 delay(3000); 

 lcd.clear(); // Clears the display 

 lcd.blink(); //Displays the blinking LCD cursor 

 delay(4000); 

 lcd.setCursor(7,1); 

 delay(3000); 

 lcd.noBlink(); // Turns off the blinking LCD cursor 

 lcd.cursor(); // Displays an underscore (line) at the position to which the next character will be written 

 delay(4000); 

 lcd.noCursor(); // Hides the LCD cursor 

 lcd.clear(); // Clears the LCD screen

}

always document the things you do

day 5

objectives:

-   tinkercad 3d design

-   h bridge

tinkercad 3d design:

i was trying to make a case for my arduino

![](https://lh4.googleusercontent.com/aXvbkcJnYDT_J_dQ4PeyL16Zr4LwTfzQErPoWMx-axZo8-IPR1hk448vs7gZVPoAqDj3tQFHIPRkraCmGFctV5bgtfCPrH_jE4bdMdFlh0VCMjrgPNVY7jRf9IdXPUUOgWVzesH41lAerkvqJD7aizw)

![](https://lh5.googleusercontent.com/Wg0AKNCrpGeIBvZoGCqaMovmc4Mqc6FAjF3_sEHW7gHDJUe10q0V32pQxfjXyTJODg6C7kLxJgSa10p4_N8XIWEY1jA8ald8Gp3o2X7rKOYN4_OolWt5qwzhm3T5txycxpd6-xxNkQX9ue_UjI0P3rk)

![](https://lh4.googleusercontent.com/_EH5SeYHeJWJiDWDJQzHIOS3a7ZJepZScqgXTFf-cjGx7T9KckAfcK9YCZ1dsqOheQW-r_NRqOGWD5WOhIIjbirCDUhIO0pANBUoa5016O7IUeUFPmuAQikvP5rb-0liZqies98wAqbi1s4Dw06t9Ak)

h bridge

componentlist:

-   arduino uno

-   breadboard

-   h bridge

-   powersupply

-   dc motor

![](https://lh5.googleusercontent.com/rbrkTCCWgHHuPHvw94dQd6yP8yfLsUderYcL5LfdG4eH3EbxVW6obDxoHPa0tJtdEQD2c4UdNZYW3SaChByejvxkiyZ4mn5xgHM-5_LVDumfS2AEEWBvacqH-PksrY1AG-pXziVTl-JhbXVR_EbfbpI)

Check the link below to start the simulation <https://www.tinkercad.com/things/fPi59TSaVUB-h-bridge/editel>

function: in this simulation the dc motor will be rotating clockwise for 3 seconds and stop, then it will rotate counter clockwise. below the code:

![](https://lh6.googleusercontent.com/FwLhB9EQX2a2yfWrwK1EOBbixWGi22jkH049VgiN0uMH34apazOuAtVKXCXHIpJ1JgrevKzdpDZriNWSqbGE7o4NYfrBxPEWV83eCPPsGmqXCZIBVgOETkpuTX3JRhOg8a1Ut1exy7-XPDoZBIXKL54)

H bridge with potentiometer:

![](https://lh5.googleusercontent.com/7xhVV1PxKy_OvlsofKyV-zGYY2xbshRbNQdyxnwkoBpPxki6j4CnIsKZnYN02aOSJEZG7udFtWJNELa4H6ouojqEayovtpbv-SJdVQMj3dOKT_AepETS4h7msPQ6XrdC3kflouZxJo2l1MrGwwGYhac)

the difference between h bridge with potentiometer and h bridge without potentiometer is in function. the direction of the dc motor depend van de potentiometer

code

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

   Serial.begin(9600);

}

void loop() {

  int duty = map(analogRead(A0),0,1023,-255,255);

  Serial.println(duty);

  analogWrite(pwm,abs(duty)); // abs is voor break

    if (duty>0){

     digitalWrite(in_1,HIGH) ;

     digitalWrite(in_2,LOW) ;

     analogWrite(pwm,250) ;

    }

    if(duty<0){

    digitalWrite(in_1,LOW) ;

    digitalWrite(in_2,HIGH) ;

     ;

    }

    if(duty==0){

    digitalWrite(in_1,HIGH) ;

    digitalWrite(in_2,HIGH) ;

    delay(1000) ;

    }

  }

ABOUT THE FINAL PROJECT

Myr final project is a temperature meter. This meter will have to following functionalities

1.  it will measure the temperature of someone

2.  the temperature will now be displayed on a lcd screen

3.  depending on the output you a led will light up

4.  if your temperature is equal to or below 36 C the green led will light up

5.  if your temperature is above 37C the red led will light up

My  inspiration for this project is the corona pandemic. by doing this project we hope it will make the way of measuring the temperature easier.

click the link below for simulation

<https://www.tinkercad.com/things/iyP7BhnSRL2-copy-of-temperature-sensor/editel?tenant=circuits>

Parts required:

-   arduino uno

-   breadboard

-   jumpers

-   resistors

-   potentiometer

-   lcd screen

-   pushbutton

connections

-   First I connect 5v from the arduino to the plus rail of the breadboard with a red jumper. 

-   secondly ground to minus rail of the breadboard with a black wire. 

-   then i connect the lcd screen to the arduino

I2C Board of LCD Arduino

GND <---> GND

VCC <---> 5V

SDA <---> A4

SCL <---> A5

-   I connect the potentiometer. first pin to ground, middle pin to the lcd screen( to monitor the contrast) and the last pin to 5v

-   then the push buttons somewhere on the breadboard. 

-   a jumper from the pushbutton the arduino

-   a resistor from the pushbutton to ground on the breadboard

-   then i connect this led

-   ![](https://lh5.googleusercontent.com/JLSifmjt0FC9pVDFE58gY68Qi4MFE7C_oAFBaSY8p2uHmiZEndmOxqQvbIbYlaeN_uoZyVqh1CiHH2hNgc1lrILtTq_6PwfrefeFwKuEjsPkR3j4rfCf1mRae_b0zbN3O0QmkiSZ37la3fNXekyZRjg)

-   for each light i did the following. i connect a resistor from the led to the ground on the breadboard and a jumper to the arduino

![](https://lh6.googleusercontent.com/0bOvWsGveLdzIHGHQXIXQCtTsXDpBnxNYbIOoA46K5C6MTpRFIp639NnywamPqnMNrc8efxAvWbx1veW6uZ60_N8gd7OMizjA0pluPmDg7T3LKmGZPwzwec-VwHHWjnX5kY6_6W_wZTMaWPlwyEuVME)

after i did these connections i start coding.

below the code

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

int SensorTempPino=A0;

int AlertaTempBaixa=8;

int AlertaTempAlta=13;

int TempBaixa=0;

int TempAlta=30;

void setup() {

  Serial.begin(9600);

    pinMode(AlertaTempBaixa, OUTPUT);

   pinMode(AlertaTempAlta, OUTPUT);

  lcd.init();

  lcd.backlight();

  lcd.print("Temperatura:");

  lcd.setCursor(0,1);

  lcd.print("      C        F");

}

void loop() {

   int SensorTempTensao=analogRead(SensorTempPino);

  float Tensao=SensorTempTensao*5;

  Tensao/=1024;

  float TemperaturaC=(Tensao-0.5)*100;

  float TemperaturaF=(TemperaturaC*9/5)+32;

  lcd.setCursor(0,1);

  lcd.print(TemperaturaC);

  Serial.println("C");

  Serial.println(TemperaturaC);

  delay (1000);

 lcd.setCursor(9,1);

     lcd.print(TemperaturaF);

  Serial.println("F");

  Serial.println(TemperaturaF);

  delay (1000);

    if (TemperaturaC>=TempAlta) {

      digitalWrite(AlertaTempBaixa, LOW);

      digitalWrite(AlertaTempAlta, HIGH);

    }

    else if (TemperaturaC<=TempBaixa){

      digitalWrite(AlertaTempBaixa, HIGH);

      digitalWrite(AlertaTempAlta, LOW);

    }

    else {

      digitalWrite(AlertaTempBaixa, LOW);

      digitalWrite(AlertaTempAlta, LOW);

    }

    delay(1000);

}

click the link to start the simulation in tinkercad.

<https://www.tinkercad.com/things/iyP7BhnSRL2-final-project/editel>

link to enclosure:<https://www.tinkercad.com/things/2tYHcpw2kBr-final-project-enclosure/edit>

pictures of the enclosure

![](https://lh4.googleusercontent.com/uGeCwoMxKMmzy5Ue4aM0Qtdib-fNjqLiVBGVGnlehnbC2Fmr1A98VDYEcUJAqB1fvNx45WsBxSbefD0tdy1J5mrmCx_FnHkSZa1gG9w_i6rxguUHOyxTHWP2Qm1BPtJL7cy7cYtessQsgBaeCqzF4NM)![](https://lh3.googleusercontent.com/nzOuakhNTYskKZoI6diMRyVXa5JwvrQHI-q9v3PmPpKiZ0V3SDNj9xkKhX22K74QaVkES7WYak6rmAmJNGudKOdvWiAx7OxLVRDeXdY41GsM7R2ooH9C_VPRAtfgEBIOAqXAHQmW76gnstTLQ2Rmv7A)![](https://lh3.googleusercontent.com/u4De7kldG42Sdb1mdqWpQUBBl8cLVfs1C_jRl4ODjGP4C_DkOGPclRe6LnnwhyFnjJx5wUWZd6ja6EYVqPH6yCpks5YBWn9sP1_UMTU3qk_eWm0kCbT1W7v9tLtd2_o716hpCTUnXVnXU2UO9Ed3dFY)

pictures of the final project

![](https://lh6.googleusercontent.com/XBualqHQhHPUHzLoxw3C2bOiCWQemVfBJuUpCnW8Lpep45UGCFnSM8jqXi9hWMxL2XrWHMQxyfBcdkZJz_iytnXu3ju8QuhmJnRwk6TX3ndbM0nE7DLo5Es9gBLinUSbjeUFpQrNsDOD9JvzDjT3xlk)![](https://lh4.googleusercontent.com/WJZEdx18cqrEAlUxRBb28wt0ETeoFK-Csx-HNYs6c8wOTIcnTMZqXTO9sbca3kmqaoJs_jxBwckcYLMMZ6Akxe2XQMkK-I1kLL7FpG8fGahXNbYSqaTxEwO6W6HmkXDeU3HHdsPQ6LgiKgr4t7Mw3RI)![](https://lh5.googleusercontent.com/i63TEq0GoU0sxFYcWRhh2kf7MV8pUk2iDluAtGzIA4Hs_5m9uZmVpchdfUUFysUUR9Dot-Cnqt1fgjCdANC3TzfIO501yboUEZS_mRrcxpy1f3KSdyNW8vqsKgc6tJqx780BaJMHPanq3Mk5n3B7Ea4)![](https://lh3.googleusercontent.com/R1X-TvNkdx64U9iBwU4G9drjujLC_pk0sYp4vkpzseXvahJ4pDX-NPi95AuurV3g_rZPzpft8cH6ZZs1FRUrvGRZ0whHFIH_aHRZJ_B9QxmQ268qN0at0PCBbuH0sZ3zN2tZjnZCwo3q__8iwruN17M)![](https://lh5.googleusercontent.com/qE7dSjKPJvhPhPxNin0KCwUYE1WElJb8thO9c1re4hELypxPoUQZf9AKXb10LYDjgHIaPeJTCGkDrOTd9MsrPh_zqywayVu3Yf7D2CACy8Xxz1-e6ScGvUXs3Z3OCMFlVUZo7Dot78UyK9baEDjHhNU)![](https://lh3.googleusercontent.com/G7rB-Jp6xtK-ZccTmirqF_afaHFkN9Xf72hCV7yUuugJDREpoLytU_wvGKW4VVaAEl1zRxw6LuQAPTAuj4l2Jx74CmEYpncJ2xoaZOx_4GmXBK-b93wmaONgjai4RWfGJEq6GvAgWwDaSwBDxUD1glQ)![](https://lh4.googleusercontent.com/BpkBH5xRi7tlNkuuDRIL99OIviyaok7YYB9lBGXMulcEe1KaQ_sS1q2BNcL5x5-827FN6sV-lxxj6ZQYG2LPXhjx9JyGvRZ7DYV90dRhp-N6BqBQLf8rgiKhD4gTZXCpqk4z8OsVHDQHhSF6PmgCLM0)![](https://lh4.googleusercontent.com/j8fYEDfdtYTbR5x68eoo20lO1CmrIwUNobBOXzyUKTV6hBFUeJTMUslF6TkMY5IXlY6RHVuHYgAJbYv8nMaDtmVFt4BTTryww9Cj347dQKAhqQkuqR9PC9CS5pzBecWW4snkYLAYoirDcMef1AHWzoI)

video final project:https://drive.google.com/file/d/1z3jcu0aNZOZCKsjRBmHZ799EvY3GqPWE/view?usp=sharing

<https://drive.google.com/file/d/1z3jcu0aNZOZCKsjRBmHZ799EvY3GqPWE/view?usp=sharing>

link to google slides of the poster:<https://docs.google.com/presentation/d/1_avbEQn-zTTygj0LWG7u-FSP2siA9FCBHhUZQO7qT_g/edit?usp=sharing>

![](https://lh3.googleusercontent.com/8ne735TAXdu_gx2hRv6kHxWl8ijPM-uEwIsXixtcfb-PN9FLr0IWP4Ek2S9z5_fsZTL8Slq8ow9PRCFs6DXNg0jPPo0Ea0Gsyp5VL6FwMPTJ6pr1r1uQKWGlg7w7Iev20jeQ23FKFW-lOinNAWH8ECU)![](https://lh4.googleusercontent.com/Vu4b2q7SE5mRHV5En1e5V7PTA7-ZW3i9wDU25asSaVeLEsK9-DS8izlUGzPCDMoCicmrqMpcf4oZdyZzueIrEe8yx0MfS8M4ssN-0bS1qKFT2mCOBqNzVpNMgoJd5Pu5REvhD_zTQ4vine_QVXQbAEg)![](https://lh6.googleusercontent.com/XxJUhIkMyPC_Kn92l8lEJK8QwjBwC21vibVz_H4lOygYE8i2YAA2kP-yQI2C_x6c94VUgosNeUfceCSRCq8myTKbHzhcLK9eug98XVDNiAhMuNrQF6ejv4GTfrcSoh8qG3cegsQWG8W82e8dS4L9aJk)![](https://lh5.googleusercontent.com/mqT9-hTEDwN50Isi1r-_B2Z64aa_udOwe_2oL0-67YTgoVqQl_JzJcoCnBtUJYRn_JfuuUyILNh_5bjAXdwvJrBCllZntnzXTdZqa2RBmtfApWcsXjcdQFY4J_uG1wAHgxUnxmvSiwnbKB65WVQiVns)

I have learned a lot from this course. I could barely build a simple circuit on my own. But now I can build circuits, even projects. I don't only know how to build but I also know what I am doing. i know most of the sensors and where to use them. one thing that is important while working is documentation. if it's not documented it's not done. take pictures,videos and make notes.