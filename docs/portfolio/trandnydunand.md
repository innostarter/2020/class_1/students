

Naam: Trandny dunand

Age: 22

I AM a student at Unasat,I do software engineering.

I participated in this training because I want to expand my knowledge.

![](https://lh4.googleusercontent.com/ekLjIdIURqwREeuaCv853TZc2j4qyIWOLvbL0IDH-4BePrx1Aq-lpVlTtvLI86LPOR5ADde5aPCXqTAlNAQfVVXF1_ffrsoDQH7M-e3hkiX-672x_gmIY3FjGbaC49WnpQpjAuubgn7yIJdIkwXvSB8)

Introduction day:
-----------------

We were introduced to Arduino UNO\
Arduino is an open-source microcontroller board based on easy to use hardware and software. Arduino boards can build circuits and interfaces for interaction and you can also tell the Arduino board how to interface with other components.

We had to build a basic electronic circuit using the Arduino UNO to blink led connected to digital pin 13. After that, we changed the circuit to blink led pin 12. At last, we had to change the speed of the blinking LEDs by adjusting the delay in the code from 1000 (which is 1 second) to 250.

The  Arduino

![](https://lh6.googleusercontent.com/Yhtw9lPeRIKMZpOaLwVbL3m2OuEaVKjtF2zeq3_5qfA9_YHcnVahlm-QFC35NkKVbi-XRf7ZM6gxgiLoYKl1NRypKHWIpDY8Ay5X1mnDmso-_e4eDK-Ce9FD-2VruVDTPOxnuAM5Hz3Apm9sSVfik5s)

Breadboard: This is a board on which you can build electronic circuits. It has rows of holes that allow you to connect  wires and components

Breadboard pictures

![](https://lh5.googleusercontent.com/dLNUkoXDYHvp_Gqol0cNy77qViqtjJXe9pJPHY_RlZDD5paDuQsxsotv47Hy5A0frUpd1coFM1DrE2nVlAd9CX9inpm19tj2rqzjg9doIp6SCKkeBSdMeHuuC2Y0W7931oslmjqkoWYsDUKomoiOMlY)

Light Emitting Diodes(LEDs): Converts electrical energy into light energy. Electricity only flows in one direction through these components. The anode positive side (longer leg) where the current enters the diode and the cathode negative side (shorter leg) where the current leaves the diode. When voltage is applied to the anode of the LED and the cathode is connected to the ground, the LEDs emit light.

Pictures of LEDs:

![](https://lh6.googleusercontent.com/9o-jozFd9LmLu_wqBA1Z3qg-i0cPxAJdUkHh6-CY7DhHKpz_ARI8QpET68AMnGrJ78bJyBtPdgIjQaFDFf0XLW4WQMccxEIYjOh1hEhmGRqoNGFI4_cUyzJLk9A8oNKz3bojKaZGmRTFImNXT-4iLVE)

Jumpers wires: These are used to connect components on the breadboard and to the Arduino.

Jumper wires pictures

![](https://lh5.googleusercontent.com/ao3-6vUtUgT6TtB9Ma2nW0FHnQ7vJUeLhXoBykQtJ7T29S0UEQuxvmNwlytnZ-ReRXiebjFk_6fDNGXLTH0qZ1UTLsxM_mWKXBOak8pPboJ_T4hOU-PVXrCrX7Zn1EJpLhDAW1s0m5Fs5kvL_D57dDQ)

Resistor: These resist the flow of electrical energy in a circuit. As a result the voltage and current change. Resistor values are measured in ohms. The colored stripes on the sides of the resistor indicate their value.

Resistor pictures:

![](https://lh6.googleusercontent.com/_ZcG2Z_QQSL0wJXFCKCO6dlWB6XWbm9CjKsqAn1aRiD3rSweIi9djYIKZEOzkzuZBkP5U1xFwC4dQMf-pOV5OigwEb1GTp2YszHvEJmMEe-sm8okthl1obkMZrqIVrQLaK3X5qvAA2j_KRb50fuSNFk)

USB Cable:\
![](https://lh5.googleusercontent.com/ISHbIC_jYOP-dGkKqTtxfnIsAa5NizY4PmMzoD3IzZENqY6zhiEoiy-b3s9Is_jL2VebZwIYpMW-oboAydjLbL047sw-smLi2aar2UeJGFcd1qNAbaRtx_Xh-4q-9Rb8BpFEseXhUfiaFIVCg4qpobY)

Use it to connect Arduino Uno or any board with the USB female A port of your computer.

Component list:\
Arduino UNO

Breadboard

 LEDs

Jumpers(3)

Resistor

USB Cable

screenshot

![](https://lh4.googleusercontent.com/CmwBbB2Qe4eCCLTPBvr-i7tkEX9ycaHO5b4Iu49k_oYA_G0cx7s-7QMm_6mGi0fz3NKH8WUgj_-cn0ZOoSuxoBYfi71apMuMsEMNd0XEWCC65Van-RifNqsy8ik_RBHXlSFodk-lZREO5_xwzRyJTp0)

### Picture:

### ![](https://lh5.googleusercontent.com/9WrlwFw3-qV2aqVHmB_UajHNrzKbizh50Vsknl_mhpgAi7U2-fodvB6yeHoPKoiwEVW24AdXZUIsLI2x4nRmd3kxhYc62TASRfJtYCGVDPbDMLsj5W3AHSCC6S38p0Mz3cA71za8HAfbDmChazjkL5o)

### ![](https://lh5.googleusercontent.com/BhMceT03wmpXfi1Q8QhzsAauwBxlrASsSjix7dvcoLhwzdglNtupn4Fz34KxAbuQNdTGv4T-G8qBg-kys6ETMwNFGB9zrBGjPREw8NVJNpNyGuBk5K6PorWrQf7kvdyxUWBNLF1W5fTcNNcQrq9Q2xU)

### Code:

blink default (pin 13) 

void setup()

{

  pinMode(13, OUTPUT);

}

void loop()

{

  digitalWrite(13, HIGH);

  delay(1000); // Wait for 1000 millisecond(s)

  digitalWrite(13, LOW);

  delay(1000); // Wait for 1000 millisecond(s)

}

blink pin 12

void setup()

{

  pinMode(12, OUTPUT);

}

void loop()

{

  digitalWrite(12, HIGH);

  delay(1000); // Wait for 1000 millisecond(s) that's 1 second

  digitalWrite(12, LOW);

  delay(1000); // Wait for 1000 millisecond(s) that's one second} 

Day1
====

We builded a series and parallel circuit in Tinkercad. we experimented with voltmeter.

The Law of ohm![](https://lh3.googleusercontent.com/zAksfMFSOpxSfPpQhNcvo9LLc6mGptwidaXsS1INdJ7SjFStwGeMtrijf7BhWFSqXWImb9aPBBecciGIx8TW_agO0rOwdObVW_H0XX2A4dLA6KjAV9Dkk5QLUhBtE3l3qYl9UuYTssM3H15tXcyupLU)\
The Law of ohm is a formula used to calculate the relationship between voltage, current, and resistance in an electrical circuit.\
Current, voltage and resistance are all related. If one of these is changed in a circuit, it affects the others.\
V= Voltage\
I= Current

R= Resistance

V = I * R

I = V / R

R = V / I

Series circuit

![](https://lh6.googleusercontent.com/D5FqeCuy39i9NgjO2MId19_5hJg_MAzDbUJ7BBObHaxEjZxcnsCsa4tppnoyJJVfUm5sBaYX51KBpSV5zl7e8Op1vaMNXHPsggPEfG_1cQ-36Yd4UUh7N4ECDhcfG3ky1NshhG08o1BLrOFFL2f3gIs)\
A series circuit is a single loop, with no branching paths. All resistors or other components are arranged in a line. In a series circuit, the total resistance is equal to the sum of all resistances.\
A series circuit has more than one resistor but only one path through which the electricity flow. From one end of the battery, the electricity moves along one Path with no branches, through the resistor to the other end of the battery.

Parallel circuit ![](https://lh6.googleusercontent.com/xojc66yeiZY88PbMzpUeMDkVvI42j-4CG-iYA9Y5ibLQ8EaVgaoGb4q98163V8N0UZkEt_JhQey-nn3mUg_Tm6SfgUw7wXFuHpAhZdm2Ou48aZg-33snrGi2L_PB-tyw7qCMMoOOTn9mHYLqtIJc1Ls)\
A parallel circuit branches into multiple paths. Voltage is the same across each component of the parallel circuit.\
The total current is the sum of the currents flowing through each component. In a parallel circuit, each bulb is wired to the battery in a separate loop.

We had to build a series and parallel circuit and determine with a multimeter, what happens to the current. We exercised with a few circuits to understand how the current flowed between Series and Parallel. Then we learned how to enumerate the current with The Ohm's Law. At last, we also learned how to use a voltmeter and a multimeter in Tinkercad.

Component list for both parallel and series circuit:\
Arduino Uno

Breadboard(solderless)

Jumpers(5 jumpers for series circuit and 6 jumpers for parallel  circuit)

Resistor (220 Ohm)

LED

Pushbuttons (2)

screenshot series circuit

![](https://lh4.googleusercontent.com/lM9nXLoVCM5dbLn9fsS1fjSwt5Xfx43OS7XrCFpiEkt3JaQg2k2u3Y0a-E1nh57aD5Umq-NZ1a5QDTgwYSmeZLx0MiD7D0f8DLP3PvHEsf63Ns7vNjAJd7Zo6HIo85kiKTAQM2Q1GpwkyD9oiVmzpHg)

Screenshot parallel circuit

![](https://lh3.googleusercontent.com/skFgHue6cEX7QaRWNvMt7JjH90swj0RpSl9sqxiIsEDnOI9Qc7-aae8fYrDz0666QJ-bmMs8CaTCj-6webcAQWVQ2Y2nRnh9JjdMdUdnAIZpYAYlg77hy-6qR9h3QKpD9eqSIVFl0Nu2anJ1IH-n_u4)

There was no code use for series or parallel circuits.

Day2
====

This day we continued (learned) building a spaceship interface circuit. We learned to code the spaceship interface and also played around with the code. We had to build a Spaceship Interface circuit
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Also in this lesson, we learned to deal with a basic Arduino program to read the state of a button switch and initiate a lightning sequence. Here the lights turn on and off OR blink repeatedly because I pressed on the pushbutton.

We built a spaceship interface with 3 LEDs then we had to put one more LED and play around with the code.

Pushbutton: A Type of switch that interrupts the flow of electricity, breaking the circuit when it's open. The close when pressure is applied. connects 2 points in a circuit.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Push Button picture:

![](https://lh3.googleusercontent.com/Nn1QBOGXRPx89RH_8EZniBMgY7wnGPCrmxfAN5E0YcXqmrSyAJKgxsDKn-9E11-SmnZSmBnmFGfeXG7zP5j-v6J6uriQ82Qgntq0TnUJX9cll_lsN2Iz5Ky2CV_LDfat_ESqhXQZ-aJMHMhmyyy_qEI)

First circuit spaceship interface  screenshot:

--------------------------------------------------

![](https://lh6.googleusercontent.com/yertfQTldkGxHEYGcwssAv85_Tg10QdI5rZjGohWeQj8WcZ6Vrfwh2w7fS2k_cugyczJn8qXpg2nZsuwvSWhWv7yneEGuuQ-3j1K_C0J2sVZLMAfbyDULN18I3iHTk59iLkn0POd6bws7kF41TlV6Sc)

Pictures:

![](https://lh6.googleusercontent.com/sbmk8TdNolxEmcFzdBO34uxPUdGaCYilJgmjYwo0hNThUYQ7JElbel73AZoARcBhDmX-2362RCiOJm2F3UETS97i1-GdMDLSXaaohiEququ9khaDQTVcYS6yNxLrtLzsa8z1bwsA71fpSpZ7D0vcuyw)

![](https://lh5.googleusercontent.com/XzSlB-BkaiOq4-KA8kK-ZBMY5gwVmCh9gUUbJtay6l1j1oNidGYOVdfbiGcX4tddIa7p64Up1Q5qkdL2kOgamDvHn-8zdDF22izcJsEL7031TnIWTRFNoebzKLdtrQeoLNOGMSwbejUlZWDQb_imdvE)

Component list:\
Arduino Uno\
Breadboard(solderless)\
Jumpers(8)\
Resistor (220 Ohm(3 for the first example and 4 for the second)) (10 kilo-ohms (1))\
LED(3 for the first example and 4 for the second)\
Pushbuttons (1)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USB Cable

First spaceship interface circuit Code:
---------------------------------------

int switchstate = 0;

void setup() {

  // declare the LED pins as outputs

  pinMode(3, OUTPUT);

  pinMode(4, OUTPUT);

  pinMode(5, OUTPUT);

  // declare the switch pin as an input

  pinMode(2, INPUT);

}

void loop() {

  // read the value of the switch

  // digitalRead() checks to see if there is voltage on the pin or not

  switchstate = digitalRead(2);

  // if the button is not pressed turn on the green LED and off the red LEDs

  if (switchstate == LOW) {

    digitalWrite(3, HIGH); // turn the green LED on pin 3 on

    digitalWrite(4, LOW);  // turn the red LED on pin 4 off

    digitalWrite(5, LOW);  // turn the red LED on pin 5 off

  }

  // this else is part of the above if() statement.

  // if the switch is not LOW (the button is pressed) turn off the green LED and

  // blink alternatively the red LEDs

  else {

    digitalWrite(3, LOW);  // turn the green LED on pin 3 off

    digitalWrite(4, LOW);  // turn the red LED on pin 4 off

    digitalWrite(5, HIGH); // turn the red LED on pin 5 on

    // wait for a quarter second before changing the light

    delay(250);

    digitalWrite(4, HIGH); // turn the red LED on pin 4 on

    digitalWrite(5, LOW);  // turn the red LED on pin 5 off

    // wait for a quarter second before changing the light

    delay(250);

  }

}

Second spaceship interface circuit screenshot:
----------------------------------------------

![](https://lh3.googleusercontent.com/KzQ-PQCUR7n7aOu1XAGX8WhLZfFY2vRMI2W12wQNw-jsyETcr4KgrKwkyYN8BAXxRBcBXvXHVrWzo6XpU5DXwCHdRCerHkBDRLl8wUNm18PWkCuAeRIgSErvN0IG6SG613f7boWUSqNmgCgiXzT0roc)

Pictures:
---------

![](https://lh6.googleusercontent.com/h2NWVVhnnTDdJxilQ-GMpaUXOv8vsf5aN1pjccMpyyMXhevzlSRu_ma77zaygwfDKcEj7pDB8p7N-fRd__msT_Ooegv73BifpQfHJJjxps6nUcuL-2DF4DcjDiA2kFURai6kt4vtpxz1m-pvL2j0A5g)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

second spaceship interface circuit code:
----------------------------------------

int switchState = 0;

void setup()

{

  pinMode(3,OUTPUT);

  pinMode(4,OUTPUT);

  pinMode(5,OUTPUT);

  pinMode(2,INPUT);

}

void loop()

{

switchState = digitalRead(2);

if (switchState == LOW){

  //THE BUTTON IS NOT PRESSED

digitalWrite(3, HIGH);

digitalWrite(4, LOW);

digitalWrite(5, LOW);

digitalWrite(6, LOW);  

}

else{

  // THE BUTTON IS PRESSED

digitalWrite(3, LOW);

digitalWrite(4, LOW);

digitalWrite(5, HIGH);

digitalWrite(6, LOW);  

  delay(250);

  // wait for a quarter second

  // toggle LEDs

digitalWrite(4, HIGH);

digitalWrite(5, LOW);

digitalWrite(6, LOW); 

  delay(250);

// wait for a quater second 

digitalWrite(4, LOW);

digitalWrite(5, LOW);

digitalWrite(6, HIGH);  

   delay(250);

// wait for a quater second  

}

} //go back to the beginning of the loop

The mistake I made:\
The port wasn't selected on Arduino UNO. The leds were also burned in the process.
-------------------------------------------------------------------------------------------------------

Day 3
=====

We learned what an LDR is and what its function is, we were also taught how to map the code. We had to build an LDR circuit and find out what color the LDR will show when the light intensity is increased or decreased.

LDR\
An LDR stands for a light dependent resistor or photoresistor or photocell is a light-controlled variable resistor that changes its resistance based on the amount of light falling on its face. This makes them jump into the conductive band and thereby conduct.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

![](https://lh6.googleusercontent.com/9eoxVxSF3rqa986wqHIkTpfAbVvTywDAJKxoAvOhNT0Y35ZqrB_0nFsBJciAM3mFWFh36NDR3TYnGjNIDv255nxWhgCXZ8SWhRkhdQHMKoj_R_fq9cbYjnGC6D3_XEirCt4NYuFRP9eA2yV3kliEN4c)

RGD LED: meaning  red blue and green LED

![](https://lh4.googleusercontent.com/B8lw94GpZpekseOBMkotn-1qF4TX86g8Mx-ydmkN2vNTXeatAycoCj3tl4xV3jmppMu6Slu0tPTff1afvWkllp7zjevpAsUCf-B7CT5lTDjNMVUmAXx_pXT0ao6Q0XGOCO0nfBSLsIkulrtT43ih1WU)

screenshot
----------

![](https://lh6.googleusercontent.com/rGqGOjZTShKKGEtYhf2cPihlEhURQejCj_0ra5NGWMfC4uhIcIGycwYaVj-gKFfqqXtMRyeios0Z64SVF3bD9nDsLt7Z-TVyhW5C2zS19AtdY4yRAvslEkVrfbBwnN_LwFw0-FZtSC6x7BI4SwebL3Y)

CArduino Uno

Breadboard (solderless)

RGB LED

Photoresistor (3)

Resistors (220(3)) 10K Ohm(3))

Jumpers (16)

USB Cable

### code

const int greenLEDPin = 9;

const int redLEDPin = 11; 

const int blueLEDPin = 10; 

const int redSensorPin = A0; 

const int greenSensorPin = A1; 

const int blueSensorPin = A2; 

int redValue = 0; 

int greenValue = 0; 

int blueValue = 0; 

int redSensorValue = 0; 

int greenSensorValue = 0; 

int blueSensorValue = 0; 

void setup() { 

 Serial.begin(9600); 

 pinMode(greenLEDPin,OUTPUT);

 pinMode(redLEDPin,OUTPUT);

 pinMode(blueLEDPin,OUTPUT);

}

void loop() {

 redSensorValue = analogRead(redSensorPin);

 delay(1000);

 greenSensorValue = analogRead(greenSensorPin);

 delay(1000);

 blueSensorValue = analogRead(blueSensorPin);

 Serial.print("Raw Sensor Values \t Red: ");

 Serial.print(redSensorValue);

Serial.print("\t Green: ");

Serial.print(greenSensorValue);

Serial.print("\t Blue: ");

Serial.println(blueSensorValue);

redValue = redSensorValue/4;

 greenValue = greenSensorValue/4;

 blueValue = blueSensorValue/4;

 Serial.print("Mapped Sensor Values \t Red: ");

 Serial.print(redValue);

 Serial.print("\t Green: ");

 Serial.print(greenValue);

 Serial.print("\t Blue: ");

 Serial.println(blueValue); 

 analogWrite(redLEDPin, redValue);

 analogWrite(greenLEDPin, greenValue);

 analogWrite(blueLEDPin, blueValue);

}

Temperature sensor
------------------

.![](https://lh3.googleusercontent.com/N-FxpfqBvqzeTmTSi0DYGXirees7XxyvvySkLrGib5KwF_WIUgUl-bCGPbMCObmVLiCwpvNyC2QYj6FkzZsiPC_xer7CvtOZ5xvPuGNRaTlTDGeDvPH9wDfRZ6UBGtWTvStDJQi78ce2VCrVKTaN3aQ)

This sensor changes its voltage output depending on the temperature of the component and uses the property of diodes. As a diode changes temperature the voltage changes with it at a known rate. The outside legs connect to power and ground. the voltage on the center pin changes as it gets warmer or cooler.

The mistake I made:\
I misplaced the jumpers that's why I accidentally burned it.

The temperature sensor was explained to us. We were assigned to build a temperature sensor circuit practically and theoretically and to find out what happens after building it.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Component list:\
Arduino Uno\
Breadboard (solderless)\
Temperature sensor\
LEDs (3)\
Resistors (220 Ohm)\
Jumpers (8)\
USB Cable
--------------------------------------------------------------------------------------------------------------------------------

screenshot

![](https://lh6.googleusercontent.com/oLiC-QTjm0e-usmxRwUmqYvYhb4MLF2Hz64szXOArzxRDGrSaMJLsIwT6ytCalGx7eTWVXtMNOic2Q8HmQkF3dElt7ZPPumXw3lUzMEovq7281QAFvuzpzz2H6jFPqhB5-SKsOvxFrnOb5mib36y-QI)

![](https://lh6.googleusercontent.com/NNR1Ed1KCo9zvNG4vmE2a-Kx1EIMNCEW8J5hsPVxSpvGbTAdW4yZ29RGm0ifPWyUTAcXoCfKk2v05Pq0mIrwbVQ9cRKHa_Azfu007rD0uBTMTqxJ4Na7z4viNSlgMEfrkaoLjIckFzzsrCqJaYLUyF8)

Pictures:

![](https://lh3.googleusercontent.com/hWqGYmDqHB-VkuGYu7lRQau9hULKiqrwXhkysKgXNhoS2ue2H39YWZ_55PkBSUtUnWpGGT1Cn8RD3IZR1h-kvRwc1Z5tD467j7n0UkNodjP5M77gyts37IiH-RsaWMuTy29ew37gDF9-0nZJ6svp_sc)

![](https://lh6.googleusercontent.com/rfdjrDJRne1TuIwOAU93MTyNejQB4zUi9DXoA-i77z2XgkLrsWv1aQ-e9-sfhv7-gKNt5wZC-C3xlOGi6LGBwONbccNKHBesYlypsBSA_iPcKxhXJH8Mn-sceWQdsYgnKAuFBFTW8TVX5GwhNLq8w9E)

code

// named constant for the pin the sensor is connected to

const int sensorPin = A0;

// room temperature in Celsius

const float baselineTemp = 20.0;

void setup() {

  // open a serial connection to display values

  Serial.begin(9600);

  // set the LED pins as outputs

  // the for() loop saves some extra coding

  for (int pinNumber = 2; pinNumber < 5; pinNumber++) {

    pinMode(pinNumber, OUTPUT);

    digitalWrite(pinNumber, LOW);

  }

}

void loop() {

  // read the value on AnalogIn pin 0 and store it in a variable

  int sensorVal = analogRead(sensorPin);

  // send the 10-bit sensor value out the serial port

  Serial.print("sensor Value: ");

  Serial.print(sensorVal);

  // convert the ADC reading to voltage

  float voltage = (sensorVal / 1024.0) * 5.0;

  // Send the voltage level out the Serial port

  Serial.print(", Volts: ");

  Serial.print(voltage);

  // convert the voltage to temperature in degrees C

  // the sensor changes 10 mV per degree

  // the datasheet says there's a 500 mV offset

  // ((voltage - 500 mV) times 100)

  Serial.print(", degrees C: ");

  float temperature = (voltage - .5) * 100;

  Serial.println(temperature);

  // if the current temperature is lower than the baseline turn off all LEDs

  if (temperature < baselineTemp + 2) {

    digitalWrite(2, LOW);

    digitalWrite(3, LOW);

    digitalWrite(4, LOW);

  } // if the temperature rises 2-4 degrees, turn an LED on

  else if (temperature >= baselineTemp + 2 && temperature < baselineTemp + 4) {

    digitalWrite(2, HIGH);

    digitalWrite(3, LOW);

    digitalWrite(4, LOW);

  } // if the temperature rises 4-6 degrees, turn a second LED on

  else if (temperature >= baselineTemp + 4 && temperature < baselineTemp + 6) {

    digitalWrite(2, HIGH);

    digitalWrite(3, HIGH);

    digitalWrite(4, LOW);

  } // if the temperature rises more than 6 degrees, turn all LEDs on

  else if (temperature >= baselineTemp + 6) {

    digitalWrite(2, HIGH);

    digitalWrite(3, HIGH);

    digitalWrite(4, HIGH);

  }

  delay(1);

}

servo motor
-----------

![](https://lh3.googleusercontent.com/1n7SCArjl9eCjZD-0u1d23YndtN4T4zZWaqNyYVGosVfg755o99VC1vDq6nvudOUdU6nc4bfyABxYerIcOuCWWJw9l3cjRkjaSIv6dGM_Ig_RUSbX37R3xSyVGyHP6G93ALDGDxz3xPPNTsP7ZIapOI)

![](https://lh3.googleusercontent.com/yrfS2xh30UWmnvUG4DN0L2QKM9eK1jPuZflkhys6LUGfzzLR7D3G1SWM9FtERpTmcTnJm9DYxq_xotmtpQaoFgT4_NnzANDRDssdbTbAk0txoFooLuOFaC8kw0CXDVL-dmSd5eHB-XXtygbKiFfBPzE)

A type of gear motor that can only rotate 180 degrees. It is controlled by sending electrical pulses from your Arduino these pulses tell the motor what position it should move to.

The servo  going to the direction of the potentiometer

we had to build a circuit with a Potentiometer and a Servo motor circuit using the kit

Potentiometer

![](https://lh3.googleusercontent.com/wvMK4HEgpw5k7B5PxV5GZ-vP6r7Ma2d1iKdYCBUr5BaRxLwz0zzjHhqTZwDyDKnd6PmQu_hu8y9aYCUmIuJu2qlWp7qIauCois81-5rwB999PlUTEf1jaMXVJtDNwEhZ02kwTUaOCvPec01-iP6OrME)

A variable resistor with three pins. Two of the pins are connected to the ends of a fixed resistor(They are always connected to volt and ground). The middle pin, or wiper, moves across the resistor, dividing it into two halves.it gives the difference in voltage as you turn the knob. The knob is often referred to as a pot.

Capacitors

![](https://lh6.googleusercontent.com/PudCHAmHPA8CSh7Ow1mLnuy9om1StklV0VeUP3d9EFPD9lORvOfxQrnW_C7p1kGijPdvpcH1SIy7-Dmg0v8TK314zfmuF_FcaV_JceiBIODAw31kM2M5HbLH_dI5StmNZak2I6mPYKpi6vZlS0jQ0Ls)

These components store and release energy in a circuit. When the circuit voltage is higher than what is stored in the capacitor, it allows current to flow in, giving the capacitor a charge but when the circuit voltage is lower, the stored charge is released. It's often placed across power and ground close to a sensor or motor to help smooth fluctuations in voltage.

Arduino Uno

Breadboard (solderless)

Potentiometer

Servomotor

Capacitors (2)

Jumpers (11)

Screenshot:

![](https://lh3.googleusercontent.com/TzNYtpxK_Qxwk5ptaHDHe6E9rI-9zRMP1w3QlhMXniz8hK0vz8o8p1VB6AisixQ_tfKNm2UN8cJIDqYgLyO-W8KVpMDpzQ9xZnnJqbQA4szImmEUKsnKOHXQWRMIWyUyH83hjGz-3Mfp1vWahbdCqpc)

Code:

#include<Servo.h>//directive tells the preprocessor to insert the contents of another file into the source code

Servo myServo;

int const potPin = A0;

int potVal;

int angle;

void setup() {

  myServo.attach(9);

  Serial.begin(9600);

}

void loop() {

  potVal= analogRead(potPin);

  Serial.print("potVal:");

  Serial.print(potVal);

  angle= map(potVal,0,1023,0,179);

  Serial.print(",angle:");

  Serial.println(angle);

  myServo.write(angle);

  delay(15);

}

Short code 

myServo.write(map(analogRead(A0),0,1023,0,179));

Day 4
=====

We had to work on our documentation and present it.

I learned to understand the code that I copy and paste from Jullie. I had to explain what the code did but I didn't know, so Theo explained it to me.

Day 5
=====

Tinkercad 3d design
-------------------

 ![](https://lh4.googleusercontent.com/qWbKlSgq6ZZlaMMfLonmb-zF5EJllBZeeJzmE_sRK_TjvI4Myb0xd3EfIwXjFB7NYEAg_5wa5fHIaupV226HGSwHRaUF7TThUNWsbnA9kPB80ffQKv10_REXRYSKYeZNb9qZZL-_ZKHqtRkID9Uhuks)![](https://lh6.googleusercontent.com/yGMwEtiXgdHMEqyL2x52SoqyzmmzT4mKFrxcvBXCBiiM9HulRBLjq48oMZsNFuI1xNAiYjVxoqqGDQxSOU1Y139eVuEk-WFiJOTOouMYzyfYU_gM3CDnJg4M2YD2tsXpc_Ch7sEzAsiuF-GYx__NJtY)

We learned 3d design and what PWM is.

 PWM meaning Pulse Width Modulation  is a technique for getting analog results with digital means. Pulse width modulation is a great method of controlling the amount of power delivered to a load without dissipating any wasted power. Pulse Width Modulation and it is a technique used in controlling the brightness of LED, speed control of DC motor, controlling a servo motor or where you have to get analog output with digital means.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

H bridge\
It is an electronic circuit that switches the polarity of a voltage applied to a load.\
H-bridge is a circuit that lets you control a DC motor to go backward or forward.\
H-bridge is used to reverse the polarity of the direction of the motor, but can also be used to brake the motor, where the motor comes to a sudden stop as the motor terminals are to let the motor-free run to a stop as the motor is effectively disconnected from the circuit.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DC motor

![](https://lh5.googleusercontent.com/JiOr0uyEM7Of9YbtxN6CwRTDlis4whFL8anucjLzbEupRFx8U7f-UEeiIkVYsHES4IgKW1e7n0cbpqFRfy8GDSamF7xwptPop95XoEfjD7uwkGoKvEbWR9QCkaQfrg4cXJH7u4h-QCMQFjGddGCfnn0)

turns electrical energy into mechanical energy when electricity is applied to its leads. the wire inside the motor becomes magnetized when the current flows through them. These magnetic fields attract and turn down magnets, causing the shaft to spin. if the direction of the electricity is changed, the motor will spin the opposite direction.

we were tasked to build an H-bridge with and without potentiometer in tinkercad and  reality

screenshot H-bridge without potentiometer
-----------------------------------------

![](https://lh4.googleusercontent.com/vPlzfKd5lqz3kx0sHrrbhy3_yz_PqSLBRHQl3YrdV66E4OugMDPMNKuba2wzXOhu5vTkvfFtvjgfY6r9WQUrxO8VxAD1NRAaFzsNaOVOLGqdgDbjqo0LdFecamfz5JC1PQtvnT-RYb4ZXSpeSc37THg)

Component list H-bridge without potentiometer:
----------------------------------------------

Arduino Uno

H-bridge L293D

DC Motor 

Power supply

Jumpers (14)

Breadboard

Code H-bridge without potentiometer\
const int pwm = 3 ; //initializing pin 3 as pwm
------------------------------------------------------------------------------------

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

pinMode(pwm,OUTPUT) ; //(pwm=pulse-width modulation)we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

}

void loop() {

   //For Clockwise motion , in_1 = High , in_2 = Low

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

   analogWrite(pwm,250) ;

   /* setting pwm of the motor to 255 we can change the speed of rotation

   by changing pwm input but we are only using arduino so we are using highest

   value to driver the motor */

   //Clockwise for 3 secs

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

   //For Anti Clockwise motion - IN_1 = LOW , IN_2 = HIGH

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

  delay(3000) ;

   //For brake

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

  delay(1000) ;

}

screenshot for h-bridge with the potentiometer
----------------------------------------------

![](https://lh3.googleusercontent.com/c1678cZo-qZ_u7vjQAmvN1Ovi9kXd_bCCD7DlRUtZziFW9fjzhgp4_r9_PM9itwnvWrW8YFnWJhBgu2P0lYG03PMmBmlQI1kSYnv1PIEgZSb00CClvWiiTR2w_MrUlJhXIBWqqgtJ8h4y1RgmIU-1dY)

Component list H-bridge with potentiometer:
-------------------------------------------

Arduino Uno

H-bridge L293D

DC Motor 

Power supply

Jumpers (17)

Breadboard

Code for H-bridge with potentiometer
------------------------------------

const int pwm = 3 ; //initializing pin 3 as pwm

const int in_1 = 8 ;

const int in_2 = 9 ;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {

pinMode(pwm,OUTPUT) ; //(pwm=pulse-width modulation)we have to set PWM pin as output

   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output

   pinMode(in_2,OUTPUT) ;

   Serial.begin(9600);

}

void loop() {

  int duty= map(analogRead(A0),0,1023,-255,255);

  Serial.println(duty);

  analogWrite(pwm,abs(duty));// abs break

  if (duty<0){

//turn Clockwise

  digitalWrite(in_1,LOW) ;

  digitalWrite(in_2,HIGH) ;

}

if (duty<0){

  // turn counterclockwise

   digitalWrite(in_1,HIGH) ;

   digitalWrite(in_2,LOW) ;

}

if (duty== 0){

  // break

  digitalWrite(in_1,HIGH) ;

  digitalWrite(in_2,HIGH) ;

}

}

LCD (Liquid Crystal Display): a type of alphanumeric or graphic display based on liquid.crystals.
-------------------------------------------------------------------------------------------------

![](https://lh3.googleusercontent.com/r-74srkMY8gjfC7QZEs-J9g9HAoeGwULDBtikj2NWdVCzQ6z9OyaedGgt_oLjbZkTZhbrtZGXrThZQrlH2eX_sNpqCq5HQL6WwzZJF3HcjPaXbYKJIzaIyc_GLLUlHv-uxyG1O_IRWftRHXaeNRmeiI)

Component list for tinkercad:\
Arduino Uno\
LCD\
Jumpers (12)\
potentiometer

Screenshot:

![](https://lh3.googleusercontent.com/7XAyRbsAVknHh7iFQ03EodsiiR7mOaHFWpROEeqnGkvbMEQtWyQfukRrLFWr0lbSHzlKjWezKCHcUlU9cyoPH2Evgx9vtgKq1THBRNT3_Xgqbo2w2kLFYEd5pnrfoMOqfcPBXg4dArXa5JRYj3AYnX8)

Code for tinkercad:

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // Creates an LC object. Parameters: (rs, enable, d4, d5, d6, d7)

void setup() {

 lcd.begin(16,2); // Initializes the interface to the LCD screen, and specifies the dimensions (width and height) of the display }

}

void loop() { 

 //lcd.print("Arduino"); // Prints "Arduino" on the LCD 

 lcd.print("Hello_world"); // Prints "Arduino" on the LCD 

 delay(3000); // 3 seconds delay 

 lcd.setCursor(2,1); // Sets the location at which subsequent text written to the LCD will be displayed 

 lcd.print("LCD Tutorial"); 

 delay(3000); 

 lcd.clear(); // Clears the display 

 lcd.blink(); //Displays the blinking LCD cursor 

 delay(4000); 

 lcd.setCursor(7,1); 

 delay(3000); 

 lcd.noBlink(); // Turns off the blinking LCD cursor 

 lcd.cursor(); // Displays an underscore (line) at the position to which the next character will be written 

 delay(4000); 

 lcd.noCursor(); // Hides the LCD cursor 

 lcd.clear(); // Clears the LCD screen 

}

Pictures:

![](https://lh4.googleusercontent.com/p4elaw386m2OrvbW-XLa0DZmgrDFGLa4xsxA_w-vNLOylJXToUvygqZyt2omNfLXsITvqCnlHY8VpwoxEO5w87tq_GXpTbf8ThuJ7sU66r1t1pFwYzZANCSpwUYd_CCwax2ErmGrAnum7ae0ysiwLzA)

Component list for real life:\
![](https://lh5.googleusercontent.com/pEIvaUx9AwAZNhawYOWDhURYpuCwEXSPe-7sgQOaj-vhDYcj75ZP5gJRf_NSn7LZEPPOhfpCeEvzGjn0NGjBesnwWxkESshMVLv7QcOvZCBeWUKOnn1Srhj9KtSKPBDTKBrxNOwNbbumKcadsWPsq0s)

I2C Board of LCD Arduino

GND <---> GND

VCC <---> 5V

SDA <---> A4

SCL <---> A5

Code for real life:

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

void setup() {

  lcd.init();

  lcd.backlight();

  lcd.print("Hello World!");

}

void loop() {

  lcd.scrollDisplayLeft();

  delay(500);

}

Final project:
==============

My final project is a hand wash timer.  

I choose a hand wash timer because the spread of Coronavirus now requires many people to wash their hands at all times. Most people don't know that they need to wash their hands with soap for 20 seconds so that all the bacteria can die. Not many people want to count aloud. Some people wash their hands hastily.\
Since not many people feel like counting to 20, the hand wash timer is there to do that for them.\
For the people who wash their hands hastily, the hand wash timer is there to force or remind them to take the time to wash their hands.

Ultrasonic sensor:

![](https://lh4.googleusercontent.com/gSy_1y0D3eqdRHQbt3DlpG7xub6sAUVMKfq1X-YZxFfaEmtfxt1YvJF0Oi6W_AyTS4oDcMz6IOknfK7xj6Q7HaGWQhYn-Du0htLT1iHXMNtszWobGnwr7AJLlInzhbiG2H9ZnhMYkvozvw7O-2fP-V8)

It determines the distance of an object and it works by sending out a burst of ultrasound and listening for the echo when it bounces off of an object. It pings the obstacles with ultrasound.

Component list:\
Ultrasonic sensor\
LEDs (6(red(1), blue(4), green(1)))\
Breadboard\
USB Cable\
Jumpers(12)\
Enclosure
----------------------------------------------------------------------------------------------------------------------

Screenshot:

![](https://lh5.googleusercontent.com/TH1pPGI33r7XvyDiLuHcdTHSsSbJ1d3VnO9QwlXaV0AV0qhrwYRwf5ICqi8G334YPszNgXRh-jwToayIoLuEI5xAf-rzOn0AL1JqQTF-FJZIGE9S6CLBYNF9kpdyyk16ufaoFXL2uxtk6W0UW3W2ucc)l;![](https://lh4.googleusercontent.com/QfyM8YlAbgObyu197kBMjxtkTdBiXeS1k62VaM_JRp0S6aHwThYUkGAEGvLLEnawz-N5AAVXUFP87VMtN9jKm7sd9JEUTnktzm8sTTnvZH_LnLeYnyj5RmDE-kN30Xod5bqoed_OI__iFHETY3gqE3s)

Pictures:
---------

![](https://lh3.googleusercontent.com/XQPciq3T6adMq1lgFOHvdHh63FbwLlzezgyhr9qM5RnTiW0-fp8H7Lhp4uDel5lXSNa_4ooULgAhrcUKwnCLaVWnrx1-PvwRkDQPnSXonqm9eEold00FcZ-JBj8q04Bpp6YGpU--8BCGBo5BMnFWeEM)

Code:
-----

int echoPin = 11;

int trigPin = 12;

long duration;

int distance;

void setup() {

  for (int i = 2; i <= 7; i++) {

    pinMode (i, OUTPUT);

  }

  pinMode (trigPin, OUTPUT);

  pinMode (echoPin, INPUT);

}

void loop() {

  // This part of the code tells the sensor to measure distance and reads the measured data

  digitalWrite(trigPin, LOW);

  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);

  delayMicroseconds(10);

  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  distance = (duration * .0343) / 2;

  // This "If Statement" detects if there is any object within 30 centimeters

  if (distance > 0 && distance <= 30) {

    ledtimer();

  }

}

void ledtimer() {

  digitalWrite (7, HIGH);

  delay(4000);

  for (int i = 6; i >= 2; i--) {

    digitalWrite (i, HIGH);

    if (i == 2) {

      digitalWrite (7, LOW);

    }

    delay(4000);

  }

  for (int i = 7; i >= 2; i--) {

    digitalWrite (i, LOW);

  }

}

Mistakes I made:

I placed the jumper wrong and I was treating the cathode as the anode and the anode as the cathode.

![](https://lh6.googleusercontent.com/jYlDp0WHwV2zJMcabH61yy6HtOmGOzlgPAnSLIFr8aa73CLEeIHAF1t6rbrsd4UVgSvYfANsNj8u2Qzqp5FdjOsqBwAo_hYiC4-7H2pwzZqe94tmQfDi_DgGj3uMqF6FFe5lKH9dyuyFBhZ6utoDf5U)![](https://lh3.googleusercontent.com/mL5TU9WPy_nU7m3tXmdeIkuxDf_ORK-ponIM89gkUtcozFLpgRTJ0tSV_TsVe-3gLtugfvKGvc8Wydm0NaJ6M6vr7f-YBRq05-5tNnhAQc0f8mqGCcdQptk_hEGUeQNR1ws7P9e0gAQyZsnG4XU8Vc4)![](https://lh6.googleusercontent.com/U-tQ9UzsRQvVyDSQgcEdZZdvZYvZpoaRB45bIlxjN0DGNkbspyEP-g7LQ853wHLg17ZfG_4jLcNvGoDbJvzEASWFH3ZR6cmQLWvw5wW3ux-20hCSKxXQDC7-vRX_lihMuNvgLtZRkIg80t6WBjGUuDk)

Real Final project:
===================

Because the first one was too easy I had to choose another final project, the auto bin.

A prototype that opens trash can automatically when someone is in front of the trash can and also closes automatically when that person moves away. It also notifies you when the trash can is full.

Product description

-Problem:

1.  Touching a garbage can isn't hygienic and with corona taking lives, people don't want to touch surfaces

2.  Many people forget to clean the trash causing the trash can to stink and worms to develop in it.

-Solution:

1.  With an Auto Bin, you will open and close the lid of the bin automatically, without needing to physically touch the bin.

When the distance sensor detects your hands from a distance, it will send a signal to the servo which will open the lid automatically. This makes the Auto Bin very hygienic and convenient in use because of the hands-free operation.

1.  The auto bin will also contain an end stopper which will send a signal to the LCD to notify you that the trash can is full and to clean it up.

Components:

-Arduino

-The LCD i2c

-Servo

-Ultra sonic distance sensor

-Jumper

-9v battery

-Solderless breadboard

-enclosure

-Trash can

Components explanation:

-Arduino:

Is the brain of my project

-The LCD i2c:

A liquid crystal display

-Servo:

Will let the shaft spin 180˚

-Ultra sonic distance sensor:

Determines the distance of an object

Code:

#include <LiquidCrystal_I2C.h>

#include <Servo.h> //includes the Servo library

#include <Wire.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);   // set the LCD address to 0x27 for a 16 chars and 2 line display

Servo servo;

int switchState = 0;

int const trigPin = 3;

int const echoPin = 4;

int const endstopPin = 7;

void setup()

{

Serial.begin(9600);

lcd.init();

lcd.backlight();

pinMode(trigPin, OUTPUT); 

pinMode(echoPin, INPUT);     

servo.attach(9);

// Set the button pin as an input.

 pinMode(endstopPin, INPUT);

}

void loop()

{

switchState = digitalRead(2);

int duration, distance;

digitalWrite(trigPin, HIGH); 

delay(1);

digitalWrite(trigPin, LOW);// Measure the pulse input in echo pin

duration = pulseIn(echoPin, HIGH);// Distance is half the duration devided by 29.1 (from datasheet)

distance = (duration/2) / 29.1;// if distance less than 0.5 meter and more than 0 (0 or less means over range) 

lcd.setCursor(0,0);

lcd.print(distance);

lcd.print("cm");

if (distance <= 15 && distance >= 0) 

{  

servo.write(50);    delay(3000);

} 

else 

{   

servo.write(60);

}

// Waiting 60 ms won't hurt any one

delay(60);

// END STOPPER SWITCH

if (switchState == LOW){

  //THE BUTTON IS NOT PRESSED

  lcd.setCursor(0,1);

lcd.print("trash empty");

}

else{

  // THE BUTTON IS PRESSED

 lcd.setCursor(0,1);

lcd.print(" trash full ");

  delay(250);

}

}

Code explanation:\
-In the header of my code I include libraries that are connected to some components

-Then I have my set up where my code only runs once. In the set up I initialize my serial, LCD, servo, and the trig- and echo pin from the sensor.

-Atlast I have my loop where my code runs continually. The loop is used to control the Arduino board.

Screenshots:

tinkercad Circuit schreenshot:

![](https://lh4.googleusercontent.com/3aK0wTcW5PWHJf2DkcS2hJHw_4mVve8AGHAmoMdHP5XqeVzNQNHhY9vCriX0gONaLUGI8bHP2Gkd8ZZ4e7b5FlkzEc9rWLJCJKqhx39uvvNJmYKnjUgCzDr3dT7_i3rO0v7p3Z18MwCL2mH_WfKMCzY)

Tinkercad enclosure screenshot:

![](https://lh3.googleusercontent.com/0bNRhHrg4U7UXeCnGZsRU2aCmcU99pKIA6imd2q4s2qwti6dWYGJ-BV_LxGNv7YlV61k9VrhmK01s43ybewTanoCf-TPUljIiYCL9CTCBPc9-HBayulKCl1adkUVNmHU8bcizpOHFzrWpHwXxtrIUIQ)

Pictures:

Circuit:

![](https://lh4.googleusercontent.com/GlHeqQKK13qKL3c3cgRiNoGL9BA85IVDMUH3dEDsBlf14sj2fRiwqrKh_Z4a4wwHU2tHyZoFGxtLMBFPnL7Bb1V-YbBKGtrhxpZpOZG5XK1X44L8-QGk6CSjkYC7ocmjnHgPzxCWcaNYswXERjOQzBo)

Printing details:

![](https://lh4.googleusercontent.com/VG9rP2sIFoNh90AmalwHebBdlfNhIt-R7QUfqzjSL_RaPOc8diwudF8XJAbFLv3A_vRkX24keNzxaJp5A2IQSafRdIcKTO9i2DaLx4qcRMubgtLUXesLfINPjpxTyh6zc5kGO8h3arajYN8jxRNpyGQ)

mistakes i made:
----------------

1.  I placed the jumpers wrong (I misplaced them). That what was supposed to go to ground (- on the breadboard) i put to the vcc (+ on the breadboard). that caused my laptop screen to notify me that I needed to plug in the prototype to my laptop.\
    later causing the LCD to burn.

2.  When the jumpers were placed right, I found out that the sensor was drawing too much power which caused  the servo shaft to move when I plugged my prototype and then stopped. it was acting like when suddenly the power was off. We added a 9v battery so the current flow wouldn't be interrupted.

3.  After that I found out I didn't add lcd.backlight(); to my code, which caused the contrast of the lcd to dim.

4.  When designing the 3d design in the tinkercad I forgot to add a hole for the end stop switch, so the wires could go straight to the trash can.

5.  When the 3d design was printed, the holes of the sensor and lcd were big and there was not enough space to plug the 9v battery into the arduino.

Pictures of my final project:
-----------------------------

![](https://lh4.googleusercontent.com/El7JCqY2oTar2XD8M-jRB0LvcJhUnqNP1HmN91r4Cw8EPJNfzsOgngaAFMjw7x3Xvjw2zNVZAetubCVzOnmSSLBCgRSCNg0mvYEkPHFM3t6rMHC1Z26_KYhQYmdFnjCCxFw47jS9bR-yJy-yIPWwKrQ)

![](https://lh3.googleusercontent.com/DdDWyE8LCfGsMXo0Pd5r7ZyXwIrURS09waZ2Fz6IN5aOzT8Zv9eeLFSqmE7QVh_a0CNHfTdGzUM9ief4_1HXOp-gpuTNWKeA29-9jWc-8brGT_Zu8BQfrLYxcWztP9LhdgM4h1orDeS-WCYKY0u7Wgk)![](https://lh5.googleusercontent.com/Ar1OE9tkMPCfuMNhEHd6BIn6nT9WLhdenJ_YoBAoo1_nt8wH16FZLXVwJkEN12h9Giv0HPyLlVNE_hQNQnQZrqJhfEekJU10psxremkuaBslPWxZz4xonkKmmY5nY8ouXFVdeYatxVZz3QQnuZRwWVg)![](https://lh4.googleusercontent.com/CbtscEK7eTeyxIZIpahwSyjxWYNVdfwJhly7F49f4lXFQZVVjrhzUiElqS8-7dzBCUfBYOcREqIzPId0ZxlYCBr1mTELKaPLA4Gb6A4c3SKkSKx4YTKZI_3KPM1qV2AfZ2IFZ-DMtue_H_oyI-cZclE)![](https://lh4.googleusercontent.com/YXpGc7BYXoH09HcRNfjgzQSnaOa1rXAm2SVO8mVfJaoz7d1HJStDQB8-c4ipuaPEOBdtssuAB_8wFyCeZolI718rYFWDtJ_FWLbx4VaHjVGqEpH2aFJaF3Pw28Tfd6S3ze21HLUeyPiK83hN0KmLy_c)![](https://lh4.googleusercontent.com/peMNm5eLwcMZa8Gzq5PvRLGNUBdy9uSgSU7eYGGV5IdzY6T6CTLuU3To5go160MYRmhrE-4nHyChKxdWQuxPQjDgzM_BUTlqdfjNaLyf8M0Hc2EB-zdcviJMap4-6NhvIoZa_dfUZ2-A6ze-ON1-U9g)![](https://lh6.googleusercontent.com/ZvpIbl_btnpRuo3F1dMxRTY3zsM0AtPAI1ugPhaSr-chYCUILGurG_wS_P_kIYYAy6aF6k8mBUZcJN2-XT1Uya2O0St_nsO0nw7hggibu-pzA2uT2gP79U9V_bXd3QdGi-OiVOpTKcgcvBcrkJpAQvE)![](https://lh6.googleusercontent.com/h_LTy7wTLk2LG4V5zM_Natft65938BAaI58vEBOsFsAcOdUcnyK4CVC4Q7TGYA3qwwDoZ_Nja05GyRIrtJTy3sQrC0UaHAoD0fq_il0CTIwfEg6pe1pmTCjDEJ2QXww1lCMdLesa89NQaXTnJHKfOoA)![](https://lh4.googleusercontent.com/zyNh_3WOnYcyu3Z96nju4BsqhwCOWqz4e475feUhHT26Vs9ajwiAj5ohd8uoTl69hRrc4sBf4cAw83POllGtYw7E2aoUPyEN5Il2MB8cXOYxxbaDAZT42hldDtgcov8zBdEfwKr2YEw0IP0mCds16cE)![](https://lh3.googleusercontent.com/4_bapUOf-1hB25ZNOhZgmtpJZufMe1W3YEEmlYi0bbXiT2tr83dIe60LJI9qplK11v1GKrufRvrOZ1TjTHccorADFzMEbNlhgJtW83-4EoafBV7lotOSz2WQfh2OaoTS6xWCsDMJGgI_xk2Io16dMCI)![](https://lh6.googleusercontent.com/H0KQwDzj8qWVPppnVb9AjPh5OpajgocOdh5pc-g8DNS7TLaSkhGa_zct6Mf_PEWj6n-7hgLx9AdLfyHU3LBsqSdJhVG5UdfBcH3RLiPq5hY8QY1lwM3hKPnxN-GymRHWERhFi1Qi2Ok7E9GPh6kamKE)![](https://lh3.googleusercontent.com/Bv29fYrFky30A_AKEc6VNrL7afj_cEb4WmC9Tz7GiBchLIE8b6oiCEmeekg4lQkePHpGiVgu3s8xAI5sQneNdqAlmJPj21-rFPwIwMeAen0hdkkxa5Mb77Kpl0qp_SQXVTaiTl58UuxPYQ-xgjv_i3o)

![](https://lh4.googleusercontent.com/yKjYeg1HpDfYPysw1_cH2aeQ03LvYWxgfEjhf0YPqXW4PdG1QzmugI0kMH0__6BstW8WRQXeC2g8dSxj3Z3PM0LF9gXmz4Yh_9nPxZuF1Dm7sIHRq2TFQxFSpAKf76yPT3ouvsKAzzxjCuondqqTpRw)![](https://lh4.googleusercontent.com/ZwcfJpmAebjkRTuP4DuCXhin4XwPh74KX1-3y0m-l4MvOns_WqN3aUdv9gNSyxbvPmoQX32szVrqtgy7QzTSn_rm05L34CxVjFGUXQ0IGo9Ua9yXpn4FDCrAg8eHeYf555qmiMSWfts3rbRSMYxlJwg)![](https://lh5.googleusercontent.com/InuXyRMy7hTNIvloJexS4StlrIg-ggiZdYB2GMyKVWPqraXvaOpqKZHYG6dRETSg8QPjNprN9qEBggjYGZWjp0cL4-w0zhLKB3O9lHO_yZpE4Dn832GNV8bDx2vVnTmmWzZwihlgNJabnI5kMtSEJ5c)

![](https://lh5.googleusercontent.com/Gtas_Mr2Zhl2wmWroOY4eKanwTCqpdDvutd18ehfnMWODbeO95OKS8x0-0KmuCX0wksgSK9TUuQvdS6wnuWNY5zxnlfRYLfX5TGDmCxBUvXP4i0CNx7j486vSigIcgQ9wj12bBwiJdDW2s4PgAsc3P8)![](https://lh5.googleusercontent.com/1SlDuuBhH5NLz3gQdcHEVfg_vi_mfKvGvJCiZo_t25Ji1uX3BVXH4GBvOpGwy9o7ZPidokeW-nVI_cLs5L-HOhezQHbjhPUfQKFRKWrjzQ3yl0yXnhSRE73k63im_2tfwBRC_Zer5X4NdhO2H-1mjK0)![](https://lh4.googleusercontent.com/jM2x5QAaKVWenkq1DvV9KBUdeDqLNilxTGLOCiHwXVRLlkCfHvSK4Ny1n-4q_5c0Hgrue6VeCbP5NQpfQMicWccAEgKmQpCxJptpruObFedJYCgzcIktH-EGxx3y9mb2ejTH6tFEHKFCSjtBqc8Ahvw)![](https://lh3.googleusercontent.com/yjL6WhnDMJndJVF_I5eFWg8TKgf6FtwiZ5MLjmAhFYnl0hIpwCYHU0WqOJ96tKyo9RgSONklzKr22O-CJzcetyyqHzMH9Qh9AYTUgSxhfM52EZ0lHFqJQqsPeVL3o6sx4abjugNbutRaL-8j7IES5mA)